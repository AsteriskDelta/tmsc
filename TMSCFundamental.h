#ifndef TMSCFUNDAMENTAL_H
#define TMSCFUNDAMENTAL_H

#include "physics/PhysicalObject.h"
#include "physics/PhysicalEntity.h"
#include "physics/Updatable.h"
#include "physics/Damagable.h"
#include "render/Renderable.h"
#include "physics/Proxy.h"
#include "physics/PhysicalProxy.h"

#endif /* TMSCFUNDAMENTAL_H */


#include <iostream>
#include <unistd.h>
#include <fstream>
#include <ARKE/EngineClient.h>
#include <ARKE/UI.h>
#include <mutex>
#include "TMSCInclude.h"
#include "TMSCFundamental.h"
typedef std::recursive_mutex Mutex;
typedef std::lock_guard<Mutex> MutexGuard;

#include "map/Tile.h"
#include "map/TileManager.h"
#include "map/MapSector.h"
#include "map/MapSector.h"
#include "map/TileInstance.h"
#include "map/Map.h"
#include "entity/Entity.h"
#include "entity/EntityRenderer.h"
#include "sprite/Sprite.h"
#include "physics/field/Light.h"

extern int ARKE_DBGMODE;


#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wshadow"
namespace GameManager {

    Window *window;
    Camera *camera;
    RenderPipeline *pipeline;
    Input *input;
    Model *model;
    Shader *opaque, *lineTest, *parTest;
    Testing *testing;

    //Universal

    void Init() {//Called immediatly on execution
        isServer = isClient = true;
        Application::BeginInitialization();
        //System::SetrLimit(1024*1024*1024);
        Application::SetDataDir("../../data/");
        Application::SetName("TMSC");
        Application::SetCodename("tmsc1");
        Application::SetCompanyName("Delta III Tech");
        Application::SetVersion("0.0r1X");
        Application::SetDebug(true);
        Application::OnExit(&GameManager::Cleanup);
        //System::SetStackLimit(1024*1024*256);
        Application::EndInitialization();

        ImageFont::Initialize();
        Input::Initialize();
        //ProgramStat::StartRecorderThread("prstat/main");
    }

    //Shader *txtShader;
    Skydome *sky;
    ParticleSystem *pts, *txt;
    TextureAtlas *txtAtlas;
    Shader *rawModelShader, *rawModelLinesShader;
    UIBar *testBar = nullptr;
    UIRect *testRect;

    void PrimaryLoad() {
        _prsguard(); //Load required assets (for low-level, ie pre loading screen)
        window = new Window(Application::GetName());
        //window->open(0,0);
        window->open(800, 600);
        input = new Input();
        testing = new Testing();

        //ResourceManager::Initialize();

        txtAtlas = new TextureAtlas(2048, 2048, 2);
        txtAtlas->setName("uiAtlas");
        //   /txtAtlas->setMipmap(true);
        txtAtlas->useCache();

        Font::sdfResolution = 32;
        FontIndex *fidx = new FontIndex(txtAtlas, 128);
        fidx->setSDF(true);
        fidx->loadFont("ui/matsura.ttf");

        //EditView::Initialize(fidx, txtAtlas);

        //opaque = Shader::Load("opaqueTest.prgm");
        //lineTest = Shader::Load("lineTest.prgm");
        parTest = Shader::Load("particle.prgm");
        camera = new Camera();
        camera->transform.position = V3(12.f, 12.f, 10);
        //camera->setPerspective(76.f, 0.12f, 8000.f);
        camera->setOrtho(1.f/64.f, 0, 0.f, 100.f, 0.5f, 0.5f, true);
        camera->Activate();
        //sky = new Skydome(8000.f);

        //std::cout << "ALLOC PARTICLE SYS\n";
        /*pts = new ParticleSystem(256);
        pts->setShader(parTest);
        pts->setAtlas(ResourceManager::GetParticles());
        //pts->setSorted(true);
        for(unsigned int i = 0; i < 1024; i++) {
          ParticleVert *v = pts->addVert();
          v->pos = V3(float(rand()%1000) / 20.f, 0.4f,  float(rand()%1000) / 20.f);
          v->size = N2(5.f, 8.f);
          v->normal = normalize(V3(float(rand()%1000) / 100.f - 5.f, float(rand()%1000) / 100.f * 0.1f ,float(rand()%1000) / 100.f - 5.f));
          v->figureID = 0;
        }
        pts->upload();*/
        txt = new ParticleSystem(4096);
        //txt->setSorted(true);
        //FontWriter::Write(fidx, txt, "does this work...? test!", 0.1f, V3(0.f, 40.f, -5.f), V3(1.f, 0.f, 0.f));
        //FontWriter::Write(fidx, txt, "A Second Test :)", 0.1f, V3(30.f, 48.f, -5.f), V3(1.f, 1.f, 0.6f));
        txt->setShader(parTest);
        txt->setAtlas(txtAtlas);

        UIResources::Initialize(txtAtlas);
        UIResources::setDefaultFont(fidx);
        UIWindow::SetDefaults(txtAtlas, parTest);
        /*
        testBar = new UIBar("ui/window.png","ui/simpleBar.png");
        testBar->setSystem(txt);
        testBar->setPosition(V3(0.f, -3.f, 0.f));
        testBar->setTint(ColorRGBA(255,255,255,196));
        testBar->setSize(N2(50.f, 6.f));
        testBar->setValue(86.2f);
        testBar->setColor(ColorRGBA(20, 180, 20, 196));
        testBar->setLabelMode(UIBar::LabelMode::Full);
        testBar->setMaximum(100.f);
        testBar->draw();
    
        testRect = new UIRect("ui/window.png");
        testRect->setSystem(txt);
        testRect->setTint(ColorRGBA(255,255,255,220));
        testRect->setPosition(V3(0.f, -3.f, 0.f));
        testRect->setSize(N2(6.f, 6.f));
        testRect->setNormal(normalize(V3_FORWARD));
        testRect->draw();
         */
        txtAtlas->upload();
        txtAtlas->saveCache();
        txt->upload();

    }

    void ptrCB(Tile* ptr) {
        std::cout << "\tgot tile " << ptr << "\n";
    }

    TileManager *tiles;
Map *mapRoot;
Entity *testEntity; Light *testLight;
    void SecondaryLoad() {
        _prsguard(); //Loads assets(run aschronously with loading screen)
        //ResourceManager::Commit();
/*
        PhysicalProxy *proxy = new PhysicalProxy();
        Damagable *dmg = new Damagable();
        Renderable *ra = new Renderable();
        Renderable *rb = new Renderable();
        Entity *entity = new Entity();

        proxy->add(dmg);
        proxy->add(ra);
        proxy->add(rb);
        proxy->add(entity);

        std::cout << "objs of interest at " << ra << ", " << rb << ", " << entity << ", NOT" << dmg << " in proxy with " << proxy->size() << " elements\n";

        for (auto it = proxy->begin<Renderable>(); it != proxy->end<Renderable>(); ++it) {
            Renderable *obj = &(*it);
            std::cout << "Got a renderable at " << obj << "\n";
            ;
        }*/



        pipeline = new RenderPipeline();
        pipeline->load("pipeline/MainRP.xml");
        pipeline->Allocate();

        //Loadable::LoadAll<Tile>("tiles", &Tile::Factory, &ptrCB);
        tiles = new TileManager();
        tiles->loadFromDir("tiles");
        tiles->buildCrossref();
        tiles->loadFrames();
        
        srand(42);
        //srand(time(nullptr));
        mapRoot = new Map();
        //mapRoot->width = mapRoot->height = 6;
        mapRoot->tiles.resize(mapRoot->width * mapRoot->height);
        mapRoot->leaf = false;
        mapRoot->order = 1;
        int subSize = mapRoot->width;;
        //mapRoot->childWidth = mapRoot->childHeight = subSize;
        for (int i = 0; i < mapRoot->width * mapRoot->height; i++) {
            TCoord c = TCoord(i % mapRoot->width, (i / mapRoot->height));
            //std::cout << "ading subsector at " << c << "\n";
            MapSector *sec = new MapSector();
            //mapRoot->tiles[i].sec = sec;
            //mapRoot->add(sec);
            mapRoot->set(c, sec);
            //sec->width = sec->height = subSize;
            sec->tiles.resize(subSize * subSize);
            sec->leaf = true;
            for (int y = 0; y < subSize; y++) {
                for (int x = 0; x < subSize; x++) {
                    TCoord sub = TCoord(x, y);
                    TileInstance *tile = new TileInstance();
                    tile->secCoords = sub;
                    sec->set(sub, tile);
                    tile->secCoords = sub;
                }
            }
            sec->parentPos = c;
            //sec->setParent(mapRoot);
        }
        testEntity = new Entity();
       // testEntity->setParent(mapRoot);
        
        testEntity->addRenderer();
        Sprite *testEntSprite = new Sprite("entity/test.png", true);
        testEntSprite->prepare();
        testEntity->renderObject->tmpSprite = testEntSprite;
        testEntity->position() = N2(6.f, 7.f);
        mapRoot->add(testEntity);
        /*std::cout << "entity is at " << dynamic_cast<PhysicalObject*>(testEntity) << "\n";
        
        for(auto it = mapRoot->beginAll<PhysicalObject>(); it != mapRoot->endAll<PhysicalObject>(); ++it) {
            std::cout << "Got physobj at " << &(*it) << ": entity=" << it->isEntity() << "\n";
        }*/
        
        std::string floorType, pillarType;
        if(true) {
            floorType = "srx.grass";
            pillarType = "srx.rock";
        } else {
            floorType = "corridor.floor";
            pillarType = "corridor.grate";
        }

        TCoord totalSize = TCoord(subSize * mapRoot->width, subSize * mapRoot->height);
        for (int x = 0; x < totalSize.x; x++) {
            for (int y = 0; y < totalSize.y; y++) {
                TCoord c(x, y);
                TileInstance *tile = mapRoot->get(c);
                if(tile == nullptr) std::cout << x << ", " << y << " = nullptr\n";
                if (tile == nullptr) continue;
                //tile->tile = tiles->get("corridor.grate");
                if (x == 0 || y < 1 + (x-1)/5 || totalSize.y - y < 2 + (x-1)/5) tile->tile = tiles->get("corridor.underblock");
                else if(x == 1 && (y <= 1 || y >= totalSize.y-2)) tile->tile = tiles->get("corridor.underblock");
                else if(x == 1/* || (x == 2 && (y <= 3 || y >= totalSize.y-3-1))*/) tile->tile = tiles->get(floorType);
                //else if (x < 1 || x > subSize * mapRoot->width - 2) tile->tile = tiles->get("corridor.ladder");
                else if (y < 2 + (x-1)/5 + 1 || (y == 2 + (x-1)/5 + 1&&x%5 == 0) ||
                         totalSize.y - y < 3 + (x-1)/5 + 1 || (totalSize.y - y == 3 + (x-1)/5 + 1&&x%5 == 0)) tile->tile = tiles->get(floorType);
                //else if (y < 5 + x / 10 || y == subSize * mapRoot->height - 1) tile->tile = tiles->get("srx.grass");
                else if ((x % 12 == 0 && y < 5 + (x-1) / 3)  || ((x+1)%12 == 0&& y < 5 + (x-1)/3)) tile->tile = tiles->get(pillarType);
                else if(x%6 >2 && x%6 <=5&& y == std::floor(x/6)*6) tile->tile = tiles->get("corridor.platform");
                else if(x%6 >1 && x%6 <=3&& y == std::floor(x/6)*9) tile->tile = tiles->get("corridor.platform");
                else if ( (y < 4 + (x-1)/5 + 1 || (y == 4 + (x-1)/5 + 1&&x%5 == 0)) && rand()%2 == 0) {
                    if(mapRoot->get(c - TCoord(0, 1))->tile ) tile->tile = tiles->get("srx.plant");
                } else {
                    //mapRoot->set(c, (TileInstance*)nullptr);
                    //delete tile;
                    tile->tile = nullptr;
                }
                
                //std::cout << x << ", " << y << " = " << to_string(tile->secCoords) << " to " << (tile->tile == nullptr? "null" : tile->tile->name) << "\n";
            }
        }
        
        for (int i = 0; i < mapRoot->width * mapRoot->height; i++) {
            TCoord c = TCoord(i % mapRoot->width, (i / mapRoot->height));
            MapSector *sec = mapRoot->get(CoordStack(c));
            for (int y = 0; y < subSize; y++) {
                for (int x = 0; x < subSize; x++) {
                    TCoord sub = TCoord(x, y);
                    TileInstance *tile = sec->get(sub);
                    if(tile == nullptr) continue;
                    if(tile->tile == nullptr) {
                        sec->set(sub, (TileInstance*)nullptr);
                        //sec->remove(tile);
                        delete tile;
                    }
                }
            }
            sec->parentPos = c;
            sec->setParent(mapRoot);
        }
        //sleep(10);

        for (int y = 0; y < subSize * mapRoot->height; y++) {
            for (int x = 0; x < subSize * mapRoot->width; x++) {
                TCoord c(x, y);
                TileInstance *tile = mapRoot->get(c);
                if (tile == nullptr) continue;
                tile->characterize(0);
            }
        }
        for (int y = 0; y < subSize * mapRoot->height; y++) {
            for (int x = 0; x < subSize * mapRoot->width; x++) {
                TCoord c(x, y);
                TileInstance *tile = mapRoot->get(c);
                if (tile == nullptr) continue;
                tile->characterize(1);
            }
        }
        
        
        Sprite::UploadAtlas();
        //Sprite::CacheAtlas();
        
        testLight = new Light();
        testLight->position() = N2(11.f, 11.f);
        testLight->setOffset(V3(0.1f, 0.1f, 0.06f));
        testLight->setCoeffs(1.f, 0.3f);
        testLight->setColor(ColorRGBA(127, 127, 127, 190), ColorRGBA(0,0,255,0));
        //testLight->setRadius(0.2f);
        testLight->setRadii(V2(9.f, 9.f));
        testLight->setShadows(true);
        testLight->setFOV(360.f, 3.f);
        testLight->setDirection(N2(0.f, -1.f));
        //testLight->setShadowsIgnoreEntities(true);
        testLight->setOccludable(true);
        testLight->addRenderer();
        testLight->prepare();
        
        mapRoot->add(dynamic_cast<Entity*>(testLight));
        
       // mapRoot->prerender(-1);
       // mapRoot->uploadRender(-1);
        mapRoot->updateGroups();
        for(auto it = mapRoot->begin<MapSector>(); it != mapRoot->end<MapSector>(); ++it) {
           it->updateGroups();
        }
        
        Renderable::PrerenderAll();
        Renderable::UploadAll();
        //std::cout << "Is Entity: " << testEntity->isEntity() << "\n";
        
        testEntity->updateSprites();
        testEntity->update(0.1f);
        /*for (auto it = mapRoot->begin<Renderable>(); it != mapRoot->end<Renderable>(); ++it) {
            Renderable *obj = &(*it);
            //std::cout << "TILEMAP renderable at " << obj << ", prepping\n";
            obj->prerender();
            obj->uploadRender();
        }*/
    }

    //Low-level client

    void ClientSetup() {//Called after SecondaryLoad is completed

    }

    void WindowChanged() {//Called at window change

    }

    bool toggleWireframe = true, toggleShaded = true;
    //Client-side stuff, called in order listed
    V3 camForward = V3_FORWARD;
    V2 camAxisRot = N2(180.f, 0.f);
    float orthoScale = 1.6f;
    int entityIdx = 0;
    static NI lightAngle = 360;

    void Update(float newDeltaTime) {
        _prsguard();
        _unused(newDeltaTime);
        {
            _prsguard();
            Application::UpdateGlobals();
            input->zero();
            window->update();
            Application::UpdateDelta();
            input->update(1);
        }

        {
            _prsguard();
            float moveSpeed = 6.f;
            float rotateSpeed = 220.0f;

            if (input->getKeyDown(Input::Key::LShift)) moveSpeed *= 14.f;

            V3 deltaPos = V3(0.0f), deltaPosPr = V3(0.f);
            if (input->getKeyDown('w')) deltaPos.y += moveSpeed * _deltaTime;
            if (input->getKeyDown('s')) deltaPos.y -= moveSpeed * _deltaTime;
            if (input->getKeyDown('d')) deltaPos.x -= moveSpeed * _deltaTime;
            if (input->getKeyDown('a')) deltaPos.x += moveSpeed * _deltaTime;
            
            if (input->getKeyDown('r')) orthoScale -= _deltaTime * moveSpeed/5.f;
            if (input->getKeyDown('f')) orthoScale += _deltaTime * moveSpeed/5.f;
            orthoScale = std::clamp(orthoScale, 0.4f, 20.f);
            
            if (input->getKeyPress('t')) ARKE_DBGMODE = (ARKE_DBGMODE+1)%3;//toggleWireframe = !toggleWireframe;
            if (input->getKeyPress('y')) toggleShaded = !toggleShaded;
            if (input->getKeyPress('e')) entityIdx = (entityIdx+1)%2;
            if(input->getKeyDown('g')) lightAngle += _deltaTime*90.f;//testLight->minDot = min(1.f, testLight->minDot + _deltaTime/2.f);
            if(input->getKeyDown('h')) lightAngle -= _deltaTime*90.f;//testLight->minDot = max(-1.5f, testLight->minDot - _deltaTime/2.f);
            camera->transform.translate(deltaPos * camera->transform.rotation + deltaPosPr);
            
            N2 entDeltaPos = N2(0.f);
            if (input->getKeyDown(Input::UpArrow)) entDeltaPos.y += moveSpeed * _deltaTime;
            if (input->getKeyDown(Input::DownArrow)) entDeltaPos.y -= moveSpeed * _deltaTime;
            if (input->getKeyDown(Input::RightArrow)) entDeltaPos.x += moveSpeed * _deltaTime;
            if (input->getKeyDown(Input::LeftArrow)) entDeltaPos.x -= moveSpeed * _deltaTime;
            
            if(entityIdx == 0) testEntity->position() += entDeltaPos;
            else testLight->position() += entDeltaPos;

            //if(input->getKeyPress('h') && triPtr) triPtr->physical.applyAccel(-V3_UP + V3_RIGHT, 90.f); 

            if(input->getKeyDown(Input::LCtrl)) {
                camAxisRot.x += input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed * _deltaTime;
                camAxisRot.y += -input->getAxisDelta(Input::Axis::MouseY) * rotateSpeed * _deltaTime;
                camAxisRot.x = wrapAngle(camAxisRot.x);
                camAxisRot.y = std::clamp((float) wrapAngle(camAxisRot.y), -70.f, 70.f);
            }
            
            
            // testLight->setDirection(normalize(N2(sin(_globalTimef), -cos(_globalTimef))));
            lightAngle = ::nvx::clamp(lightAngle, NI(0), NI(360));
            testLight->setFOV(lightAngle, 16);
            
            //camForward = -V3_FORWARD;
            //V3 forward = camera->transform.forward();
            //forward = normalize(forward + camera->transform.right() * float(input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed) + camera->transform.up() * float(input->getAxisDelta(Input::Axis::MouseY) * rotateSpeed) );
            //camForward = rotate(camForward, camAxisRot.y*DEG_TO_RAD, camera->transform.up());
            //camForward = rotate(camForward, camAxisRot.x*DEG_TO_RAD, camera->transform.right());
            //camForward = normalize(camForward);

            Quat cRot = Quat(V3(camAxisRot.y * DEG_TO_RAD, 0.f, 0.f)) * Quat(V3(0.f, camAxisRot.x*DEG_TO_RAD, 0.f));
            ; //quatFromToRotation(V3_FORWARD, camForward);
            //if(forward != V3_FORWARD && forward != -V3_FORWARD)camera->transform.rotate(cRot);
            camera->transform.rotation = cRot; //.rotate(cRot);
            
            camera->setOrtho(1.f/64.f * orthoScale, 0, 0.f, 100.f, 0.5f, 0.5f, true);
            //camera->transform.rotate(Quat(V3(input->getAxisDelta(Input::Axis::MouseY) * -rotateSpeed * _deltaTime, input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed * _deltaTime, 0.f)));
            if (_globalFrame % 60 == 0) std::cout << to_string(V3(camera->transform.position.x, camera->transform.position.y, orthoScale)) << "\n";
        }

        //ResourceManager::CommitDiffUB();
        mapRoot->update(_deltaTime);
        
        Renderable::PrerenderAll();
        Renderable::UploadAll();

        //ProgramStat::SubmitFrame();
    }

    Transform* GetCameraTransform() {
        return (&camera->transform);
    }

    REND_DRAW_R renderScene REND_DRAW_D{
        if (stage->hasFlag(RenderStage::DrawOpaque)) {
            OnOpaque3D(camera, stage);
        }
        if (stage->hasFlag(RenderStage::DrawTransparent)) {
            OnTransparent3D(camera, stage);
        }
        if (stage->hasFlag(RenderStage::DrawUI)) {
            OnUI3D(camera, stage);
        }
        return 1;}

    static double tt = 0.f;

    void OnOpaque3D(Camera * const camera, RenderStage * const stage) {
        _prsguard();
        _unused(camera);
        _unused(stage);

        //WorldManager::Render();
        //blk->upload((char*)transfs, sizeof(transfs)*128, true);
        //model->draw(stage);
        riEnsureState();
        if (toggleShaded) {
            //opaque->activate();
            riEnsureState();
            //opaque->bindTextureArray(ResourceManager::GetDiffuse(), 1, 1);
            //opaque->bindTexture(Skydome::noise, 4, 4);
            //opaque->bindV3(camera->forward(), 27);

            //opaque->bindV3(camera->transform.position, 28);
            //opaque->bindFloat(tt, 25);

            //ResourceManager::GetDiffuseUB()->bindTo(41);
            tt += _deltaTime;
            riEnsureState();
            //WorldManager::Render();
            riEnsureState();
            

            //sky->render(V3(camera->transform.position));
            glDisable(GL_CULL_FACE);
            //glDisable(GL_DEPTH_TEST);
            glEnable(GL_DEPTH_TEST);
            //mapRoot->render();
            Renderable::RenderPass(Renderable::Pass::Entity);
            Renderable::RenderPass(Renderable::Pass::Map);
            //glDepthMask(false);
            //pts->draw();
            //glDepthMask(true);
            //glEnable(GL_DEPTH_TEST);
            
            glDisable(GL_DEPTH_TEST);
            glDepthMask(false);
            Renderable::RenderPass(Renderable::Pass::Lighting);
            Renderable::RenderPass(Renderable::Pass::Debug);
            glDepthMask(true);
            glEnable(GL_DEPTH_TEST);
            
            glEnable(GL_CULL_FACE);
            // std::cout << "Drawing stage\n";
            //testing->DrawSphere();
            //testObj.draw();
            //subObj.draw();
            //testHObj.draw();
            //opaque->deactivate();
            riEnsureState();
        }

        if (toggleWireframe) {
            //glEnable(GL_BLEND);
            //lineTest->activate();
            //WorldManager::RenderDebug();
            //lineTest->deactivate();

            //glDisable(GL_BLEND);
        }

    }

    void OnTransparent3D(Camera * const camera, RenderStage * const stage) {
        _prsguard();
        _unused(camera);
        _unused(stage);
        glDisable(GL_CULL_FACE);
        glDepthMask(false);
        if (toggleShaded) {
            //pts->draw();
            //txt->draw();

        }

        if (toggleWireframe) {

        }
        glDepthMask(true);
        //editView->render();
        glEnable(GL_CULL_FACE);
    }

    void OnUI3D(Camera * const camera, RenderStage * const stage) {
        _prsguard();
        _unused(camera);
        _unused(stage);
        glDisable(GL_CULL_FACE);
        glDepthMask(false);
        if (toggleShaded) {


        }
        txt->upload();
        txt->draw();
        
        
        
        /*for (auto it = mapRoot->begin<Renderable>(); it != mapRoot->end<Renderable>(); ++it) {
            Renderable *obj = &(*it);
            obj->render();
            MapSector *sec = obj->adapt<MapSector>();
            //if(sec != nullptr) std::cout << "\t\tRENDER " << sec->
        }*/
        
        if (toggleWireframe) {

        }
        glDepthMask(true);
        //editView->render();
        glEnable(GL_CULL_FACE);
    }

    void OnOrtho() {

    }

    void Render() {
        _prsguard();
        pipeline->Activate();
        //camera->transform.position.z = -0.1f;
        //camera->transform.lookAt(V3(0,0,0));
        camera->Activate();
        //opaque->activate();

        //testing->DrawPlane();
        //testing->DrawSphere();
        //model->draw();
        pipeline->Execute(renderScene);
        //testing->DrawTriangle();
        //opaque->deactivate();

        pipeline->Blit();
        pipeline->Deactivate();
        //std::cout << "new frame\n";

        //window->update();

        camera->Deactivate();
    }

    unsigned short GetDistortionCount() {
        return 0;
    }

    void OnDistortion(unsigned short id) {
        _unused(id);
    }

    //Server-side stuff

    void OnTick() {

    }

    void Cleanup() {
        //ResourceManager::Cleanup();
        //ProgramStat::StopRecorderThread();
    }

    bool isServer, isClient;
};
#pragma GCC diagnostic pop

int main(int argc, char** argv) {
    _unused(argc);
    _unused(argv);
    GameManager::Init();

    GameManager::PrimaryLoad();
    GameManager::SecondaryLoad();

    GameManager::ClientSetup();

    int exitCode = 0;
    try {
        while (true) {
            _prsguard();
            GameManager::Render();

            GameManager::Update(1.f);
            //if(_globalFrame % 60 == 0) std::cout << "FPS: " << (1.f/_deltaTime) << "\n";
            //usleep(std::max(0, int((0.014f-_deltaTime)*1400)));
        }
    } catch (const Application::Exiting &ex) {
        exitCode = ex.code;
    } catch (const std::exception& ex) {
        Console::Fatal(ex.what());
        return 1;
    } catch (...) {
        std::cout << "Caught something...\n";
    }

    return exitCode;
}

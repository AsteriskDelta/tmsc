#ifndef ENTITY_H
#define ENTITY_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include "EntityResources.h"
#include "EntityStats.h"

class EntityClass;
class EntityController;
class Sprite;
class MapSector;
class EntityRenderer;
class EntityGroup;
class DebugGroup;

class Entity : 
    public virtual PhysicalEntity, public virtual PhysicalProxy, public virtual Damagable, public virtual Adapter, public virtual Updatable {
    friend class EntityRenderer;
public:
    Entity();
    virtual ~Entity();
    
    //Health, energy, status, etc.
    EntityResources resources;
    
    //Tint, specs, (name, etc.)
    EntityStats stats;
    
    EntityClass *classParent;
    EntityController *controller;
    EntityRenderer *renderObject;
    
    virtual void update(float deltaTime) override;
    
    void setDebugGroup(DebugGroup *dbg);
    virtual void drawDebug(DebugGroup* dbg);
    
    void addRenderer();
    void removeRenderer();
    void setGroup(EntityGroup *gr);
    
    void updateSprites();
    void removeSprites();
    inline EntityGroup *getGroup() const {
        return group;
    }
    
    inline PhysicalObject* physicalParent() const {
        return _derive<PhysicalObject>(this->parent());
    }
    inline virtual bool isEntity() const override { return true; };
    //inline virtual const Bounds<N2>* bounds() const override { return &maximalBounds; };
protected:
    Bounds<N2> maximalBounds;
    //MapSector *sector;
    EntityGroup *group;
    DebugGroup *debugGroup;
};

#endif /* ENTITY_H */


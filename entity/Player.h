
/* 
 * File:   Player.h
 * Author: galen
 *
 * Created on April 2, 2017, 5:05 PM
 */

#ifndef PLAYER_H
#define PLAYER_H

class Player {
public:
    Player();
    Player(const Player& orig);
    virtual ~Player();
private:

};

#endif /* PLAYER_H */


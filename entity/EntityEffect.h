
/* 
 * File:   EntityEffect.h
 * Author: galen
 *
 * Created on April 12, 2017, 2:04 PM
 */

#ifndef ENTITYEFFECT_H
#define ENTITYEFFECT_H

class EntityEffect {
public:
    EntityEffect();
    EntityEffect(const EntityEffect& orig);
    virtual ~EntityEffect();
private:

};

#endif /* ENTITYEFFECT_H */


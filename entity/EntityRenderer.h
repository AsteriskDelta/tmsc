#ifndef ENTITYRENDERER_H
#define ENTITYRENDERER_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include "Entity.h"

class SpriteFrame;
class SpriteInstance;
class Entity;
class EntityGroup;
class DebugGroup;
class EntitySpriteInstance;

class EntityRenderer {
public:
    friend class Entity;
    EntityRenderer(Entity *par);
    virtual ~EntityRenderer();
    
    Entity *parent;
    Sprite *tmpSprite;
protected:
    void updateSprites();
    void reserveSprites();
    void removeSprites();
    
    void drawDebug(DebugGroup* dbg);
    
    inline EntityGroup* group() const {
        return parent->getGroup();
    }
    
    std::vector<SpriteFrame*> frames;
    std::vector<EntitySpriteInstance*> instances;
private:

};

#endif /* ENTITYRENDERER_H */


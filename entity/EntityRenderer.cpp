#include "EntityRenderer.h"
#include "sprite/SpriteInstance.h"
#include "Entity.h"
#include "render/EntityGroup.h"
#include "sprite/Sprite.h"
#include "render/DebugGroup.h"
#include "sprite/PhysicalSpriteInstance.h"
#include "entity/EntitySpriteInstance.h"

EntityRenderer::EntityRenderer(Entity *par) : parent(par), tmpSprite(nullptr) {
    
}

EntityRenderer::~EntityRenderer() {
    this->removeSprites();
    if(parent->debugGroup != nullptr) parent->debugGroup->clear(this);
}

void EntityRenderer::updateSprites() {
    //parent->maximalBounds.zero();
    for(auto it = instances.begin(); it != instances.end(); ++it) {
        SpriteInstance *const ins = *it;
        /*ins->transform = parent->transform();
        ins->transform.position.z += 0.5f;*/
        ins->position() = N2(0.f);//parent->position();
        ins->span() = N2(2.f, 2.f);
        //ins->setParent(parent);
        //parent->add(ins);
        ins->computeLocals();
        ins->computeAgnostics();
        ins->update();
        //std::cout << "\tENTITY adding bounds " <<VP(ins->parentBounds().center)<<", " <<VP(ins->parentBounds().farPoint()) << "\n";
        //parent->maximalBounds.add(*(ins->bounds()));
        /*
        DebugGroup *dbg = nullptr;
        if((dbg = parent->debugGroup) != nullptr) {
            dbg->setOwner(this);
            dbg->clear(this);
            ins->drawDebug(dbg);
            dbg->setOwner(nullptr);
        }*/
        //std::cout << "Entity updated renderer instance\n";
    }
    parent->computeLocals();
    parent->computeAgnostics();
    //std::cout << "ENTITY for overall " <<VP(parent->bounds()->center)<<", " <<VP(parent->bounds()->farPoint()) << "\n";
    //std::cout << "ENTITY as agnostic: " <<VP(parent->agnosticBounds()->center)<<", " <<VP(parent->agnosticBounds()->farPoint()) << "\n";
}
void EntityRenderer::reserveSprites() {
    if(group() == nullptr) return;
    
    if(tmpSprite != nullptr && instances.size() == 0) {
        tmpSprite->repair();
        EntitySpriteInstance *ins = new EntitySpriteInstance();
        ins->setFrame(SpriteFrame(tmpSprite));
        group()->attach(ins);
        parent->add(ins);
        instances.push_back(ins);
        //std::cout << "Entity reserving tmpSprite...\n";
    }
}
void EntityRenderer::removeSprites() {
    if(instances.size() == 0) return;
    
    for(SpriteInstance *const& ins : instances) {
        delete ins;
    }
    instances.clear();
}

void EntityRenderer::drawDebug(DebugGroup* dbg) {
    dbg->clear(this);
    dbg->setOwner(this);
    if(tmpSprite != nullptr && instances.size() > 0) {
        tmpSprite->drawDebug(dbg, instances[0]);
    }
    dbg->setOwner(this);
    //std::cout << "DRAW BOUNDS\n";
    if(parent->bounds() != nullptr) {
        //std::cout << "EntityDbg Got ag: " <<VP(parent->localToAgnostic(parent->bounds()->center)) << ", " << VP(parent->localToAgnostic(parent->bounds()->farPoint())) << "\n";
        Sprite::DrawDebugBounds(dbg, parent, parent->bounds(), ColorRGBA(255,0,0,255));
    }
    
    for(auto it = instances.begin(); it != instances.end(); ++it) {
        SpriteInstance *const ins = *it;
        ins->drawDebug(dbg);
    }
    dbg->setOwner(nullptr);
}
#include "EntityResources.h"

//Allow to be stored at a higher precision than required
const static int entryFactors[] = {1, 1, 1, 1, 1};
#define ERES_VARCNT (VarCount)
#define ERES_ENTCNT (EntryCount)

EntityResources::EntityResources() {
    for(int i = 0; i < ERES_ENTCNT; i++) entries[i].makeZero();
}

EntityResources::EntityResources(const EntityResources& orig) {
    for(int i = 0; i < ERES_ENTCNT; i++) entries[i].set(orig.entries[i]);
}

EntityResources::~EntityResources() {
}

int EntityResources::get(const unsigned short idx, const unsigned char entry) const {
    return entries[entry].vals[idx] / entryFactors[entry];
}
bool EntityResources::has(const unsigned short idx, int amnt, const unsigned char entry) const {
    return this->get(idx, entry) >= amnt;
}

int EntityResources::set(const unsigned short idx, int val, const unsigned char entry) {
    entries[entry].vals[idx] = val * entryFactors[entry];
    return val;
}
int EntityResources::del(const unsigned short idx, int d, const unsigned char entry) {
    entries[entry].vals[idx] += d * entryFactors[entry];
    return this->get(idx, entry);;
}

int EntityResources::increaseUntil(const unsigned short idx, int val, const unsigned char entry) {
    entries[entry].vals[idx] = std::max(entries[entry].vals[idx], val * entryFactors[entry]);
    return val;
}
int EntityResources::decreaseUntil(const unsigned short idx, int val, const unsigned char entry) {
    entries[entry].vals[idx] = std::min(entries[entry].vals[idx], val * entryFactors[entry]);
    return val;
}

void EntityResources::update(int dTicks) {
    delay.set(delay - dTicks);
    delay.makeAtLeast(0);
    
    Entry curDelta;
    curDelta.set((delta * delay.zeros() * dTicks) / TICKS_PER_SECOND);
    
    current.set(current + curDelta);
    current.makeAtMost(max);
}


EntityResources::Entry& EntityResources::Entry::set(const EntityResources::Entry &o) {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = o.vals[i];
    return *this;
}

EntityResources::Entry EntityResources::Entry::operator*(const EntityResources::Entry& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] * o.vals[i];
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator/(const EntityResources::Entry& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] / o.vals[i];
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator+(const EntityResources::Entry& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] + o.vals[i];
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator-(const EntityResources::Entry& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] - o.vals[i];
    return ret;
}

EntityResources::Entry EntityResources::Entry::operator*(const int& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] * o;
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator/(const int& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] / o;
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator+(const int& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] + o;
    return ret;
}
EntityResources::Entry EntityResources::Entry::operator-(const int& o) const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = vals[i] - o;
    return ret;
}

void EntityResources::Entry::makeAtMost(const EntityResources::Entry& o) {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = std::min(vals[i], o.vals[i]);
}
void EntityResources::Entry::makeAtLeast(const EntityResources::Entry& o) {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = std::max(vals[i], o.vals[i]);
}
void EntityResources::Entry::makeAtMost(const int& max) {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = std::min(vals[i], max);
}
void EntityResources::Entry::makeAtLeast(const int& min) {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = std::max(vals[i], min);
}
void EntityResources::Entry::makeZero() {
    for(int i = 0; i < ERES_VARCNT; i++)  vals[i] = 0;
}

EntityResources::Entry EntityResources::Entry::zeros() const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = int(vals[i] == 0);
    return ret;
}
EntityResources::Entry EntityResources::Entry::nonzeros() const {
    Entry ret;
    for(int i = 0; i < ERES_VARCNT; i++)  ret.vals[i] = int(vals[i] != 0);
    return ret;
}
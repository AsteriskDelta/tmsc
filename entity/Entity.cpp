#include "Entity.h"
#include "render/EntityGroup.h"
#include "EntityRenderer.h"
#include "render/DebugGroup.h"
#include "map/MapSector.h"

Entity::Entity() : classParent(nullptr), controller(nullptr), renderObject(nullptr), group(nullptr), debugGroup(nullptr) {
    
}

Entity::~Entity() {
    this->removeRenderer();
}

void Entity::update(float deltaTime) {
    PhysicalEntity::update(deltaTime);
    
    //position().x += deltaTime/2.f;
    
    if(renderObject != nullptr) renderObject->updateSprites();
    this->computeAgnostics();
    
    /*std::cout << "entity children:\n";
    //for(auto it = Proxy::beginAll<PhysicalObject>; it != Proxy::endAll<PhysicalObject>(); ++it) {
    for(auto it = this->beginAll<PhysicalObject>(); it != this->endAll<PhysicalObject>(); ++it) {    
        std::cout << "\tchild: Physicalobject: " << &(*it) << " and pos=" << VP(it->position()) << "\n";
    }*/
    
    PhysicalObject *const physp = this->physicalParent();
    //std::cout << "testing inters on parent "<<physp<<", from " << this->parent() << "\n";
    if(physp != nullptr) {
        auto inters = this->getPotentialColliders(this);
        if(inters.size() > 0) {
            std::cout << "Begin possible intersections (from " << VP(position())<<"):\n";
            for(PhysicalObject* collider : inters) {
                std::cout << "\tGot collider at " << collider << "\n";
            }
        }
    }
    this->drawDebug(debugGroup);
}


void Entity::addRenderer() {
    if(renderObject != nullptr) delete renderObject;
    renderObject = new EntityRenderer(this);
}
void Entity::removeRenderer() {
    if(renderObject != nullptr) {
        delete renderObject;
        renderObject = nullptr;
    }
}

void Entity::setDebugGroup(DebugGroup *dbg) {
    debugGroup = dbg;
}


void Entity::setGroup(EntityGroup *gr) {
    if(group != nullptr && gr != group) group->remove(this);
    group = gr;
    renderObject->reserveSprites();
    //std::cout << "Entity::setGroup called on " << this << " with " << group << "\n";
}
void Entity::updateSprites() {
    if(renderObject == nullptr) {
        Console::Err("Entity::updateSprites called on object with no SpriteRenderer (address ",this,")");
        return;
    }
    
    renderObject->reserveSprites();
}
void Entity::removeSprites() {
    if(renderObject == nullptr) return;
    renderObject->removeSprites();
}

void Entity::drawDebug(DebugGroup *dbg) {
    //std::cout << "entity::drawDebug at " << this << ", renderObject="<<renderObject << "dbg=" << dbg << "\n";
    if(renderObject == nullptr || dbg == nullptr) return;
    renderObject->drawDebug(dbg);
}
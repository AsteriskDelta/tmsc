#ifndef ENTITYSPRITEINSTANCE_H
#define ENTITYSPRITEINSTANCE_H
#include "../TMSCInclude.h"
#include "sprite/SpriteInstance.h"
#include "physics/PhysicalObject.h"
#include "sprite/PhysicalSpriteInstance.h"

class EntitySpriteInstance : public virtual PhysicalSpriteInstance, public virtual Adapter {
public:
    EntitySpriteInstance();
    virtual ~EntitySpriteInstance();
    
    
    inline virtual bool isEntity() const override { return true; };
private:

};

#endif /* ENTITYSPRITEINSTANCE_H */


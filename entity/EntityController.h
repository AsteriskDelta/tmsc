
/* 
 * File:   EntityController.h
 * Author: galen
 *
 * Created on April 12, 2017, 3:22 PM
 */

#ifndef ENTITYCONTROLLER_H
#define ENTITYCONTROLLER_H

class EntityController {
public:
    EntityController();
    EntityController(const EntityController& orig);
    virtual ~EntityController();
private:

};

#endif /* ENTITYCONTROLLER_H */



/* 
 * File:   Projectile.h
 * Author: galen
 *
 * Created on April 2, 2017, 5:08 PM
 */

#ifndef PROJECTILE_H
#define PROJECTILE_H

class Projectile {
public:
    Projectile();
    Projectile(const Projectile& orig);
    virtual ~Projectile();
private:

};

#endif /* PROJECTILE_H */


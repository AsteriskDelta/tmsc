
/* 
 * File:   EntityClass.h
 * Author: galen
 *
 * Created on April 12, 2017, 10:41 AM
 */

#ifndef ENTITYCLASS_H
#define ENTITYCLASS_H

class EntityClass {
public:
    EntityClass();
    EntityClass(const EntityClass& orig);
    virtual ~EntityClass();
private:

};

#endif /* ENTITYCLASS_H */


#ifndef ENTITYRESOURCES_H
#define ENTITYRESOURCES_H
#include "../TMSCInclude.h"
#include "EntityEffect.h"
#include <list>

class EntityResources {
public:
    EntityResources();
    EntityResources(const EntityResources& orig);
    virtual ~EntityResources();
    
    enum {
        Health = 0,
        Aux = 1,
        VarCount
    };
    enum {
        Current = 0,
        Max = 1,
        Delta = 2,
        Delay = 3,
        Underflow = 4,
        EntryCount
    };
    
    int get(const unsigned short idx, const unsigned char entry = Current) const;
    bool has(const unsigned short idx, int amnt, const unsigned char entry = Current) const;
    
    int set(const unsigned short idx, int val, const unsigned char entry = Current);
    int del(const unsigned short idx, int d, const unsigned char entry = Current);
    
    int increaseUntil(const unsigned short idx, int val, const unsigned char entry = Current);
    int decreaseUntil(const unsigned short idx, int val, const unsigned char entry = Current);
    
    void update(int dTicks);
    
    struct Entry {
        union {
            struct {
                int health;
                int aux;
            };
            int vals[2];
        };
        
        Entry& set(const Entry& o);
        
        Entry operator*(const Entry& o) const;
        Entry operator/(const Entry& o) const;
        Entry operator+(const Entry& o) const;
        Entry operator-(const Entry& o) const;
        
        Entry operator*(const int& o) const;
        Entry operator/(const int& o) const;
        Entry operator+(const int& o) const;
        Entry operator-(const int& o) const;
        
        void makeAtMost(const Entry& o);
        void makeAtLeast(const Entry& o);
        void makeAtMost(const int& max);
        void makeAtLeast(const int& min);
        void makeZero();
        
        Entry zeros() const;
        Entry nonzeros() const;
    };
    
    std::list<EntityEffect> effects;
private:
    union {
        struct {
            Entry current, max;
            Entry delta, delay, underflow;
        };
        Entry entries[5];
    };
};

typedef EntityResources ERes;

#endif /* ENTITYRESOURCES_H */


#include "PhysicalProxy.h"

void PhysicalProxy::getPotentialCollidersRecurse(const Bounds<N2> &objAgBounds/*PhysicalObject *const& obj*/, std::unordered_set<PhysicalObject*> *const & ret, PhysicalObject *const& maskObj) {
    //std::cout << "PhysicalProxy recurse: enabled=" << this->isCollisionEnabled() << ", bounds=("<<VP(objAgBounds.center)<<", " <<VP(objAgBounds.farPoint()) << "\n";
    if(!this->isCollisionEnabled()) return;
    else if(this == maskObj) return;
    
    //insert our own polygon, if we have one
    //const Bounds<N2> &objAgBounds = *(obj->agnosticBounds());
    if(this->agnosticPoly() != nullptr && this->agnosticBounds()->intersects(objAgBounds)) ret->insert((this));
    /*if(this->isEntity()) {
            std::cout << "PHYSPROX GOT ENTITY!!! At " << dynamic_cast<PhysicalObject*>(this) << " DC=" << dynamic_cast<PhysicalObject*>(this)->isEntity() << "\n";
        }*/
    for(auto it = this->beginAll<PhysicalObject>(); it != this->endAll<PhysicalObject>(); ++it) {
        if(!it->isCollisionEnabled() || it->agnosticBounds() == nullptr) continue;
        
        //std::cout << "\ttesting child... intersects="<<(it->agnosticBounds()->intersects(objAgBounds))<<", bounds=("<<VP(it->agnosticBounds()->center)<<", " <<VP(it->agnosticBounds()->farPoint()) << ", empty="<<it->agnosticBounds()->isEmpty()<<"\n";
        
        if(it->agnosticBounds()->intersects(objAgBounds) && &(*it) != maskObj && !it->agnosticBounds()->isEmpty()) {
            it->getPotentialCollidersRecurse(objAgBounds, ret);
        }
    }
}

void PhysicalProxy::computeAgnostics() {
    //phyCachedBounds.zero();
    if(!this->isCollisionEnabled()) return;
    //Add our own colliders, if we have them
    SpriteUser::computeAgnostics();
    
    for(auto it = this->beginAll<PhysicalObject>(); it != this->endAll<PhysicalObject>(); ++it) {
        if(!it->isCollisionEnabled() || it->agnosticBounds() == nullptr) continue;
        phyCachedBounds.add(*(it->agnosticBounds()));
    }
}

void PhysicalProxy::computeLocals() {
    if(!this->isCollisionEnabled()) return;
    
    //std::cout << "PhysicalProxy computing locals...\n";
    //Add our own colliders, if we have them
    SpriteUser::computeLocals();
    
    for(auto it = this->beginAll<PhysicalObject>(); it != this->endAll<PhysicalObject>(); ++it) {
        //std::cout << "Considering child ...\n";
        if(!it->isCollisionEnabled() || it->bounds() == nullptr) continue;
        //std::cout << "adding child\n";
        phyLocalBounds.add(it->getParentBounds());
    }
}
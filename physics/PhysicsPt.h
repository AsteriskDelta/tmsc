#ifndef PHYSICSPT_H
#define PHYSICSPT_H
#include "../TMSCInclude.h"
#include <NVX/NPolygon.h>

class PhysicsPt_Base : public NPolygonPtBase<N2, true> {
public:
    //typedef NPolygon<N2, PhysicsPt_Base> Poly;
    //typedef NPolygonPt<N2, PhysicsPt_Base> PolyPt;
    //typedef PhysicsPt Self;
    
    //N2 pt;
    //N2 norm;
    struct Flags {
        bool grabable : 1;
    } meta;
    
    inline PhysicsPt_Base() : NPolygonPtBase<N2, true>() {};
    inline PhysicsPt_Base(const PhysicsPt_Base& o) : NPolygonPtBase<N2, true>(o) {};
    inline PhysicsPt_Base(const N2& p, const N2& n = N2(0)) : NPolygonPtBase<N2, true>(p, n) {};
    
    inline Flags& flags() { return meta; };
    inline const Flags& flags() const { return meta; };

    /*PhysicsPt () : PolyPt() {};
    PhysicsPt (const PhysicsPt& o);
    PhysicsPt (Poly *par, const N2& p, const N2& n = N2(0.f));
    PhysicsPt (const N2& p, const N2& n);
    virtual ~PhysicsPt();

    inline virtual const N2& positionConst() const override {
        return this->pt;
    };
    inline virtual N2& position() override {
        return const_cast<N2&>(this->positionConst());
    };
    inline virtual const N2& normalConst() const {
        return this->norm;
    };
    
    inline virtual N2& normal() {
        return const_cast<N2&>(this->normalConst());
    };
    inline virtual void setNormal(const N2& n) {
        (this->normal()) = n;
    }
    
    inline Poly *parent() const {
        return reinterpret_cast<Poly*>(this->parentPtr);
    };
    inline Self* sibling(int delta) const {
        if (this->parentPtr == nullptr) return nullptr;
        return this->parent()->sibling(this, delta);
    }

    inline Self* right(int del = 1) const {
        return this->sibling(del);
    };

    inline Self* left(int del = 1) const {
        return this->sibling(-del);
    };*/
private:

};

typedef NPolygonPt<N2, PhysicsPt_Base> PhysicsPt;
typedef NPolygon<N2, PhysicsPt> PhysicsPoly;

#endif /* PHYSICSPT_H */


#include "PhysicalObject.h"
#include "Material.h"

PhysicalObject::PhysicalObject() {
}

PhysicalObject::PhysicalObject(const PhysicalObject& orig) : Adapter(orig), Transformable(orig), SpriteUser(orig) {
}

PhysicalObject::~PhysicalObject() {
}

/*Sprite* PhysicalObject::sprite() const {
    
}
PhysicsPoly* PhysicalObject::poly() const {
    
}*/
Material* PhysicalObject::material() const {
    return nullptr;
}

NI PhysicalObject::area() const {
    return this->poly()->area();
}
NI PhysicalObject::mass() const {
    if(material() != nullptr) return material()->density() * this->area();
    else return this->area();
}
NI PhysicalObject::density() const {
    return this->mass() / this->area();
}

bool PhysicalObject::isCollisionEnabled() const {
    return true;
}

std::unordered_set<PhysicalObject*> PhysicalObject::getPotentialColliders(const Bounds<N2>& objAgBounds, PhysicalObject *const& maskObj) {
    if(this->agnosticBounds() == nullptr ||
            !this->isCollisionEnabled()) return std::unordered_set<PhysicalObject*>();
    
    PhysicalObject *parentObj = _derive<PhysicalObject>(this->parent());
    if((this->agnosticBounds()->fullyContains(objAgBounds) && this != maskObj) || parentObj == nullptr) {
        std::unordered_set<PhysicalObject*> ret;
        this->getPotentialCollidersRecurse(objAgBounds, &ret, maskObj);
        return ret;
    } else {
        return parentObj->getPotentialColliders(objAgBounds, maskObj);
    }
}
std::unordered_set<PhysicalObject*> PhysicalObject::getPotentialColliders(PhysicalObject* obj) {
    if(obj == nullptr || obj->agnosticBounds() == nullptr) return std::unordered_set<PhysicalObject*>();
    
    return this->getPotentialColliders(*(obj->agnosticBounds()), obj);
}

void PhysicalObject::getPotentialCollidersRecurse(const Bounds<N2> &objAgBounds, std::unordered_set<PhysicalObject*> *const & ret, PhysicalObject *const& maskObj) {
    if(!this->isCollisionEnabled()) return;
    else if(this == maskObj) return;
    
    if(this->agnosticBounds()->intersects(objAgBounds)) ret->insert(this);
}
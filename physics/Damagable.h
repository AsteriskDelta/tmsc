#ifndef DAMAGABLE_H
#define DAMAGABLE_H
#include "../TMSCInclude.h"
#include "PhysicalObject.h"
#include "Damage.h"

class PhysicalEntity;

class Damagable : public virtual Adapter {
public:
    Damagable();
    Damagable(const Damagable& orig);
    virtual ~Damagable();
    
    void applyDamage(const Damage& dmg, PhysicalEntity *src);
private:

};

#endif /* DAMAGABLE_H */


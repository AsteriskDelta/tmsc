#ifndef TRANSFORMABLE_H
#define TRANSFORMABLE_H
#include "../TMSCInclude.h"
#include <NVX/N2.h>
#include <ARKE/Matrix.h>
#include <ARKE/Transform.h>

class Transformable : public virtual Adapter {
public:
    inline Transformable() : _parentPtr(nullptr) , _rel() {
        
    }
    inline Transformable(const Transformable& orig) : Adapter(orig), _parentPtr(orig._parentPtr), _rel(orig._rel) {
        
    }
    inline virtual ~Transformable() {
        this->setParent(nullptr);
    }
    
    inline virtual Transformable* self() { return this; };
    
    inline virtual const N2& positionConst() const { return _rel.position; };
    inline virtual const float& rotationConst() const { return _rel.rotation; };
    inline virtual const N2& spanConst() const { return _rel.size; };
    
    //inline virtual Transform& transform() { return relTransform; };
    inline virtual N2& position() { return const_cast<N2&>(this->positionConst()); };
    inline virtual float& rotation() { return const_cast<float&>(this->rotationConst());  };
    inline virtual N2& span() { return const_cast<N2&>(this->spanConst()); ; };
    
    inline virtual N2 internalSpan() const { return this->spanConst(); };
    inline virtual N2 scale() const { return this->spanConst() / this->internalSpan(); };
    inline virtual N2 inverseScale() const { return N2(1.f) / this->scale(); };
    
    //Absolute coordinates
    virtual N2 parentToLocal(N2 parPos) const;
    virtual N2 localToParent(N2 locPos) const;
    
    //Transform to phyics' frame of reference
    virtual N2 localToAgnostic(N2 locPos) const;
    virtual N2 localNormToAgnostic(N2 locStart, N2 locNorm) const;
    virtual N2 agnosticToLocal(N2 agPos) const;
    
    virtual N2 localToRender(N2 locPos) const;
    virtual N2 renderToLocal(N2 pos) const;
    virtual N2 localNormToRender(N2 locStart, N2 locNorm) const;
    
    //Enable/disable matrix accumulation functions (may only be false for those without children, or the GPU couldn't render it)
    virtual bool isLinearTransform() const { return true; };
    //virtual Mat3 matrix(bool recursive = false) const;
    //virtual Mat3 invMatrix(bool recursive = false) const;
    virtual Mat4 renderMatrix(bool recursive = false) const;
    virtual Mat4 invRenderMatrix(bool recursive = false) const;
    
    inline virtual Proxy* proxy() { return nullptr; };
    
    //Pay no mind to pointer hacking... so long as the vtable lives, so to shall this code's use
    inline virtual Transformable *parent() const override {
        return reinterpret_cast<Transformable*>(_parentPtr);
    };
    template<typename T, typename PRX=Proxy*>
    inline void add(T obj) {
        //std::cout << "This=" << this << "\n";
        //std::cout << "self=" << this->self() << "\n";
        //std::cout << "proxy=" << this->proxy() << "\n";
        if(this->proxy() != nullptr) {
            reinterpret_cast<PRX>(proxy())->add(obj);
        }
    }
    template<typename T, typename PRX=Proxy*>
    inline void remove(T obj) {
        if(this->proxy() != nullptr) {
            reinterpret_cast<PRX>(proxy())->remove(obj);
        }
    }
    
    virtual void setParent(Transformable *const& newPar);
    inline virtual void setParentDeleted() {
        _parentPtr = nullptr;
    }
    
    
    inline virtual void onSetParent() {};
    
    template<typename T>
    inline void _setParent(T * const& newPar) {
        if (newPar == _parentPtr) return;

        if (_parentPtr != nullptr) this->parent()->remove(this->self());
        _parentPtr = newPar;
        //We're only invoked by add, don't call recursivly
        //if (newPar != nullptr) this->parent()->add(this->self());
        this->onSetParent();
    }
    
    void clearParent(Transformable *const& newPar);
protected:
    void* _parentPtr;
    struct RelativeData {
        N2 position = N2(0.f);
        N2 size = N2(1.f);
        float rotation = 0.f;
    } _rel;
};

#endif /* TRANSFORMABLE_H */


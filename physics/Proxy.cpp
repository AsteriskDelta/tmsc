#include "Proxy.h"

int ARKE_DBGMODE = 0;

Proxy::Proxy() : Transformable(),  _children(), suspended(false) {
}

Proxy::~Proxy() {
    if(_parentPtr != nullptr) parent()->remove(this);
    //Unfriend everyone first, in case our children are on the list (and would be deleted before being unfriended)
    this->clearFriends();
    //Orphan children
    for(auto it = this->begin<Adapter>(); it != this->end<Adapter>(); ++it) {
        if(!it->onOrphaned()) delete &(*it);
    }
}


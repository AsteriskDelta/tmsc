#include "Damagable.h"

Damagable::Damagable() {
}

Damagable::Damagable(const Damagable& orig) : Adapter(orig) {
}

Damagable::~Damagable() {
}

void Damagable::applyDamage(const Damage& dmg, PhysicalEntity *src) {
    _unused(dmg); _unused(src);
}

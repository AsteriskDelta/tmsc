#ifndef UPDATABLE_H
#define UPDATABLE_H
#include "../TMSCInclude.h"

class Updatable : public virtual Adapter {
public:
    Updatable();
    Updatable(const Updatable& orig);
    virtual ~Updatable();
    
    virtual void update(float deltaTime);
private:

};

#endif /* UPDATABLE_H */



/* 
 * File:   Damage.h
 * Author: galen
 *
 * Created on April 2, 2017, 5:01 PM
 */

#ifndef DAMAGE_H
#define DAMAGE_H

class Damage {
public:
    Damage();
    Damage(const Damage& orig);
    virtual ~Damage();
private:

};

#endif /* DAMAGE_H */


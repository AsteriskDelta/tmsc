#include "Field.h"
#include "render/DebugGroup.h"
//#include "lib/inlDev.h"

Field::Field() : radii(0.f), attenuation(), direction(N2_UP), minDot(-2.f), fullDot(-2.f), occludable(false), 
        shadowsEnabled(false), shadowsIgnoreEntities(false), shadowThreshold(0.05f),currentlyOccluded(false), baseShape(nullptr), calcShadows(nullptr), occupants()
         {
    
}

Field::~Field() {
    if(calcShadows != nullptr) delete calcShadows;
    if(baseShape != nullptr) delete baseShape;
}

void Field::setRadius(NI r) {
    radii = N2(r);
}
void Field::setRadii(N2 r) {
    radii = r;
}
void Field::setOffset(N3 off) {
    attenuation.offset = off;
}
void Field::setCoeffs(NI lin, NI sq) {
    attenuation.linear = lin;
    attenuation.square = sq;
}

void Field::setFOV(NHP degrees, NHP falloffDegrees) {
    N2 def = ::nvx::rotate(N2_UP, degrees/2.f);
    minDot = dot(N2_UP, def);
    
    def = ::nvx::rotate(N2_UP,(degrees)/2.f - falloffDegrees);
    fullDot = dot(N2_UP, def);
    if(degrees >= 360) {
        minDot = fullDot = -2.f;//Avoid FPE issues
    }
    //dotFalloffMult = falloffMult;
}
void Field::setDirection(N2 dir) {
    direction = dir;
}

void Field::setShadowThreshold(NI thresh) {
    shadowThreshold = thresh;
}

void Field::setShadowsIgnoreEntities(bool state) {
    shadowsIgnoreEntities = state;
}

void Field::setOccludable(bool s) {
    occludable = s;
}

void Field::prepare() {
    //NI maxRadius = std::max(radii.z, std::max(radii.x, radii.y));
    cached.radiiSq = radii*radii;
    
    //Multiply the linear component so that linMult*x*lin + x^2*sq == 1 at x=1
    if(attenuation.linear == 0.f) cached.linearMult = 0.f;
    else cached.linearMult = (1.f - attenuation.square) / attenuation.linear;
    
    cached.linearFull = attenuation.linear * cached.linearMult;
    this->makeBaseShape();
    this->computeLocals();
}

void Field::update(float deltaTime) {
    if(this->trackOccupied()) {
        for(Entity *const& entity : occupants) {
            this->onOccupy(entity, deltaTime);
        }
    }
    
    debugGroup->clear(this);
    this->updateShadows();
    this->computeLocals();
    this->computeAgnostics();
    this->drawDebug(this->debugGroup);
}

bool Field::fieldContains(const N2& parentPos) const {
    if(parent() == nullptr) return this->fieldContainsAgnostic(this->localToAgnostic(parentPos));//return distance2(this->position(), parentPos) <= max(cached.radiiSq.x, cached.radiiSq.y);
    else return this->fieldContainsAgnostic(parent()->localToAgnostic(parentPos));
}
NI Field::fieldStrength(const N2& parentPos) const {
    /*if(!this->fieldContains(parentPos)) return 0.f;
    try {
        const N2 del = parentPos - this->position();
        const N2 delNorm = normalize(del);
        const NI yMix = fabs(dot(delNorm, N2_UP));
        const NI rad = (1.f-yMix) * radii.x + yMix * radii.y;
        const NI dist = length(del)/rad;
        
        return std::max(0.f, 1.f - dist*cached.linearFull - dist*dist*attenuation.square);
    } catch(...) {
        return 0.f;
    }*/
    if(parent() == nullptr) return this->fieldStrengthAgnostic(this->localToAgnostic(parentPos));//return distance2(this->position(), parentPos) <= max(cached.radiiSq.x, cached.radiiSq.y);
    else return this->fieldStrengthAgnostic(parent()->localToAgnostic(parentPos));
}

NI Field::getRelDist(const N2& parentPos, N2 *const& del) const {
    if(parent() == nullptr) return getRelDistAgnostic(parentPos, del);
    else return this->getRelDistAgnostic(parent()->localToAgnostic(parentPos), del);
}
NI Field::getRelDistAgnostic(const N2& agPos, N2 *const& outDel) const {
    const N2 del = agPos - this->localToAgnostic(N2(0.f));
    const N2 delNorm = normalize(del);
    const NI yMix = fabs(dot(delNorm, N2_UP));
    const NI rad = (1.f - yMix) * radii.x + yMix * radii.y;
    const NI dist = length(del)/rad;
    if(outDel != nullptr) (*outDel) = delNorm * rad;
    return dist;
}

bool Field::fieldContainsAgnostic(const N2& agPos) const {
    if(baseShape == nullptr) return false;
    return this->baseShape->contains(agPos);
    //return this->fieldStrengthAgnostic(agPos) > 0.001f;//return distance2(this->localToAgnostic(N2(0.f)), agPos) <= max(cached.radiiSq.x, cached.radiiSq.y);
}
NI Field::fieldStrengthAgnostic(const N2& agPos, N2 *const& outDel) const {
    if(!this->fieldContainsAgnostic(agPos)) return 0.f;
    try {
        N2 lDel;
        const NI dist = this->getRelDistAgnostic(agPos, &lDel);
        const NI rawDot = dot(direction, normalize(lDel));
        const NI dotFac = smoothstep(minDot, fullDot, rawDot);
        if(rawDot < minDot) return 0;

        NI str = 1.f - cached.linearFull - dist*dist*attenuation.square;
        str *= dotFac;
        //str *= smoothstep(NI(0), NI(0.1f)*dotFalloffMult, dotFac);//pow(dotFac, 0.5);

        if(outDel != nullptr) (*outDel) = lDel;
        return std::max(NI(0.f), str);
    } catch(...) {
        return 0.f;
    }
}

void Field::onEnter(Entity *entity) {
    if(this->trackOccupied()) {
        occupants.insert(entity);
    }
}
void Field::onExit(Entity *entity) {
    if(this->trackOccupied()) {
        occupants.erase(entity);
    }
}

void Field::onOccupy(Entity *entity, NI deltaTime) {
    _unused(entity);
    _unused(deltaTime);
}

void Field::setShadows(bool s) {
    if(!shadowsEnabled && s && baseShape == nullptr) this->makeBaseShape();
    shadowsEnabled = s;
}

void Field::updateShadows() {
    if(!shadowsEnabled) return;
    this->makeBaseShape();
    currentlyOccluded = false;
    PhysicsPoly::Viewpoint view;
    view.start = this->localToAgnostic(N2(0.f));
    N2 maxRad = N2(max(radii.x, radii.y)*2.f);
    const NI occludeThreshold = 0.3f;
    const NI occludeThresholdSq = occludeThreshold*occludeThreshold;
    
    if(debugGroup != nullptr) debugGroup->setOwner(this);
    else return;
    auto inters = this->getPotentialColliders(this);
    //std::cout << "[Field] Begin possible intersections (from " << VP(position()) << "), agBounds = " << VP(agnosticBounds()->nearPoint()) << " -> " << VP(agnosticBounds()->farPoint()) << ":\n";
    //std::cout << "\tGOT INTERS=" << inters.size() << "\n";
    //for (PhysicalObject* collider : inters) {
    for(auto it = inters.begin(); it != inters.end(); ++it) {
        PhysicalObject *collider = dynamic_cast<PhysicalObject*>(*it);
        //std::cout << "COLLIDER AT " << collider << ", isEntity=" << collider->isEntity() << " DC=" << _derive<Entity>(collider) << "\n";
        if(!collider->isComplete() || collider->agnosticPoly() == nullptr || collider->agnosticBounds() == nullptr) continue;
        else if(shadowsIgnoreEntities && collider->isEntity()) continue;
        
        //if(!collider->isEntity()) continue;
        PhysicsPoly *const cPoly = collider->agnosticPoly();
        //std::cout << "\t[Field] Got collider at " << collider << ", centroid= " << cPoly->centroid() << ", trueCenter=" << cPoly->centroid() << ", area= " << cPoly->area() << ", size=" << cPoly->size() << "\n";
        
        if(debugGroup != nullptr) debugGroup->drawPoint(cPoly->centroid(), ColorRGBA(255, 255, 0, 127));
        N2 centerDelta;
        const NI relDist = this->getRelDistAgnostic(cPoly->centroid(), &centerDelta);
        _unused(relDist);
        view.end = centerDelta + view.start;

        
        
        
        //std::cout << "\t\tbounds contained? " << collider->agnosticBounds()->contains(view.start)  << ", polySz=" << cPoly->size() << "\n";
        if(collider->agnosticBounds()->contains(view.start) && cPoly->contains(view.start)) {
            //std::cout << "\t\tCONTAINED\n";
            if(debugGroup != nullptr) debugGroup->drawPoint(view.start, ColorRGBA(0,0,255), 0.5f);
            if(occludable) {
                currentlyOccluded = true;
                return;
            }
        } else {
            PhysicsPoly::Projection proj = cPoly->projection(view);
            
            if(!proj) continue;
            else if(distance2(proj.points[0]->position(), view.start) < occludeThresholdSq || 
                    distance2(proj.points[1]->position(), view.start) < occludeThresholdSq) {
                if(occludable) {
                    currentlyOccluded = true;
                    return;
                } else continue;
            }
            //std::cout << "got pts " << proj.points[0] << ", " << proj.points[1] << "\n";
            //std::cout << "at " << proj.points[0]->position() << proj.points[1]->position() << "\n";
            //debugGroup->drawLine(view.start, proj.points[0]->position(), ColorRGBA(255), 0.05);
            //debugGroup->drawLine(view.start, proj.points[1]->position(), ColorRGBA(255), 0.05);
            
            //std::cout << "view was " << view.start << " -> " << view.end << "\n";
            Ray<N2> ray0(view.start, delnormalize(view.start, proj.points[0]->position())),
                    ray1(view.start, delnormalize(view.start, proj.points[1]->position()));
            
            //TODO::This segfaults for osme reason!?!?!
            auto inter0 = baseShape->intersect(ray0, true), inter1 = baseShape->intersect(ray1, true);
            if(!inter0 || !inter1) continue;
            debugGroup->drawPoint(inter0.start(), Colors::Red, 0.2f);
            debugGroup->drawPoint(inter1.start(), Colors::Red, 0.2f);
            
            //std::cout << "field got inters " << inter0 << " from ray " << ray0 << "\n";
            
            FieldPoly minusPoly;
            
            minusPoly.add(view.start + delnormalize(view.start, inter0.start()) * baseShape->radius() * 2);
            if(distance2(inter0.start(), inter0.far(baseShape).end()) != 0||true) minusPoly.add(inter0.start());
            for(PhysicsPt *pt = proj.points[0]->sibling(cPoly, proj.winding); pt != nullptr; pt = pt->sibling(cPoly, -proj.winding)) {
                PhysicsPt *nextPt = pt->sibling(cPoly, -proj.winding);
                /*bool isVisable = true;//baseShape->radialContains(pt->position(), view.start, false);
                N2 gNorm = normalize(pt->position() - view.start);
                
                if(isVisable) {
                    //If there's something between the view and us, we're occluded and should be culled
                    N2 interPoint;
                    bool occTest = cPoly->intersect(pt->position() - gNorm*cPoly->maxRadius(), pt->position(), interPoint, false, true);

                    if(occTest && distance2(interPoint, pt->position()) < 0.005f) {
                        borderPoints.push_back(pt);
                    } else if(occTest) {
                        //debugGroup->drawPoint(interPoint, ColorRGBA(255, 0, 255), 0.1f);
                    }
                } else {
                    //debugGroup->drawPoint(pt->position(), ColorRGBA(64, 110, 140), 0.3f);
                }*/
                auto castToPt = cPoly->intersect(Ray<N2>(view.start, del(view.start, pt->position())));
                if(castToPt.start() == pt->position()) minusPoly.add(pt->position());
                if(castToPt) debugGroup->drawLine(view.start, castToPt.start(), ColorRGBA(255, 0,0));
                if(nextPt == proj.points[1]) break;
            }
            if(distance2(inter1.start(), inter1.far(baseShape).end()) != 0||true) minusPoly.add(inter1.start());
            minusPoly.add(view.start + delnormalize(view.start, inter1.start()) * baseShape->radius() * 2);
            minusPoly.add(view.start + delnormalize(view.start, view.end) * baseShape->radius()*2);
            
            //baseShape->repair();
            std::list<FieldPoly::Idx> minusRm, baseRm;
            for(FieldPoly::Idx i = 0; i < minusPoly.size(); i++) {
                if(!baseShape->contains(minusPoly[i])) {
                    minusRm.push_back(i);
                    debugGroup->drawPoint(minusPoly.at(i), Colors::Yellow);
                }
            }
            FieldPoly::Idx baseInsAfter = std::min(inter0.farIdx, inter1.farIdx);
            char insDir = (baseInsAfter == inter0.farIdx)? 1 : 1;
            
            FieldPoly origMinusPoly = minusPoly;
            //origMinusPoly.repair();
            //origMinusPoly.decollinearize();
            minusPoly.removeList(minusRm);
            debugGroup->drawPoint(baseShape->at(baseInsAfter), Colors::Green, 0.15);
            debugGroup->drawPoint(baseShape->at(baseInsAfter+insDir), Colors::White, 0.15);
            //baseShape->insert(baseInsAfter-1, insDir, &minusPoly, 0, minusPoly.size()-1, 1);
            /*
            if(distance2(inter0.start(), baseShape->at(baseInsAfter)) < 0.01) {
                baseInsAfter--;
            }*/
            
            if(minusPoly.size() > 0) {
                FieldPoly::Idx iStart, iEnd; int iDel;
                if(false&& baseInsAfter == baseShape->size()-1) {
                    iStart = 0; iEnd = baseShape->size()-1; iDel = 1;
                } else {
                    iStart = minusPoly.size()-1; iEnd = 0; iDel = -1;
                }
                
                for(FieldPoly::Idx i = iStart; true; i += iDel) {
                    baseShape->insert(baseInsAfter, minusPoly.at(i), 1);
                    if(i == iEnd) break;
                }
            }
            
            for(FieldPoly::Idx i = 0; i < baseShape->size(); i++) {
                if(origMinusPoly.contains(baseShape->at(i), false)) {
                    baseRm.push_back(i);
                    debugGroup->drawPoint(baseShape->at(i), Colors::Magenta, 0.8);
                    //if(i == baseInsAfter) baseInsAfter--;
                    //if(i == max(inter0.farIdx, inter1.farIdx)) baseInsAfter++;;
                    //baseInsAfter = min(baseInsAfter, i);
                }// else debugGroup->drawPoint(baseShape->at(i), Colors::Cyan, 0.5);
            }
            baseShape->removeList(baseRm);
            baseShape->repair();
            
            //std::cout << "origSize: " << origMinusPoly.size() << "\n";
            if(!origMinusPoly.contains(inter0.start())) {
                std::cout << "DISCONT AT " << view.start << " -> " << inter0.start() << "\n";
                    debugGroup->drawPoint(inter0.start(), Colors::Cyan, 0.4);
                for(const auto& seg : origMinusPoly.segments()) {
                    if(seg.intersect(inter0.start())) std::cout << "intersects with " << seg << "\n";
                    else if(seg.within(inter0.start(), 0.5))std::cout << "\t close to " << seg << ", dist=" << seg.distance(inter0.start())<<"\n";
                }
            }
            if(!origMinusPoly.contains(inter1.start())) {
                std::cout << "DISCONT AT " << view.start << " -> " << inter1.start() << "\n";
                 debugGroup->drawPoint(inter1.start(), Colors::Cyan, 0.4);
                for(const auto& seg : origMinusPoly.segments()) {
                    if(seg.intersect(inter1.start())) std::cout << "intersects with " << seg << "\n";
                    else if(seg.within(inter1.start(), 0.5)) std::cout << "\t close to " << seg << ", dist=" << seg.distance(inter1.start())<<"\n";
                }
            }
            /*
            for(const auto& seg : origMinusPoly.segments()) {
                debugGroup->drawLine(seg.start(), seg.end(), Colors::Blue);
                //std::cout << "seg from " << seg.start() << " to " << seg.end() << "\n";
            }*/
            
            //baseShape->subtract(&minusPoly);
            
            //debugGroup->drawLine(view.start, inter0.start(), ColorRGBA(255), 0.05);
            //debugGroup->drawLine(view.start, inter1.start(), ColorRGBA(255), 0.05);
            
            /*else if(!baseShape->radialContains(proj.points[0]->position(), view.start, true) && 
                    !baseShape->radialContains(proj.points[1]->position(), view.start, true) ) continue;*/
            //std::cout << "\t\tgot intersection points at " << VP(proj.intersections[0]) << " and " << VP(proj.intersections[1]) << "\n";
            /*
            N2 interNorms[2] = {normalize(proj.intersections[0] - view.start), normalize(proj.intersections[1]-view.start)};
            N2 relRads[2]; unsigned int interIndicesA[2] = {0,0}, interIndicesB[2] = {0,0};
            NI interStrs[2] = {this->fieldStrengthAgnostic(proj.points[0]->position(), &relRads[0]), this->fieldStrengthAgnostic(proj.points[1]->position(), &relRads[1])};
            if(std::max(interStrs[0], interStrs[1]) < shadowThreshold || distance2(proj.intersections[0], proj.intersections[1]) < 0.002f) continue;
            */
            ColorRGBA lineCol = ColorRGBA(255);//ColorRGBA(round(max(interStrs[0], interStrs[1])*255.f));
            //if(in lineCol = ColorRGBA(255,0,0);
            /*N2 inter2[2][2];
            baseShape->intersect(view.start+ interNorms[0]*maxRad, view.start, inter2[0][0], inter2[0][1], false, true, &interIndicesA);
            baseShape->intersect(view.start+ interNorms[1]*maxRad, view.start, inter2[1][0], inter2[1][1], false, true, &interIndicesB);
            
            int insDir = 1;
            unsigned int baseInterNext = interIndicesA[0], baseInterLast = interIndicesB[0];
            N2 cenDir;
            if(dot(interNorms[0], interNorms[1]) < -0.998) cenDir = perp(interNorms[0]);//Nearly co-linear, it's anyone's guess
            else cenDir = normalize(interNorms[0] + interNorms[1]);
            N2 perpDir = perp(cenDir);
            FieldPoly minusPoly;
            
            //We assume non-total occlusion from a single object, since this would require superposition
            const NI mRad = max(radii.x, radii.y) * 3.f;
            //mRad *= mRad;//For production, makes debugging difficult
            
            minusPoly.add(FieldPt(view.start + cenDir * mRad, cenDir));
            const NI perpSgn1 = NI(sgn(dot(perpDir, interNorms[0]))),
                    perpSgn2 = NI(sgn(dot(perpDir, interNorms[1])));
            
            if(dot(cenDir, interNorms[0]) < 0.f) minusPoly.add(FieldPt(view.start + perpDir * mRad * perpSgn1, perpDir * perpSgn1) );
            minusPoly.add(FieldPt(view.start + interNorms[0]*mRad, interNorms[0]));
            
            //Causes shadow self-intersection
            //minusPoly.add(FieldPt(view.start, -cenDir));
            minusPoly.add(FieldPt(proj.points[0]->position(), -cenDir));
            minusPoly.add(FieldPt(proj.points[1]->position(), -cenDir));
            
            minusPoly.add(FieldPt(view.start + interNorms[1]*mRad, interNorms[1]));
            if(dot(cenDir, interNorms[1]) < 0.f) minusPoly.add(FieldPt(view.start + perpDir * mRad * perpSgn2, perpDir * perpSgn2) );
            minusPoly.repair();
            
            for(unsigned int i = 0; i < minusPoly.size(); i++) {
                FieldPt *const a = minusPoly.get(i), *const b = minusPoly.get(i+1);
                debugGroup->drawLine(a->position(), b->position(), ColorRGBA(0, 0, 255, 127), 0.05);
            }
            
            
            
            baseShape->repair();
            std::list<PhysicsPt*> borderPoints;
            for(PhysicsPt *pt = proj.points[0]; pt != nullptr; pt = pt->sibling(-proj.winding)) {
                bool isVisable = baseShape->radialContains(pt->position(), view.start, false);
                N2 gNorm = normalize(pt->position() - view.start);
                
                if(isVisable) {
                    //If there's something between the view and us, we're occluded and should be culled
                    N2 interPoint;
                    bool occTest = cPoly->intersect(pt->position() - gNorm*cPoly->maxRadius(), pt->position(), interPoint, false, true);

                    if(occTest && distance2(interPoint, pt->position()) < 0.005f) {
                        borderPoints.push_back(pt);
                    } else if(occTest) {
                        //debugGroup->drawPoint(interPoint, ColorRGBA(255, 0, 255), 0.1f);
                    }
                } else {
                    //debugGroup->drawPoint(pt->position(), ColorRGBA(64, 110, 140), 0.3f);
                }
                if(pt == proj.points[1]) break;
            }
            
            */
            //Delete pts contained in minusPoly
            //bool containsInter1 = baseShape->radialContains(inter2[0][1], view.start, false),//, ||*/ baseShape->contains(inter2[0][1]), 
            //        containsInter2 = baseShape->radialContains(inter2[1][1], view.start, false);//|| */baseShape->contains(inter2[1][1]);
            /*for(int i = baseShape->size()-1; i >= 0; i-- ) {
                FieldPt *const pt = baseShape->get(i);
                if(minusPoly.contains(pt->position())) {
                    debugGroup->drawPoint(pt->position(), ColorRGBA(255, 0, 255), 0.22f);
                    baseShape->remove(i);
                    //baseShape->repair();
                    baseInterNext = i - 1;
                    //i--;//Deincrement to skip over deleted item
                } else {
                    debugGroup->drawPoint(pt->position(), ColorRGBA(0, 255, 0));
                }
            }*/
            
            //NOT IN USE
            /*bool deletePFirst = dot(normalize(baseShape->get(baseInterNext+1)->position() - view.start), cenDir) > 
            dot(normalize(inter2[0][1] - view.start), cenDir);
            
            bool deletePLast = dot(normalize(baseShape->get(baseInterLast)->position() - view.start), cenDir) > 
            dot(normalize(inter2[1][1] - view.start), cenDir);
            
                debugGroup->drawPoint(baseShape->get(baseInterNext+1)->position(), ColorRGBA(255, 255, 0), 0.2f);
                debugGroup->drawPoint(baseShape->get(baseInterNext-1)->position(), ColorRGBA(0, 0, 255), 0.2f);
                debugGroup->drawPoint(baseShape->get(baseInterNext)->position(), ColorRGBA(0, 255, 0), 0.2f);
                debugGroup->drawPoint(baseShape->get(baseInterLast)->position(), ColorRGBA(255, 0, 0), 0.3f);
                debugGroup->drawLine(view.start, view.start + cenDir*20.f, ColorRGBA(255));
                
                debugGroup->drawLine(view.start, view.start + normalize(baseShape->get(baseInterNext+1)->position() - view.start)*20.f, ColorRGBA(0,0,255));
                
                debugGroup->drawLine(view.start, view.start + normalize(baseShape->get(baseInterLast)->position() - view.start)*20.f, ColorRGBA(0,255,255));
            
                if(deletePFirst || deletePLast) {
                    unsigned int delFirst, delLast; int delDir = deletePFirst? 1 : -1;
                    if(delDir < 0) {
                        delFirst = baseInterLast;
                        delLast = baseInterNext+1;
                    } else {
                        delFirst = baseInterNext+1;//deletePFirst? baseInterNext+1 : baseInterNext+2;
                        delLast = deletePLast? baseInterLast : baseInterLast-1;
                    }
                    
                    //if(delFirst != baseInterLast) {
                        baseShape->removeRange(delFirst, delLast, delDir);
                   // }
                    
                }*/
                /*
            NI insDotP = dot(cenDir, normalize(baseShape->get(baseInterNext+1)->position() - view.start)),
                  insDotN = dot(cenDir, normalize(baseShape->get(baseInterNext-1)->position() - view.start));
                 * */
            /*if(int(interIndicesA[0]) < int(interIndicesB[0])) {
                baseInterNext = interIndicesB[0];
                baseInterLast = interIndicesA[0];
                insDir = -1;
            }*/
            //if(insDotN > insDotP) insDir = -1;
            /*if(insDotN > 0.f && insDotP < 0.f) {
                baseInterNext = interIndicesB[0];
                baseInterLast = interIndicesA[0];
            }
            if(interIndicesA[0] != interIndicesB[0])// && abs(interIndicesA[0] - interIndicesB[0]) > 1&& abs(interIndicesA[1] - interIndicesB[1]) != baseShape->size()-1) {//Base has at least one vertex between the two intersections, remove them
                //baseShape->removeRange(int(baseInterNext)+insDir, int(baseInterLast), insDir);
            }*/
            //Hint new normals
            /*if(containsInter1) {
                baseInterNext = baseShape->insert(baseInterNext, FieldPt(inter2[0][1], interNorms[0]), insDir);
                baseInterNext = baseShape->insert(baseInterNext, FieldPt(proj.points[0]->position(), cenDir), insDir);
            } else if (false) {
                N2 transferNorm = normalize(baseShape->get(baseInterNext)->position() - view.start), transferInter;
                bool transferOK = cPoly->intersect(view.start, view.start + transferNorm*50.f, transferInter, false, true);
                if(transferOK) {
                    baseInterNext = baseShape->insert(baseInterNext, FieldPt(transferInter, interNorms[0]), insDir);
                    debugGroup->drawLine(view.start, view.start + transferNorm * 50.f, ColorRGBA(255, 255, 255));
                } else {
                    debugGroup->drawLine(view.start, view.start + transferNorm * 50.f, ColorRGBA(255, 255, 0));
                }
            }
            //if(proj.winding == 0) continue;
            for(PhysicsPt* pt : borderPoints) {
                PhysicsPt *next = pt->sibling(-proj.winding);
                if(next == nullptr) continue;
                
                //baseInterNext = baseShape->insert(baseInterNext, FieldPt(pt->position(), -(pt->normal())), insDir);
                
                N2 p1 = pt->position(), p2 = next->position();
                //if(debugGroup != nullptr) debugGroup->drawLine(p1, p2, ColorRGBA(255, 0, 80), 0.1f);
            }
            
            if(containsInter2) {
                baseInterNext = baseShape->insert(baseInterNext, FieldPt(proj.points[1]->position(), cenDir), insDir);
                baseInterNext = baseShape->insert(baseInterNext, FieldPt(inter2[1][1], interNorms[1]), insDir);
            }
            baseShape->repair();
            
            if(debugGroup != nullptr) {
                
                //debugGroup->drawPoint(baseShape->get(interIndicesA[1])->position(), ColorRGBA(0,255,0));
                //debugGroup->drawPoint(baseShape->get(interIndicesA[0])->position(), ColorRGBA(0,255,120));
                //debugGroup->drawPoint(baseShape->get(interIndicesB[1])->position(), ColorRGBA(0,255,0));
                //debugGroup->drawPoint(baseShape->get(interIndicesB[0])->position(), ColorRGBA(0,255,120));
                
                //debugGroup->drawPoint(view.start+relRads[0], ColorRGBA(255,0,0));
                //debugGroup->drawPoint(view.start+relRads[1], ColorRGBA(255,0,0));


                debugGroup->drawPoint(proj.intersections[0], ColorRGBA(255));
                debugGroup->drawPoint(proj.intersections[1], ColorRGBA(255));

                //debugGroup->drawLine(proj.points[0]->position(), view.start, lineCol);
                //debugGroup->drawLine(proj.points[1]->position(), view.start, lineCol);
                //debugGroup->drawLine(inter2[0][1], proj.points[0]->position(), ColorRGBA(127));
                //debugGroup->drawLine(inter2[1][1], proj.points[1]->position(), ColorRGBA(127));

                debugGroup->drawPoint(inter2[0][1], ColorRGBA(255, 0, 255));
                debugGroup->drawPoint(inter2[1][1], ColorRGBA(255, 0, 255));
                //debugGroup->drawPoint(inter2[0][0], ColorRGBA(255, 0, 255));
                //debugGroup->drawPoint(inter2[1][0], ColorRGBA(255, 0, 255));

                debugGroup->drawPoint(proj.points[0]->position(), ColorRGBA(0, 255, 255));
                debugGroup->drawPoint(proj.points[1]->position(), ColorRGBA(0, 255, 255));
            }
            
            if(proj.winding == 0) continue;
            for(PhysicsPt *pt = proj.points[0]; pt != nullptr && pt != proj.points[1]; pt = pt->sibling(-proj.winding)) {
                PhysicsPt *next = pt->sibling(-proj.winding);
                if(next == nullptr) continue;
                
                N2 p1 = pt->position(), p2 = next->position();
                //if(debugGroup != nullptr) debugGroup->drawLine(p1, p2, ColorRGBA(255, 0, 80), 0.1f);
            }*/
            
            //bool shouldReduce = 
        }
    }
    
    if(debugGroup != nullptr) {
        for(unsigned int i = 0; i < baseShape->size(); i++) {
            FieldPt *const a = & baseShape->get(i), *const b = & baseShape->get(i+1);
            debugGroup->drawLine(a->position(), b->position(), ColorRGBA(255, 0, 0, 127), 0.05);
            //debugGroup->drawPoint(a->position(), ColorRGBA(int(round(NI(i+1)/NI(baseShape->size()) * 255.f)), 255, 255, 255), 0.2f);
        }
    }
}

void Field::makeBaseShape() {
    if(baseShape == nullptr) baseShape = new FieldPoly();
    else baseShape->clear();
    
    N2 ll, ur;
    if(this->minDot <= 0.f) {
        ll = N2(-radii.x, -radii.y);
        ur = N2(radii.x, radii.y);
    } else {
        const NI yMix = fabs(dot(direction, N2_UP));
        const NI effRadius = (1.f - yMix) * radii.x + yMix * radii.y;
        N2 extant = direction * effRadius * 1.1f;//Compensate for smoothing in shader, avoid visual glitches
        
        try {
            NI theta = acos(minDot);
            N2 sideVecs[2] = {rotate(extant, theta), rotate(extant, -theta)};
            Bounds<N2> tmpBounds;
            tmpBounds.add(N2(0.f));
            tmpBounds.add(extant);
            tmpBounds.add(sideVecs[0]);
            tmpBounds.add(sideVecs[1]);
            
            /*if(minDot < 0.f) {
                N2 dCross = perp(extant);
                tmpBounds.add(dCross);
                tmpBounds.add(-dCross);
            }*/
            
            const NI dotX = dot(N2_RIGHT, direction), dotY = dot(N2_UP, direction);
            if(fabs(dotX) > minDot) tmpBounds.add(N2(radii.x, 0.f) * (dotX >= 0.f? 1.f : -1.f));
            if(fabs(dotY) > minDot) tmpBounds.add(N2(0.f, radii.y) * (dotY >= 0.f? 1.f : -1.f));
            
            ll = tmpBounds.nearPoint();
            ur = tmpBounds.farPoint();
        } catch(...) {};
    }

    baseShape->add(this->localToAgnostic(N2(ll.x, ll.y)));
    baseShape->add(this->localToAgnostic(N2(ll.x, ur.y)));
    baseShape->add(this->localToAgnostic(N2(ur.x, ur.y)));
    baseShape->add(this->localToAgnostic(N2(ur.x, ll.y)));
    baseShape->repair();
}

FieldPoly* Field::getBaseShape() {
    if(baseShape == nullptr) this->makeBaseShape();
    return baseShape;
}

void Field::computeLocals() {
    SpriteUser::computeLocals();
    
    if(!shadowsEnabled) {
        phyLocalBounds.add(N2(-radii.x, -radii.y));
        phyLocalBounds.add(N2( radii.x,  radii.y));
    } else if(baseShape != nullptr) {
        phyLocalBounds.add(this->agnosticToLocal(baseShape->bounds.nearPoint()));
        phyLocalBounds.add(this->agnosticToLocal(baseShape->bounds.farPoint()));
    }
}

void Field::drawDebug(DebugGroup *dbg) {
    
    //Entity::drawDebug(dbg);
}


/* 
 * File:   GravityField.h
 * Author: galen
 *
 * Created on April 15, 2017, 1:07 PM
 */

#ifndef GRAVITYFIELD_H
#define GRAVITYFIELD_H

class GravityField {
public:
    GravityField();
    GravityField(const GravityField& orig);
    virtual ~GravityField();
private:

};

#endif /* GRAVITYFIELD_H */


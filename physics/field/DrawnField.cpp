#include "DrawnField.h"
#include <ARKE/QuadGroup.h>
#include "render/FieldVert.h"

DrawnField::DrawnField(int rPass) : Field(), centerColor(255), edgeColor(0), group(nullptr), hasEdgeColor(false), targetRenderPass(rPass) {
    Renderable::registerForPass(rPass);
}

DrawnField::~DrawnField() {
}

void DrawnField::prepare() {
    Field::prepare();
    //this->update();
}

void DrawnField::update(float deltaTime) {
    //std::cout << "DrawnField requesting prerender...\n";
    Field::update(deltaTime);
    Renderable::needsPrerender();
}

void DrawnField::setColor(ColorRGBA cen, ColorRGBA edge) {
    centerColor = cen;
    if(edge != ColorRGBA(0)) edgeColor = edge;
    else {
        edgeColor = centerColor;
        (edgeColor.alphaRef()) = 0;
    }
}
    
bool DrawnField::prerender(int pass) {
    //std::cout << "DrawField::prerender()\n";
    if(!Renderable::prerender(pass)) return false;
    
    this->meshPolygon();
    //std::cout << "DrawnField got prerender, requesting upload...\n";
    Renderable::needsUpload();
    
    return true;
}
bool DrawnField::uploadRender(int pass) {
    if(!Renderable::uploadRender(pass)) return false;
    
    if(group != nullptr) group->upload();
    return true;
}
bool DrawnField::render(int pass) {
    if(!Renderable::shouldRender(pass)) return false;
    
    _unused(pass);
    if(shader() == nullptr) {
        Console::Warn("DrawnField called with null shader (not from a complete derived class!)");
        return false;
    } else if(group == nullptr) {
        Console::Warn("DrawnField::render() called with null group!");
        return false;
    }
    
    if(!this->currentlyOccluded) {
        riEnsureState();
        //Renderable::PushMatrix(this->renderMatrix(true));
        this->shader()->activate();
        shader()->bindColor(centerColor, 8);
        shader()->bindColor(edgeColor, 9);
        shader()->bindV2(N2(cached.linearFull, attenuation.square), 10);
        shader()->bindV3(V3(V2(radii), 0.f), 11);
        shader()->bindV3(V3(attenuation.offset), 12);
        shader()->bindV3(V3(V2(this->position()), 0.f), 13);
        shader()->bindV2(V2(direction), 14);
        shader()->bindV2(N2(minDot, fullDot), 15);
        //shader()->bindV3(V3(this->position(), 0.f), 13);

        riEnsureState();
        group->draw();
        riEnsureState();

        this->shader()->deactivate();
        riEnsureState();
    }
    //Renderable::PopMatrix();
    
    return Renderable::render(pass);
}

void DrawnField::meshPolygon() {
    FieldPoly *const poly = this->polygon();
    if(poly == nullptr) {
        Console::Err("DrawnField::meshPolygon() called with null poly!");
        return;
    }
    if(group == nullptr) group = new GroupT((poly->size()+1)*3, true, true);
    else group->clear();
    
    const V3 cenPos = V3(0.f);//V3(this->position(), 0.f);
    //std::cout << "building group for poly of size " << poly->size() << "\n";
    for(unsigned int i = 0; i < poly->size(); i++) {
        FieldPt *const cur = &poly->get(i);
        FieldPt *const next = cur->right(poly);
        if(next == nullptr) continue;
        
        GroupT::Face *face = (group->add());
        
        FieldVert *vert = &(face->verts[0]);
        vert->position = V3(V2(this->localToRender(N2(0.f))), 0.f);
        vert->texCoords = N2T<Uint16, 1>(0);
        vert->tint = ColorRGBA(255);
        
        //std::cout << "\t\tpushing verts cen=" <<  VP(this->localToRender(N2(0.f))) << ", " << VP(this->localToRender(cur->pt)) << " from " << VP(this->position()) << "\n";
        
        vert = &(face->verts[1]);
        vert->position = V3(V2(cur->position()), 0.f);// + cenPos;
        vert->texCoords = N2T<Uint16, 1>(0);
        vert->tint = ColorRGBA(255);
        
        vert = &(face->verts[2]);
        vert->position = V3(V2(next->position()), 0.f);// + cenPos;
        vert->texCoords = N2T<Uint16, 1>(0);
        vert->tint = ColorRGBA(255);
        /*
        std::cout << "\tadding face for " << i << "\n";
        std::cout << "\t\t0: " << glm::to_string(face->verts[0].pos) << "\n";
        std::cout << "\t\t1: " << glm::to_string(face->verts[1].pos) << "\n";
        std::cout << "\t\t2: " << glm::to_string(face->verts[2].pos) << "\n";*/
    }
}
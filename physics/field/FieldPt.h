#ifndef FIELDPT_H
#define FIELDPT_H
#include "../../TMSCInclude.h"
#include <NVX/NPolygon.h>

class FieldPt_Base : public NPolygonPtBase<N2, true> {
public:
    inline FieldPt_Base() : NPolygonPtBase<N2, true>() {};
    inline FieldPt_Base(const FieldPt_Base& o) : NPolygonPtBase<N2, true>(o) {};
    inline FieldPt_Base(const N2& p, const N2& n = N2(0)) : NPolygonPtBase<N2, true>(p, n) {};
    /*typedef NPolygon<N2, FieldPt> Poly;
    typedef NPolygonPt<N2, FieldPt> PolyPt;
    typedef FieldPt Self;
    
    N2 pt;
    N2 norm;

    inline FieldPt () : PolyPt() {};
    FieldPt (const FieldPt& o);
    FieldPt (Poly *par, const N2& p, const N2& n = N2(0.f));
    FieldPt (const N2& p, const N2& n);
    virtual ~FieldPt();

    inline virtual const N2& positionConst() const override {
        return this->pt;
    };
    inline virtual N2& position() override {
        return const_cast<N2&>(this->positionConst());
    };
    inline virtual const N2& normalConst() const {
        return this->norm;
    };
    
    inline virtual N2& normal() {
        return const_cast<N2&>(this->normalConst());
    };
    inline virtual void setNormal(const N2& n) {
        (this->normal()) = n;
    }
    
    inline Poly *parent() const {
        return reinterpret_cast<Poly*>(this->parentPtr);
    };
    inline Self* sibling(int delta) const {
        if (this->parentPtr == nullptr) return nullptr;
        return this->parent()->sibling(this, delta);
    }

    inline Self* right(int del = 1) const {
        return this->sibling(del);
    };

    inline Self* left(int del = 1) const {
        return this->sibling(-del);
    };*/
private:

};

typedef NPolygonPt<N2, FieldPt_Base> FieldPt;
typedef NPolygon<N2, FieldPt> FieldPoly;

#endif /* FIELDPT_H */


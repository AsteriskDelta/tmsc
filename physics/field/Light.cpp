#include "Light.h"

Shader *Light::lightShader = nullptr;

void Light::LoadShader() {
    lightShader = Shader::Load("prgm/lightField.prgm");
}

Light::Light() : DrawnField(this->renderPass()) {
    if(lightShader == nullptr) LoadShader();
}

Light::~Light() {
}



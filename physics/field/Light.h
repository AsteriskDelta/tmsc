#ifndef LIGHT_H
#define LIGHT_H
#include "../../TMSCInclude.h"
#include "Field.h"
#include "DrawnField.h"
#include <ARKE/Color.h>

class Light : public virtual Field, public virtual DrawnField {
public:
    Light();
    virtual ~Light();
    
    inline virtual int renderPass() const override { return Renderable::Pass::Lighting; };
    
    
    inline virtual Shader *shader() const {
        return lightShader;
    }
protected:
    static Shader *lightShader;
    static void LoadShader();
};

typedef Light LightField;

#endif /* LIGHT_H */


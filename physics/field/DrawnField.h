#ifndef DRAWNFIELD_H
#define DRAWNFIELD_H
#include "../../TMSCInclude.h"
#include "Field.h"
#include "../../render/Renderable.h"
#include <ARKE/Color.h>

class Shader;
template<typename T, int C> class FaceGroup;
struct FieldVert;

class DrawnField : public virtual Field, public virtual Renderable {
public:
    DrawnField(int rendPass);
    virtual ~DrawnField();
    
    virtual void update(float deltaTime) override;
    virtual void prepare() override;
    
    void setColor(ColorRGBA cen, ColorRGBA edge = ColorRGBA(0));
    
    inline virtual int renderPass() const override { return targetRenderPass; };
    virtual bool prerender(int pass) override;
    virtual bool uploadRender(int pass) override;
    virtual bool render(int pass) override;
    
    ColorRGBA centerColor, edgeColor;
    
    typedef FaceGroup<FieldVert, 3> GroupT;
    GroupT* group;
protected:
    bool hasEdgeColor;
    int targetRenderPass;
    inline virtual Shader *shader() const { return nullptr; };
    virtual void meshPolygon();
};

#endif /* DRAWNFIELD_H */


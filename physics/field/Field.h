#ifndef FIELD_H
#define FIELD_H
#include "../../TMSCInclude.h"
#include "FieldPt.h"
#include "../../physics/PhysicalEntity.h"
#include "../../entity/Entity.h"
#include "../../physics/Updatable.h"
#include <ARKE/V3.h>
#include <unordered_set>

class Field : public virtual Entity, public virtual Updatable, public virtual Adapter {
public:
    Field();
    virtual ~Field();
    
    N2 radii;
    struct {
        N3 offset = N3(0.f);
        NI square = 0.f;
        NI linear = 1.f;
    } attenuation;
    N2 direction;
    NHP minDot, fullDot;
    bool occludable;
    
    virtual void setRadius(NI r);
    virtual void setRadii(N2 r);
    virtual void setOffset(N3 off);
    virtual void setCoeffs(NI lin, NI sq);
    virtual void setFOV(NHP degrees, NHP falloffDegrees = 0.f);
    virtual void setDirection(N2 dir);
    virtual void setShadowThreshold(NI thresh);//As minimum strength, ie, 0->1
    virtual void setShadowsIgnoreEntities(bool state = true);
    virtual void setOccludable(bool s);
    
    virtual void prepare();
    virtual void update(float deltaTime) override;
    virtual void drawDebug(DebugGroup *dbg) override;
    
    inline virtual Sprite* sprite() const override { return nullptr; };
    inline virtual PhysicsPoly* poly() const override { return nullptr; };
    inline virtual bool isComplete() const override { return true; };
    virtual void computeLocals() override;
    
    virtual bool fieldContains(const N2& parentPos) const;
    virtual NI fieldStrength(const N2& parentPos) const;
    
    virtual bool fieldContainsAgnostic(const N2& agPos) const;
    virtual NI fieldStrengthAgnostic(const N2& agPos, N2 *const& outDel = nullptr) const;
    
    virtual NI getRelDist(const N2& parentPos, N2 *const& del = nullptr) const;
    virtual NI getRelDistAgnostic(const N2& agPos, N2 *const& del = nullptr) const;
    
    virtual void onEnter(Entity *entity);
    virtual void onExit(Entity *entity);
    
    inline virtual bool trackOccupied() const { return false; };
    virtual void onOccupy(Entity *entity, NI deltaTime);
    
    virtual void setShadows(bool s = true);
    inline virtual bool hasShadows() const { return false; };
    virtual void updateShadows();
protected:
    virtual void makeBaseShape();
    virtual FieldPoly* getBaseShape();
    
    inline virtual FieldPoly* polygon() const {
        //if(this->hasShadows() && calcShadows != nullptr) return calcShadows;
        //else return baseShape;
        return baseShape;
    }
    
    struct {
        N2 radiiSq;
        NI linearMult, linearFull;
    } cached;
    bool shadowsEnabled, shadowsIgnoreEntities;
    NI shadowThreshold;
    
    bool currentlyOccluded;
    
    FieldPoly *baseShape, *calcShadows;
    std::unordered_set<Entity*> occupants;
};

#endif /* FIELD_H */


#ifndef PHYSICALENTITY_H
#define PHYSICALENTITY_H
#include "../TMSCInclude.h"
#include "PhysicalObject.h"
#include "Transformable.h"
#include "Damagable.h"
#include "Updatable.h"
#include <ARKE/V3.h>
#include <ARKE/Quat.h>
#include <ARKE/Transform.h>

class MapSector;

class PhysicalEntity : public virtual PhysicalObject, public virtual Transformable, 
        public virtual Damagable, public virtual Updatable, public virtual Adapter {
public:
    PhysicalEntity();
    virtual ~PhysicalEntity();
    
    V3 velocity;
    Quat angularVelocity;
    /*
    inline virtual Transform transform() {
        return Transform(position(), rotation());
    }*/
    
    //virtual V3 parentPosition() const;
    
    virtual void update(float rawDeltaTime);
    
    virtual void terminate();
    inline virtual bool isEntity() const override { return true; };
    
    //virtual void setSector(MapSector *sec);
    //virtual void clearSector(MapSector *sec);
    
    _hasPhysicalEntity();
protected:
    //MapSector *sector;
};

#endif /* PHYSICALENTITY_H */


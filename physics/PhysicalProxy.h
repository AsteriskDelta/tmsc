#ifndef PHYSICALPROXY_H
#define PHYSICALPROXY_H
#include "../TMSCInclude.h"
#include "Proxy.h"
#include "PhysicalObject.h"

class PhysicalProxy : public virtual Proxy, public virtual PhysicalObject, public virtual Adapter{
public:
    inline PhysicalProxy() {};
    inline virtual ~PhysicalProxy() {};
    
    virtual void computeLocals() override;
    virtual void computeAgnostics() override;
    virtual void getPotentialCollidersRecurse(const Bounds<N2> &objAgBounds, std::unordered_set<PhysicalObject*> *const & ret, PhysicalObject *const& maskObj = nullptr) override;
private:

};

#endif /* PHYSICALPROXY_H */


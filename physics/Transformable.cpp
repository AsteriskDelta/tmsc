#include "Transformable.h"
#include "Proxy.h"
//#include <glm/gtx/matrix_transform_2d.hpp>
//#include <glm/gtx/transform.hpp>

void Transformable::setParent(Transformable * const& newPar) {
    _setParent<Transformable>(newPar);
}

void Transformable::clearParent(Transformable * const& newPar) {
    if (Transformable::parent() != newPar) return;
    this->_parentPtr = nullptr;
    //Transformable::_setParent<Transformable>(nullptr);
}

N2 Transformable::parentToLocal(N2 pos) const {
    pos -= positionConst();
    const auto rot = rotationConst();
    if(rot != 0) pos = rotate(pos, -rot);
    pos *= inverseScale();
    
    return pos;
}
N2 Transformable::localToParent(N2 pos) const {
    //std::cout << "\t\t\tlclParent: " << VP(pos);
    pos *= scale();
    const auto rot = rotationConst();
    if(rot != 0) pos = rotate(pos, rot);
    pos += positionConst();
    //std::cout << " gave transformation " << VP(pos) << "\n";
    //std::cout << "\t\t\t\tfrom scale() = " << VP(scale()) << " via " << VP(spanConst()) << " / " << VP(internalSpan()) << "\n";
    return pos;
}
/*
Proxy* Transformable::proxy() { 
    return nullptr;
};*/
/*
Mat3 Transformable::matrix(bool recursive) const {
    Mat3 mat;// = translate(position) * mat4_cast(rotation) *  scale(ratio);
    mat = translate(mat, this->positionConst());
    mat = rotate(mat, this->rotationConst());
    mat = scale(mat, this->scale());
    
    if(recursive && this->parent() != nullptr) return this->parent()->matrix(true) * mat;
    else return mat;
}

Mat3 Transformable::invMatrix(bool recursive) const {
    return inverse(this->matrix(recursive));
}*/
Mat4 Transformable::renderMatrix(bool recursive) const {
    Mat4 mat = glm::translate(V3(V2(this->positionConst()), 0.f)) * 
            glm::rotate(float(this->rotationConst()), V3(0.f, 0.f, 1.f)) * 
            glm::scale(V3(V2(this->scale()), 1.f));
    //mat = translate(mat, this->positionConst().x, this->positionConst().y, 0.f);
    //mat = rotate(mat, 0.f, 0.f, this->rotationConst());
    //mat = scale(mat, this->scale().x, this->scale().y, 1.f);
    
    if(recursive && this->parent() != nullptr) return this->parent()->renderMatrix(true) * mat;
    else return mat;
}
Mat4 Transformable::invRenderMatrix(bool recursive) const {
    return inverse(this->renderMatrix(recursive));
}

N2 Transformable::localToAgnostic(N2 locPos) const {
    //std::cout << "locAG: " << VP(locPos) << "\n";
    /*if(this->isLinearTransform()&&false) {
        const N2 ret = N2(this->matrix(true) * V3(locPos, 0.f));
        //std::cout << "using matrix... " << VP(ret) << "\n";
        return ret;
    }*/
    //std::cout << "locAG: " << VP(locPos) << ", par=" << VP(this->localToParent(locPos)) << "\n";
    //std::cout << "\tvia pos=" << VP( positionConst()) << ", scale=" << VP(scale()) << "\n";
    if(this->parent() == nullptr) return this->localToParent(locPos);
    else return parent()->localToAgnostic(this->localToParent(locPos));
}
N2 Transformable::agnosticToLocal(N2 agPos) const {
    //if(this->isLinearTransform()&&false) return N2(this->invMatrix(true) * V3(agPos, 0.f));
    
    if(this->parent() == nullptr) return this->parentToLocal(agPos);
    else {
        agPos = parent()->agnosticToLocal(agPos);
        return this->parentToLocal(agPos);
    }
}

N2 Transformable::localNormToAgnostic(N2 locStart, N2 locNorm) const {
    N2 tStart = this->localToAgnostic(locStart);
    N2 tEnd = this->localToAgnostic(locStart+locNorm);
    if(tStart == tEnd) return N2(0.f);
    else return normalize(tEnd - tStart);
}

N2 Transformable::localToRender(N2 locPos) const {
    return this->localToAgnostic(locPos);
}
N2 Transformable::renderToLocal(N2 pos) const {
    return this->agnosticToLocal(pos);
}
N2 Transformable::localNormToRender(N2 locStart, N2 locNorm) const {
    return this->localNormToAgnostic(locStart, locNorm);
}
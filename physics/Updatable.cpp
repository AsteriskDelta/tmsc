#include "Updatable.h"
#include "Proxy.h"

Updatable::Updatable() {
}

Updatable::Updatable(const Updatable& orig) : Adapter(orig) {
}

Updatable::~Updatable() {
}

void Updatable::update(float deltaTime) {
    _proxyInvoke(this, Updatable, update(deltaTime));
}

#ifndef PHYSICALDELEGATE_H
#define PHYSICALDELEGATE_H
#include "../TMSCInclude.h"
#include "PhysicalObject.h"
#include "../sprite/SpriteUser.h"
#include "Transformable.h"

#define PHYSICAL_DELEGATE_GUARD_NULL(x) if(this->delegate() == nullptr) return (x);
extern const float PhysicalDelegate_NullFloat;
extern const N2 PhysicalDelegate_NullV2;

template<typename T>
class PhysicalDelegate : public virtual PhysicalObject, public virtual SpriteUser, public virtual Transformable, public virtual Adapter {
public:
    inline PhysicalDelegate() {};
    inline virtual ~PhysicalDelegate() {};
    
    inline virtual T* delegate() const {
        return nullptr;
    }
     
    //static const Mat3 NullMat3;
    //static const mat4 NullMat4;
    
    /*inline virtual Sprite* sprite() const override {
        PHYSICAL_DELEGATE_GUARD_NULL(nullptr);
        return delegate()->sprite();
    }*/
    inline virtual const Bounds<N2>* bounds() const override {
        PHYSICAL_DELEGATE_GUARD_NULL(nullptr);
        return delegate()->bounds();
    }
    inline virtual PhysicsPoly* poly() const override {
        PHYSICAL_DELEGATE_GUARD_NULL(nullptr);
        return delegate()->poly();
    }
    /*
    inline virtual const N2& positionConst() const override {
        PHYSICAL_DELEGATE_GUARD_NULL(PhysicalDelegate_NullV2);
        return delegate()->positionConst();
    };
    inline virtual const float& rotationConst() const override {
        PHYSICAL_DELEGATE_GUARD_NULL(PhysicalDelegate_NullFloat);
        return delegate()->rotationConst(); 
    };
    inline virtual const N2& spanConst() const override { 
        PHYSICAL_DELEGATE_GUARD_NULL(PhysicalDelegate_NullV2);
        return delegate()->spanConst();
    };*/
    inline virtual N2 internalSpan() const override { 
        PHYSICAL_DELEGATE_GUARD_NULL(N2(0.f));
        return delegate()->internalSpan(); 
    };
    
    /*
    virtual N2 parentToLocal(N2 parPos) const override { 
        PHYSICAL_DELEGATE_GUARD_NULL(N2(0.f));
        return delegate()->parentToLocal(parPos); 
    };
    virtual N2 localToParent(N2 locPos) const override {
        PHYSICAL_DELEGATE_GUARD_NULL(N2(0.f));
        return delegate()->localToParent(locPos); 
    };
    virtual bool isLinearTransform() const override { 
        PHYSICAL_DELEGATE_GUARD_NULL(false);
        return delegate()->isLinearTransform(); 
    };
    virtual Mat3 matrix(bool recursive = false) const override { 
        PHYSICAL_DELEGATE_GUARD_NULL(Mat3());
        return delegate()->matrix(recursive); 
    };
    virtual Mat4 renderMatrix(bool recursive = false) const override {
        PHYSICAL_DELEGATE_GUARD_NULL(Mat4());
        return delegate()->renderMatrix(recursive); 
    };*/
    
private:

};

#endif /* PHYSICALDELEGATE_H */


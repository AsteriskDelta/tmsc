#ifndef PROXY_H
#define PROXY_H
#include "../TMSCInclude.h"
#include "Transformable.h"
#include "Friendable.h"
#include "PhysicalObject.h"
#include "PhysicalEntity.h"
#include "Damagable.h"
#include <unordered_set>

class Proxy : public virtual Transformable, public virtual Friendable, public virtual Adapter {
public:
    Proxy();
    virtual ~Proxy();
    
    inline operator Adapter&() { return *this; };
    
    /*
    template<typename T>
    struct Iterator {
        inline Iterator(std::unordered_set<Adapter*>* h, std::unordered_set<Adapter*>* n = nullptr) : holder(h), nextHolder(n) {
            it = holder->begin();
            if(it == holder->end()) return;
            if(_adapt<T>(*it) == nullptr) ++(*this); 
        };
        inline Iterator(const Iterator<T>& o) : holder(o.holder), it(o.it) {};
        inline Iterator(const std::unordered_set<Adapter*>::iterator& newIt) : holder(nullptr), nextHolder(nullptr), it(newIt) {};
        inline ~Iterator() {};
        std::unordered_set<Adapter*> *holder, *nextHolder;
        std::unordered_set<Adapter*>::iterator it;
        
        inline T& operator*() const { return *_derive<T>(*it); };
        inline T* operator->() const { return _derive<T>(*it); };
        inline Iterator<T>& operator++() {
            while((++it) != holder->end()) {
                //std::cout << "testing " << (*it) << "\n";
                if(_derive<T>(*it) != nullptr) return *this;
            }
            if(nextHolder != nullptr) {
                *this = Iterator<T>(nextHolder);
            }
            return *this;
        }
        
        inline bool operator!=(const Iterator<T>& o) {
            return it != o.it;
        }
    };*/
    template<typename T>
    using Iterator=Friendable::Iterator<T>;
    
    template<typename T>
    inline Iterator<T> begin(bool all = false) {
        if(all) return Iterator<T>(&_children, &_friends);
        else return Iterator<T>(&_children);
    }
    template<typename T>
    inline Iterator<T> end(bool all = false) {
        if(all) return Iterator<T>(_friends.end());
        else return Iterator<T>(_children.end());
    }
    
    template<typename T> inline Iterator<T> beginAll() { return this->begin<T>(true); };
    template<typename T> inline Iterator<T> endAll() { return this->end<T>(true); };
    
    using Friendable::beginFriends;
    using Friendable::endFriends;
    
    /*
    inline void add(Adapter * obj) {
        _children.emplace(obj);
    }
    inline void remove(Adapter * obj) {
        _children.erase(obj);
    }
    inline void addFriend(Adapter * obj) {
        _friends.emplace(obj);
    }
    inline void removeFriend(Adapter * obj) {
        _friends.erase(obj);
    }*/
    template<typename T>
    inline void add(T obj) {
        _children.emplace((Adapter*)obj);
        obj->setParent(this);
    }
    template<typename T>
    inline void remove(T obj) {
        obj->clearParent(this);
        _children.erase((Adapter*)obj);
    }
    template<typename T>
    inline void orphan(T obj) {
        bool wasErased = (_children.erase((Adapter*)obj) > 0);
        if(wasErased) {
            obj->clearParent(this);
            if(!obj->onOrpahn()) delete obj;
        }
    }
    
    inline virtual Proxy *parent() const override { 
        return reinterpret_cast<Proxy*>(_parentPtr);
    };
    inline void setParent(Proxy *const& newPar) {
        Transformable::_setParent<Proxy>(newPar);
    }
    inline void clearParent(Proxy *const& newPar) {
        if(Proxy::parent() != newPar) return;
        //Transformable::_setParent<Proxy>(nullptr);
        this->_parentPtr = nullptr;
    }
    
    inline void pause() {
        suspended = true;
    }
    inline void resume() {
        suspended = false;
    }
    inline bool paused() const { return suspended; };
    
    //Proxy *_parentPtr;
    std::unordered_set<Adapter*> _children;//Owns, 1200s ftw
    bool suspended;
    
    inline unsigned int size(bool all = false) const { return _children.size() + (all? _friends.size() : 0); };
    
    inline virtual Proxy* proxy() override { return this; };
    
    _hasPhysicalProxy();
private:

};

#endif /* PHYSICALPROXY_H */


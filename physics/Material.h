#ifndef MATERIAL_H
#define MATERIAL_H
#include "../TMSCInclude.h"

class Material {
public:
    Material();
    Material(const Material& orig);
    virtual ~Material();
    
    virtual float density() const;
private:

};

#endif /* MATERIAL_H */


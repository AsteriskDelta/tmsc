#ifndef PHYSICALOBJECT_H
#define PHYSICALOBJECT_H
#include "../TMSCInclude.h"
#include "PhysicsPt.h"
#include "../sprite/SpriteUser.h"
#include <unordered_set>

class Sprite;
class Material;

class PhysicalObject : public virtual SpriteUser, public virtual Adapter {
public:
    PhysicalObject();
    PhysicalObject(const PhysicalObject& orig);
    virtual ~PhysicalObject();
    
    //virtual Sprite* sprite() const override;
    //virtual PhysicsPoly* poly() const;
    virtual Material* material() const;
    
    virtual NI area() const;
    virtual NI mass() const;
    virtual NI density() const; 
    
    virtual bool isCollisionEnabled() const;
    
    virtual std::unordered_set<PhysicalObject*> getPotentialColliders(PhysicalObject* obj);
    virtual std::unordered_set<PhysicalObject*> getPotentialColliders(const Bounds<N2> &objAgBounds, PhysicalObject *const& maskObj = nullptr);
    virtual void getPotentialCollidersRecurse(const Bounds<N2> &objAgBounds, std::unordered_set<PhysicalObject*> *const & ret, PhysicalObject *const& maskObj = nullptr);
    
    inline virtual bool isEntity() const { return false; };
    
    _hasPhysicalObject();
private:
    mutable struct {
        NI area, mass, density;
    } cached;
};

#endif /* PHYSICALOBJECT_H */



/* 
 * File:   Force.h
 * Author: galen
 *
 * Created on April 12, 2017, 9:31 PM
 */

#ifndef FORCE_H
#define FORCE_H

class Force {
public:
    Force();
    Force(const Force& orig);
    virtual ~Force();
private:

};

#endif /* FORCE_H */


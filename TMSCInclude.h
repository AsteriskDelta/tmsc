#ifndef TMSCINCLUDE_H
#define TMSCINCLUDE_H

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <ARKE/EngineClient.h>
#include <ARKE/UI.h>
#include <mutex>
#include <NVX/NVX.h>
#include <NVX/N2.h>
using namespace nvx;

#define N2_UP (N2(0, 1))
#define N2_RIGHT (N2(1, 0))
#define N2_DOWN (N2(0, -1))
#define N2_LEFT (N2(-1, 0))

#define DEF_TILESIZE (16)
#define PX_PER_UNIT (DEF_TILESIZE)
#define UNIT_PER_PX (1.f/float(PX_PER_UNIT))

typedef std::recursive_mutex Mutex;
typedef std::lock_guard<Mutex> MutexGuard;

inline double wrapAngle(double x){
  x = fmod(x + 180,360);
  if (x < 0)
    x += 360;
  return x - 180;
}

typedef unsigned long long idint;

typedef N2T<int16_t, 1> TCoord;
//typedef N2Raw TCoord;
#define VP(x) ((x).str())
#define VBD(x) ("Bounds("+VP((x).nearPoint()) + " -> " + VP((x).farPoint())+")")

class PhysicalObject;
class PhysicalEntity;
class Proxy;
class Damagable;
class Renderable;

#define _adapterCastFunc(CN, FN, PTR) inline virtual CN* FN() { return (CN*)PTR; };\
inline virtual const CN* FN##Const() const { return (const CN*)PTR; };

#define _adapterFuncImpl(CN, PTR)  template<> inline CN* Adapter::adapt<CN>() { return (CN*)PTR; };\
template<> inline const CN* Adapter::adaptConst<CN>() const { return (const CN*)PTR; };

#define _hasPhysicalObject() _adapterCastFunc(PhysicalObject, physicalObject, this);
#define _hasPhysicalEntity() _adapterCastFunc(PhysicalEntity, physicalEntity, this);
#define _hasPhysicalProxy() _adapterCastFunc(Proxy, physicalProxy, this);
#define _hasDamagable() _adapterCastFunc(Damagable, damagable, this);
#define _hasRenderable() _adapterCastFunc(Renderable, renderable, this);

#define _adaptable() inline operator Adaptable&() { return *this; };\
inline operator const Adaptable&() const { return *this; };

class Adapter {
public:

    inline Adapter() {
    };

    inline Adapter(const Adapter& o) {
        _unused(o);
    };

    inline virtual ~Adapter() {
    };
    
    //inline virtual Adapter* self() { return this; };
    //inline virtual const Adapter* selfConst() { return this; };
    
    template<typename T>
    inline T* adapt() {
        return nullptr;
    }
    template<typename T>
    inline const T* adaptConst() const {
        return nullptr;
    }
    
    inline virtual Adapter* parent() const { return nullptr; };
    template<typename T>
    inline T* parent_t() const {
        return dynamic_cast<T*>(this->parent());
    }
    
    /* Returns true if object should be retained after parent deletion, otherwise false promises deletion
     * If you return true, MAKE SURE you do *something* in an overload of this function, lest this object linger eternally in RAM.
     */
    inline virtual bool onOrphaned() { return false; };
    
    _adapterCastFunc(Adapter, adapter, this);

    _adapterCastFunc(PhysicalObject, physicalObject, nullptr);
    _adapterCastFunc(PhysicalEntity, physicalEntity, nullptr);
    _adapterCastFunc(Proxy, physicalProxy, nullptr);
    _adapterCastFunc(Damagable, damagable, nullptr);
    _adapterCastFunc(Renderable, renderable, nullptr);
    
};

template<typename T>
inline T* _adapt(Adapter *const& ptr) {
    return dynamic_cast<T*>(ptr);
}

template<typename T, typename K>
inline T* _derive(K *const& ptr) {
    return dynamic_cast<T*>(ptr);
}

_adapterFuncImpl(Adapter, this);
_adapterFuncImpl(PhysicalObject, nullptr);
_adapterFuncImpl(PhysicalEntity, nullptr);
_adapterFuncImpl(Proxy, nullptr);
_adapterFuncImpl(Damagable, nullptr);
_adapterFuncImpl(Renderable, nullptr);

#define TICKS_PER_SECOND (1000)

//#include "physics/PhysicalProxy.h"
#define _proxyInvoke(PTR, T, FUNC) {\
    Proxy *const _prx = _adapt<Proxy>(PTR);\
    if(_prx != nullptr && ! _prx->paused()) {\
        for (auto it = _prx->begin<T>(); it != _prx->end<T>(); ++it) {\
            T *const obj = &(*it);\
            obj->FUNC;\
        }\
    }\
}


inline TCoord tcRollover(const TCoord& a, const TCoord& b) {
    TCoord ret = a;
    while(ret.x < 0) ret.x += b.x;
    while(ret.y < 0) ret.y += b.y;
    while(ret.x >= b.x) ret.x -= b.x;
    while(ret.y >= b.y) ret.y -= b.y;
    return ret;
}

inline TCoord tcDivide(const TCoord& a, const TCoord& b) {
    TCoord ret;
    if(b.x == 0) ret.x = 0;//sgn(a.x);
    else ret.x = a.x/b.x;
    if(b.y == 0) ret.y = 0;//sgn(a.y);
    else ret.y = a.y/b.y;
    return ret;
}

inline int tcLength(const TCoord& a) {
    return int(std::max(abs(a.x), abs(a.y)));
}
inline int tcLength2(const TCoord& a) {
    return int(abs(a.x)+abs(a.y));
}

inline int tcHamming(const TCoord& a) {
    return int(tcLength(a - TCoord(1,1))/2);
}

inline TCoord tcNormalize(const TCoord& a) {
    //int maxVal = std::max(1, tcLength(a));
    //return a/maxVal;
    return TCoord(sgn(a.x), sgn(a.y));
}

inline int tcDot(const TCoord& a, const TCoord& b) {
    return int(a.x*b.x + a.y*b.y);
}

inline int tcContrib(const TCoord& a, const TCoord& b) {
    TCoord tmp = tcDivide(a,b);
    return int(tmp.x+tmp.y);
}

inline int select(const int &v, const int &a, const int &b) {
    if(v >= 0) return a;
    else return b;
}

#endif /* TMSCINCLUDE_H */


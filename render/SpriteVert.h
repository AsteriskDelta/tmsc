#ifndef SPRITEVERT_H
#define SPRITEVERT_H
#include "../TMSCInclude.h"
#include <ARKE/V3.h>
#include <NVX/N2.h>
#include <ARKE/Color.h>

struct SpriteVert {
    union {
        V3 position;
        V3 pos;
    };
    N2T<Uint16, 1> texCoords;
    N2T<Uint16, 1> offsetCoords;
    ColorRGBA tint;
    //Uint8 atlas;
    union {
        ColorRGBA meta;
        struct {
            Uint8 atlas;
            Uint8 b,c,d;
        };
    };
    
    
    inline SpriteVert() {};
    inline SpriteVert(const SpriteVert& o) : position(o.position), texCoords(o.texCoords), offsetCoords(o.offsetCoords), tint(o.tint), meta(o.meta) {};

    //7 bytes remaining

    void mapAttributes(MeshContainer * const mesh) {
        Uint8 offset = 0;
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof (float));
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof (Uint16), false);
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof (Uint8), true);
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof (Uint8), true);
    }
};

#endif /* SPRITEVERT_H */


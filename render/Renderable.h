#ifndef RENDERABLE_H
#define RENDERABLE_H
#include "../TMSCInclude.h"
#include <unordered_set>
#include <vector>
#include <ARKE/Matrix.h>

class Proxy;


class Renderable : public virtual Adapter {
public:
    Renderable();
    Renderable(const Renderable& orig);
    virtual ~Renderable();
    
    virtual bool prerender(int pass);
    virtual bool uploadRender(int pass);
    virtual bool render(int pass);
    
    inline virtual bool isIndexed() const { return registeredForPass < 0; };
    inline virtual bool shouldRender(int pass) const { 
        return pass == Pass::Any || !isIndexed() || registeredForPass == pass;
    };
    
    inline virtual int renderPass() const { return registeredForPass; };
    void registerForPass(int pass);
    void registerRenderPass();
    void unregisterRenderPass();
    
    void needsPrerender();
    void needsUpload();
    
    _hasRenderable();
    
    static void PrerenderAll();
    static void UploadAll();
    static void RenderPass(int pass);
    static const std::unordered_set<Renderable*>* IndexedForPass(int pass);
    
    struct Pass {
    enum PassID {
        Any = -3,
        Visability = 0,
        Entity,
        Map,
        Lighting,
        Particles,
        Debug,
        //Resolve takes place here
        PostFX,
        UI
    };
    };
    
    static void PushMatrix(const Mat4& mat);
    static void PopMatrix();
private:
    Proxy *rrProxy();
    
    short registeredForPass;
    struct {
        bool taggedForPrerender : 1;
        bool taggedForUpload : 1;
        char padd : 6;
    };
    
    static std::vector<Renderable*> prerenderQueue, uploadQueue;
};

#endif /* RENDERABLE_H */


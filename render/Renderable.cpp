#include "Renderable.h"
#include "physics/Proxy.h"
#include <ARKE/Renderer.h>

Renderable::Renderable() : registeredForPass(-1), taggedForPrerender(false), taggedForUpload(false) {
}

Renderable::Renderable(const Renderable& orig) : Adapter(orig) {
}

Renderable::~Renderable() {
    this->unregisterRenderPass();
}

bool Renderable::prerender(int pass) {
    taggedForPrerender = false;
    
    if(this->isIndexed()) return this->shouldRender(pass);
    _proxyInvoke(this, Renderable, prerender(pass));
    return true;
}
bool Renderable::uploadRender(int pass) {
    taggedForUpload = false;
    
    if(this->isIndexed()) return this->shouldRender(pass);
   _proxyInvoke(this, Renderable, uploadRender(pass));
   return true;
}
bool Renderable::render(int pass) {
    if(this->isIndexed()) return this->shouldRender(pass);
   _proxyInvoke(this, Renderable, render(pass));
   return true;
}

Proxy* Renderable::rrProxy() {
    return _adapt<Proxy>(this);
}

std::vector<Renderable*> Renderable::prerenderQueue, Renderable::uploadQueue;

void Renderable::needsPrerender() {
    if(this->taggedForPrerender) return;
    
    taggedForPrerender = true;
    prerenderQueue.push_back(this);
}
void Renderable::needsUpload() {
    if(this->taggedForUpload) return;
    
    taggedForUpload = true;
    uploadQueue.push_back(this);
}

static std::vector<std::unordered_set<Renderable*>> Renderable_PassIndex;
void Renderable::registerForPass(int pass) {
    if(pass < 0) return;
    else if(registeredForPass >= 0) this->unregisterRenderPass();//Only allow one pass registration
    
    if(pass >= int(Renderable_PassIndex.size())) Renderable_PassIndex.resize(pass+1);
    Renderable_PassIndex[pass].insert(this);
    registeredForPass = pass;
}
void Renderable::registerRenderPass() {
    return this->registerForPass(renderPass());
}
void Renderable::unregisterRenderPass() {
    //if(this->renderPass() == 1) return;
    if(registeredForPass < 0 || registeredForPass >= int(Renderable_PassIndex.size())) return;
    Renderable_PassIndex[registeredForPass].erase(this);
    registeredForPass = -1;
}

const std::unordered_set<Renderable*>* Renderable::IndexedForPass(int pass) {
    if(pass < 0 || pass >= int(Renderable_PassIndex.size())) return nullptr;
    else return &(Renderable_PassIndex[pass]);
}

void Renderable::RenderPass(int pass) {
    const std::unordered_set<Renderable*> *const set = IndexedForPass(pass);
    if(set == nullptr) return;
    for(Renderable *const& obj : *set) {
        obj->render(pass);
    }
}

void Renderable::PrerenderAll() {
    for(auto it = prerenderQueue.begin(); it != prerenderQueue.end(); ++it) {
        Renderable *const obj = *it;
        obj->prerender(Pass::Any);
        //std::cout << "\tPRERENDER " << obj << "\n";
    }
    prerenderQueue.clear();
}
void Renderable::UploadAll() {
    for(auto it = uploadQueue.begin(); it != uploadQueue.end(); ++it) {
        Renderable *const obj = *it;
        obj->uploadRender(Pass::Any);
        //std::cout << "\tUPLOADRENDER " << obj << "\n";
    }
    uploadQueue.clear();
}

void Renderable::PushMatrix(const Mat4& mat) {
    Renderer::PushMatrix(mat);
}

void Renderable::PopMatrix() {
    Renderer::PopMatrix();
}
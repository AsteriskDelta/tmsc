#ifndef SPRITEGROUP_H
#define SPRITEGROUP_H
#include "../TMSCInclude.h"
#include "SpriteVert.h"
#include "../sprite/SpriteInstance.h"
#include "../sprite/SpriteFrame.h"
#include <ARKE/V3.h>
#include <ARKE/Quat.h>
#include "../render/Renderable.h"
#include "../physics/Transformable.h"

template<typename T, int C> class FaceGroup;
class Shader;
class PhysicalSpriteInstance;

class SpriteGroup : public Renderable, public virtual Transformable {
    friend class SpriteInstance;
public:
    SpriteGroup(unsigned int startSize = 256, bool dyn = false);
    virtual ~SpriteGroup();
    
    //Transform transform;
    
    virtual SpriteInstance* add(const SpriteFrame& frame);
    //virtual PhysicalSpriteInstance* addPhysical(const SpriteFrame& frame);
    virtual void remove(SpriteInstance* ins, bool del);
    
    //Attach an instance created elsewhere (ie, subclass)
    virtual void attach(SpriteInstance *ins);
    
    virtual unsigned int size() const;
    
    virtual void upload(bool force = false);
    virtual void draw();
    
    virtual bool prerender(int pass);
    virtual bool uploadRender(int pass);
    virtual bool render(int pass);
    
    inline virtual bool needsAtlas() const { return true; };
    inline virtual Shader* getShader() const { return shader; };
    
    inline bool isDynamic() { return dynamic; };
    bool isDirty() const;
protected:
    FaceGroup<SpriteVert, 4> *group;
    bool dynamic;
    
    SpriteVert* getVertexPtr(unsigned int sID);
    
    static Shader* shader;
    static void LoadShader();
};

#endif /* SPRITEGROUP_H */


#include "DebugGroup.h"
#include <ARKE/QuadGroup.h>
#include <ARKE/Shader.h>
#include "sprite/Sprite.h"

Shader* DebugGroup::shader = nullptr;

void DebugGroup::LoadShader() {
    shader = Shader::Load("prgm/debugGroup.prgm");
}

DebugGroup::DebugGroup(unsigned int startSize, bool dyn) : SpriteGroup(startSize, dyn), defaultWidth(0.02f), defaultRadius(0.08f), currentLevel(0), currentOwner(nullptr), ownerMap() {
    if(shader == nullptr) LoadShader();
    this->registerForPass(Renderable::Pass::Debug);
    //group->setShader(shader);
}

DebugGroup::~DebugGroup() {
    this->clear();
    if(group != nullptr) delete group;
}

extern int ARKE_DBGMODE;
static unsigned int ARKE_DBG_UNS;

void DebugGroup::draw() {
    //glDisable(GL_DEPTH_TEST);
    //glDepthMask(false);
    
    if(ARKE_DBG_UNS != (unsigned int)ARKE_DBGMODE) ARKE_DBG_UNS = (unsigned int)ARKE_DBGMODE;
    
    riEnsureState();
    this->getShader()->activate();
    riEnsureState();
    this->getShader()->bindUint(ARKE_DBG_UNS, 7);
    riEnsureState();
    group->draw();
    riEnsureState();
    this->getShader()->deactivate();
    riEnsureState();
    
    
    //glDepthMask(true);
    //glEnable(GL_DEPTH_TEST);
}

void DebugGroup::setLevel(Uint8 lvl) {
    currentLevel = lvl;
}
void DebugGroup::setOwner(void *ptr) {
    currentOwner = ptr;
}

void DebugGroup::drawFace(V3 mid, V3 xRel, V3 yRel, ColorRGBA col) {
    unsigned int subID = group->addID();
    SpriteVert *verts = this->getVertexPtr(subID);
    if(verts == nullptr) {
        Console::Err("DebugGroup::drawFace given null vert ptr by group ", group, " for id ", subID);
        return;
    }
    
    static const V3 corners[4] = {V3(-1.f,-1.f,0.f), V3(1.f,-1.f, 0.f), V3(1.f,1.f, 0.f), V3(-1.f,1.f, 0.f)};
    for(int i = 0; i < 4; i++) {
        const V3& crd = corners[i];
        verts[i].position = mid + xRel * crd.x + yRel*crd.y;
        
        verts[i].texCoords = N2T<Uint16, 1>(Uint16((crd.x+1.f)*512.f), Uint16((crd.y+1.f)*512.f));
        verts[i].offsetCoords = N2T<Uint16, 1>(0);
        
        verts[i].tint = col;
        verts[i].meta = ColorRGBA(currentLevel);
        verts[i].position = /*transform * */verts[i].position;
    }
    
   ownerMap[currentOwner].push_back(subID);
}

void DebugGroup::drawLine(V3 start, V3 end, ColorRGBA col, float width) {
    if(this->group == nullptr) return;
    if(width <= 0.f) width = defaultWidth;
    
    V3 midpoint = (start + end) / 2.f;
    V3 parVec = (start - midpoint);
    V3 perpVec = normalize(cross(parVec, V3(0.f, 0.f, 1.f))) * width;
    
    this->drawFace(midpoint, parVec, perpVec, col);
}

void DebugGroup::drawPoint(V3 pt, ColorRGBA col, float radius) {
    if(this->group == nullptr) return;
    if(radius <= 0.f) radius = defaultRadius;
    
    V3 midpoint = pt;
    V3 parVec = V3_UP*radius;
    V3 perpVec = normalize(cross(parVec, V3(0.f, 0.f, 1.f))) * radius;
    
    this->drawFace(midpoint, parVec, perpVec, col);
}

void DebugGroup::clear() {
    for(auto it = ownerMap.begin(); it != ownerMap.end(); ++it) {
        std::vector<unsigned int> *const facesForOwner = &(it->second);
        for(const unsigned int& faceID : *facesForOwner) {
            group->remove(faceID);
        }
    }
    ownerMap.clear();
    group->clear();
    Renderable::needsUpload();
}

void DebugGroup::clear(void* owner) {
    auto it = ownerMap.find(owner);
    if(it == ownerMap.end()) return;
    
    std::vector<unsigned int> *const facesForOwner = &(it->second);
    for(const unsigned int& faceID : *facesForOwner) {
        group->remove(faceID);
    }
    ownerMap.erase(owner);
}

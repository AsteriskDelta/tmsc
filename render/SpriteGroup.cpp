#include "SpriteGroup.h"
#include <ARKE/QuadGroup.h>
#include <ARKE/Shader.h>
#include "sprite/Sprite.h"

Shader* SpriteGroup::shader = nullptr;

void SpriteGroup::LoadShader() {
    shader = Shader::Load("sprite.prgm");
}

SpriteGroup::SpriteGroup(unsigned int startSize, bool dyn) : group(nullptr) {
    if(shader == nullptr) LoadShader();
    group = new FaceGroup<SpriteVert>(startSize, true, dyn);
    //group->setShader(shader);
}

SpriteGroup::~SpriteGroup() {
    if(group != nullptr) delete group;
}

SpriteInstance* SpriteGroup::add(const SpriteFrame& frame) {
    //std::cout << "\t\tSpriteGroup::add\n";
    SpriteInstance *ret = new SpriteInstance(this, group->addID(), frame);
    ret->setParent(this);
    ret->update();
    return ret;
}
void SpriteGroup::attach(SpriteInstance *ins) {
    if(ins->group != nullptr) {
        //Console::Error("SpriteGroup asked to attach SpriteInstance owned by someone else! Remove it first!");
        //return;
        ins->group->remove(ins, false);
    }
    ins->group = this;
    ins->subID = group->addID();
    //ins->update();
    
}
void SpriteGroup::remove(SpriteInstance* ins, bool del) {
    if(ins->group == this) group->remove(ins->subID);
    ins->group = nullptr;
    if(del) delete ins;
}
unsigned int SpriteGroup::size() const {
    return group->size();
}
void SpriteGroup::upload(bool force) {
    //std::cout << "\tSpriteGroup::upload() with size=" << this->size() << "\n";
    group->upload(force);
}
void SpriteGroup::draw() {
    riEnsureState();
    this->getShader()->activate();
    riEnsureState();
    if(this->needsAtlas()) Sprite::BindAtlas(this->getShader());
    riEnsureState();
    group->draw();
    riEnsureState();
    this->getShader()->deactivate();
    riEnsureState();
}

bool SpriteGroup::prerender(int pass) {
    if(!Renderable::prerender(pass)) return false;
    
    return true;
}
bool SpriteGroup::uploadRender(int pass) {
    if(!Renderable::uploadRender(pass)) return false;
    
    this->upload();
    return true;
}
bool SpriteGroup::render(int pass) {
    if(!Renderable::shouldRender(pass)) return false;
    
    Renderable::PushMatrix(this->renderMatrix(true));
    this->draw();
    Renderable::PopMatrix();
    
    
    return Renderable::render(pass);
}

SpriteVert* SpriteGroup::getVertexPtr(unsigned int sID) {
    FaceGroup<SpriteVert>::Face *face = group->get(sID);
    if(face == nullptr) return nullptr;
    
    group->setDirty();
    Renderable::needsUpload();
    return reinterpret_cast<SpriteVert*>(face);
}

bool SpriteGroup::isDirty() const {
    return group->dirty();
}
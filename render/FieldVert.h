#ifndef FIELDVERT_H
#define FIELDVERT_H
#include "../TMSCInclude.h"
#include <ARKE/V3.h>
#include <NVX/N2.h>
#include <ARKE/Color.h>

struct FieldVert {
    union {
        V3 position;
        V3 pos;
    };
    N2T<Uint16, 1> texCoords;
    ColorRGBA tint;
    
    
    inline FieldVert() {};
    inline FieldVert(const FieldVert& o) : position(o.position), texCoords(o.texCoords), tint(o.tint) {};

    //12 bytes remaining

    void mapAttributes(MeshContainer * const mesh) {
        Uint8 offset = 0;
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof (float));
        mesh->addAttribute(offset, 2, MeshAttribute::Unsigned, sizeof (Uint16), true);
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof (Uint8), true);
    }
};

#endif /* FIELDVERT_H */


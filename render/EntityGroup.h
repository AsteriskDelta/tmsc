#ifndef ENTITYGROUP_H
#define ENTITYGROUP_H
#include "../TMSCInclude.h"
#include "SpriteVert.h"
#include "../sprite/SpriteInstance.h"
#include "../sprite/SpriteFrame.h"
#include <ARKE/V3.h>
#include <ARKE/Quat.h>
#include "SpriteGroup.h"

class Entity;

class EntityGroup : public SpriteGroup {
public:
    EntityGroup(unsigned int startSize = 256, bool dyn = true);
    virtual ~EntityGroup();
    
    void add(Entity* entity);
    void remove(Entity *entity);
    using SpriteGroup::add;
    using SpriteGroup::remove;
    
    virtual void draw() override;
private:

};

#endif /* ENTITYGROUP_H */


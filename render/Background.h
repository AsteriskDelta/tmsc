
/* 
 * File:   Background.h
 * Author: galen
 *
 * Created on April 2, 2017, 5:11 PM
 */

#ifndef BACKGROUND_H
#define BACKGROUND_H

class Background {
public:
    Background();
    Background(const Background& orig);
    virtual ~Background();
private:

};

#endif /* BACKGROUND_H */


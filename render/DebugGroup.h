#ifndef DEBUGGROUP_H
#define DEBUGGROUP_H
#include "../TMSCInclude.h"
#include "SpriteVert.h"
#include "../sprite/SpriteInstance.h"
#include "../sprite/SpriteFrame.h"
#include <ARKE/V3.h>
#include <ARKE/Quat.h>
#include "SpriteGroup.h"

class DebugGroup : public SpriteGroup {
public:
    friend class SpriteInstance;
    
    DebugGroup(unsigned int startSize = 256, bool dyn = false);
    virtual ~DebugGroup();
    
    virtual void draw() override;
    
    void setLevel(Uint8 lvl);
    void setOwner(void *ptr);
    
    void drawLine(V3 start, V3 end, ColorRGBA col = ColorRGBA(255), float width = -1.f);
    inline void drawLine(N2 start, N2 end, ColorRGBA col = ColorRGBA(255), float width = -1.f) {
        return this->drawLine(V3(V2(start), 0.f), V3(V2(end), 0.f), col, width);
    }
    
    void drawPoint(V3 pt, ColorRGBA col = ColorRGBA(255), float radius = -1.f);
    inline void drawPoint(N2 pt, ColorRGBA col = ColorRGBA(255), float radius = -1.f) {
        return this->drawPoint(V3(V2(pt), 0.f), col, radius);
    }
    
    void drawFace(V3 mid, V3 xRel, V3 yRel, ColorRGBA col);
    
    void clear();
    void clear(void* owner);
    
    inline virtual bool needsAtlas() const override { return false; };
    inline virtual Shader* getShader() const override { return shader; };
    
    float defaultWidth, defaultRadius;
protected:
    Uint8 currentLevel;
    void *currentOwner;
    std::unordered_map<void*, std::vector<unsigned int>> ownerMap;
    
    static Shader* shader;
    static void LoadShader();
private:

};

#endif /* DEBUGGROUP_H */


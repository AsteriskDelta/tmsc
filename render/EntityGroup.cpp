#include "EntityGroup.h"
#include "entity/Entity.h"

EntityGroup::EntityGroup(unsigned int startSize, bool dyn) : SpriteGroup(startSize, dyn) {
    this->registerForPass(Renderable::Pass::Entity);
}

EntityGroup::~EntityGroup() {
}

void EntityGroup::add(Entity* entity) {
    entity->setGroup(this);
    entity->updateSprites();
}
void EntityGroup::remove(Entity *entity) {
    entity->removeSprites();
    entity->setGroup(nullptr);
}

void EntityGroup::draw() {
    //std::cout << "Drawing entitygroup of size=" << this->size() << "\n";
    SpriteGroup::draw();
}
#include <cstdlib>
#include "TMSCInclude.h"
#include <NVX/NPolygon.h>
#include <NVX/Bounds.h>
#include "sprite/Sprite.h"

extern int ARKE_DBGMODE;
using namespace std;
typedef Sprite::Poly Poly;
typedef Bounds<N2> Bound2;

void init();

void printPoly(Poly* poly) {
    std::cout << "\tPrintPoly size=" << poly->size() << "\n";
    for(unsigned int i = 0; i < poly->size(); i++) {
        std::cout << "\t\t"<<i<<"\t"<< poly->get(i).position() << "\n";
    }
}

void printBound(Bound2 &bound) {
    std::cout << "got bounds from " << VP(bound.center) << " to " << VP(bound.axes[0]+bound.center) << "\n";
}


int main(int argc, char** argv) {
    _unused(argc); _unused(argv); 
    init();
    Poly* poly = new Poly(), *otherPoly = new Poly();
    
    for(unsigned int i = 0; i < 10; i++) poly->add(N2(i, 0.f));
    for(unsigned int i = 20; i < 26; i++) otherPoly->add(N2(i, 0.f));
    printPoly(poly);
    
    int insIdx = poly->insert(-1, N2(0.f, 2.f), 1);
    std::cout << "inserted at " << insIdx << "\n";
    printPoly(poly);
    
    insIdx = poly->insert(-1, N2(0.f, -2.f), -1);
    std::cout << "inserted at " << insIdx << "\n";
    printPoly(poly);
    
    int rangeEnd = poly->removeRange(2, 4);
    std::cout << "got end=" << rangeEnd << "\n";
    printPoly(poly);
    
    int revRangeEnd = poly->removeRange(6, 4, -1);
    std::cout << "got revEnd = " <<revRangeEnd<<"\n";
    printPoly(poly);
    
    std::cout << "other polygon: \n";
    printPoly(otherPoly);
    
    int afterIns = poly->insert(0, 1, otherPoly, 0, otherPoly->size()-1, 1);
    std::cout << "after insertion (ret idx " << afterIns << "):\n";
    printPoly(poly);
    /*
    Sprite *testEntSprite = new Sprite("entity/test.png", true);
    testEntSprite->generatePhysics();
    //testEntSprite->repair();
    printBound(testEntSprite->bounds);
    Bound2 testB;// = testEntSprite->bounds;
    for(Poly *ply = testEntSprite->poly; ply != nullptr; ply = ply->next) {
        std::cout << "for sub-poly:\n\t";
        printBound(ply->bounds);
        testB.add(ply->bounds);
    }
    std::cout << " got test bounds:\n";
    printBound(testB);
    */
    return 0;
}


namespace GameManager {  
    void Init() {
         Application::BeginInitialization();
        //System::SetrLimit(1024*1024*1024);
        Application::SetDataDir("../../data/");
        Application::SetName("TMSC");
        Application::SetCodename("tmsc1");
        Application::SetCompanyName("Delta III Tech");
        Application::SetVersion("0.0r1X");
        Application::SetDebug(true);
        Application::OnExit(&GameManager::Cleanup);
        //System::SetStackLimit(1024*1024*256);
        Application::EndInitialization();
    }
    void PrimaryLoad() {
        
    }
    void SecondaryLoad() {
        
    }
    
    void ClientSetup() {//Called after SecondaryLoad is completed
    }

    void WindowChanged() {//Called at window change
    }

    void Update(float newDeltaTime) {
        _unused(newDeltaTime);
    }

    Transform* GetCameraTransform() {
        return nullptr;
    }

    REND_DRAW_R renderScene REND_DRAW_D{
        
        return 1;
    }

    void OnOpaque3D(Camera * const camera, RenderStage * const stage) {
        _unused(camera); _unused(stage);
    }

    void OnTransparent3D(Camera * const camera, RenderStage * const stage) {
        _unused(camera); _unused(stage);
    }

    void OnUI3D(Camera * const camera, RenderStage * const stage) {
        _unused(camera); _unused(stage);
    }

    void OnOrtho() {

    }

    void Render() {
        
    }

    unsigned short GetDistortionCount() {
        return 0;
    }

    void OnDistortion(unsigned short id) {
        _unused(id);
    }

    //Server-side stuff

    void OnTick() {

    }

    void Cleanup() {
        //ResourceManager::Cleanup();
        //ProgramStat::StopRecorderThread();
    }

    bool isServer, isClient;
  }


void init() {
    GameManager::Init();
}

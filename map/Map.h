#ifndef MAP_H
#define MAP_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include <vector>
#include "MapSector.h"

class Map : public virtual PhysicalProxy, public virtual PhysicalEntity, 
            public virtual MapSector {
public:
    Map();
    virtual ~Map();
    
    virtual bool hasRenderTransform() override;
    virtual Transform getRenderTransform() override;
    
    virtual void update(float deltaTime) override;
    
    /*virtual void prerender(int pass) override;
    virtual void uploadRender(int pass) override;
    virtual void render(int pass) override;*/
private:

};

#endif /* MAP_H */


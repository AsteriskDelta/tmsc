#ifndef TILEINSTANCE_H
#define TILEINSTANCE_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include "TileFrameInstance.h"
#include "sprite/PhysicalSpriteInstance.h"
#include <unordered_set>

class Tile;
class TileFrame;
class MapSector;
class SpriteGroup;
class DebugGroup;

class TileInstance : public virtual PhysicalSpriteInstance, public virtual Friendable, public virtual Damagable {
public:
    TileInstance();
    virtual ~TileInstance();
    
    TileInstance *getSibling(const TCoord& off);
    
    Tile *tile;
    struct Data {
        unsigned int tileID = 0;
        short frameID = -1;
        char orientation = -1;
        bool smooth = true;
        bool collisionCache = true;
        TCoord normal() const;
    } data;
    
    bool considered;
    inline bool isConsidered(bool self = false) const {
        if(self && consideredOwner == this) return false;
        else return considered;
    }
    void setConsidered(bool state, TileInstance *owner);
    
    //PhysicalSpriteInstance *sprite;
    
    //void makeSprite(SpriteGroup *grp);
    //void freeSprite();
    void updateSprite();
    
    inline TCoord coords() const { return secCoords; };
    inline const TCoord& coordsConst() const;
    
    virtual void update() override;
    void characterize(int stage = -1);
    
    bool needsUpdate() const;
    
    bool hasFrame() const;
    
    bool coversTile(TCoord off, TileInstance *const o = nullptr, bool fully = false);
    void updateCollisionsEnabled();
    virtual bool isCollisionEnabled() const override;
//private:
    TCoord secCoords;
    MapSector* sector() const; 
private:
    TileFrame* tileFrame() const;
private:
    TileInstance* consideredOwner;
};

#endif /* TILEINSTANCE_H */


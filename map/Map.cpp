#include "Map.h"

Map::Map() : MapSector() {
    
}


Map::~Map() {
}

bool Map::hasRenderTransform() {
    return true;
}
Transform Map::getRenderTransform() {
    return MapSector::getRenderTransform();//Transform();
}
/*
void Map::prerender(int pass) {
    Proxy::pause(); {
        MapSector::prerender(pass);
        //EntityTree::prerender();
    } Proxy::resume();

    Renderable::prerender(pass);
}
void Map::uploadRender(int pass) {
    Proxy::pause(); {
        MapSector::uploadRender(pass);
        //EntityTree::uploadRender();
    } Proxy::resume();
    
    Renderable::uploadRender(pass);
}

extern int ARKE_DBGMODE;
void Map::render(int pass) {
    if(this->hasRenderTransform()) Renderer::PushTransform(this->getRenderTransform());
    
    Proxy::pause(); {
        MapSector::render(pass);
        //EntityTree::render();
    } Proxy::resume();
    
    Renderable::render(pass);
    
    //MapSector::render(2);//Render only debug, do not recurse
    
    if(this->hasRenderTransform()) Renderer::PopTransform();
    MapSector::render(pass);
}*/

void Map::update(float deltaTime) {
    MapSector::update(deltaTime);
}
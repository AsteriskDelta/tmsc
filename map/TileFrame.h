#ifndef TILEFRAME_H
#define TILEFRAME_H
#include "../TMSCInclude.h"
#include <ARKE/Loadable.h>
#include <ARKE/Image.h>
#include <NVX/NPolygon.h>
#include "physics/PhysicsPt.h"
#include "sprite/Sprite.h"
#include "TileFrameInstance.h"
#include <NVX/N2.h>
#include <vector>

class TileInstance;
class SpriteFrame;
class DebugGroup;

struct TileFrameMeta {
    
};

class TileFrame : public Sprite {
public:
    TileFrame();
    TileFrame(const std::string& p);
    virtual ~TileFrame();
    
    unsigned short parentID;
    
    TCoord getInputNormal(SpriteFrame *const orient = nullptr) const;
    TCoord getOpens(SpriteFrame *const orient = nullptr) const;
    
    TCoord inputNormal, size;
    unsigned short versionID, transID, span;
    short dmgLevel, brokeLevel, rawGrad, detailID;
    
    TCoord opens;
    
    bool cap;
    bool corner, round, airtight, opaque, grabable;
    bool inverse, isRamp, fake, filling, collides;
    bool allowRotate, allowMirror;
    TCoord ramp, transVec;
    std::vector<TileFrame*> children;
    TileFrame* nullChild;
    TileFrame*& getChild(TCoord off, SpriteFrame *const orient = nullptr);
    
    V3 renderOffset;
    
    int match(TileInstance *ins, TileFrameInstance& targetFrameInfo, int *errs = nullptr, SpriteFrame *const orient = nullptr);
    int multiMatch(TileInstance *ins, TileFrameInstance& targetFrameInfo, int *errs = nullptr, SpriteFrame *const orient = nullptr);
    int allOrientationMultiMatch(TileInstance *ins, TileFrameInstance& targetFrameInfo, int *errs = nullptr);
    bool isMulti() const;
    
    void setConsidered(TileInstance *ins, bool s = true, SpriteFrame *orient = nullptr);
    
    std::string tileName() const;
    
    N2 drawSize(bool flip = false) const;
    inline TCoord drawSizePX(bool flip = false) const {
        //if(flip) return TCoord(size.y, size.x);
        //else return TCoord(size.x, size.y);
        //return TCoord(ceil(this->drawSize(flip))*float(PX_PER_UNIT));
        return TCoord(this->drawSize(flip) * float(PX_PER_UNIT));
    }
    bool isConstrained() const;
    float getZOffset() const;
    
    virtual bool load(const std::string& p) override;
    static TileFrame* Factory(const std::string p = "");
    
    virtual void prepare() override;
    void setTile(Tile *const tPtr);
    Tile *tile;
    
    inline bool isTransition() const {
        return transID != 0;
    }
    inline bool isFullyOpaque() const {
        return filling && opaque;
    }
    inline bool isOpaque() const {
        return opaque;
    }
    inline bool isFilling() const {
        return filling;
    }
    inline bool isPhysical() const {
        return collides;
    }
    
    virtual void generatePhysics() override;
    virtual void drawDebug(DebugGroup *dbg, SpriteInstance *ins) override;
private:
    void initTileVars();
};

#endif /* TILEFRAME_H */


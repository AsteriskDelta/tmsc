#include "Tile.h"
#include <NVX/NPolygon.h>
#include <string>
#include "TileFrame.h"
#include "TileInstance.h"
#include <ARKE/XML.h>
#include <ARKE/AppData.h>
#include "TileManager.h"

///#define BLOCKDBG if(true)
#define BLOCKDBG if(false)

#define DEFAULT_MULTICHANCE 3

Tile::Tile() : Loadable(), id(0), minDiagonal(-1), minMult(-1, -1), allowTilingErrs(false), allowMultiErrs(false), multiChance(DEFAULT_MULTICHANCE), 
        rampChance(255), disregardAdj(false), disregardNorm(false), allOpaque(true), allCollisionEnabled(true), frames(), rawMelds() {
    
}

Tile::Tile(const std::string& p) : Tile() {
    this->load(p);
}

Tile::~Tile() {
}

bool Tile::couldLoad(const std::string& p) const {
    return p.size() >= 4 && p.find(".xml") != std::string::npos;
}

bool Tile::load(const std::string& p = "") {
    if(p.size() > 0) path = p;
    if(!Loadable::load(path)) return false;
    
    //std::cout << "TILE::LOAD " << path << "\n";
    
    XMLDoc doc;
    if(!doc.load(path, false)) {
      Console::Warn("Tile XML file at ",path," could not be parsed... Continuing anyway.");
      return true;
    }

    XMLNode root = doc.child("tile");
    if(!root) return true;

    if(root.child("melds")) {
        for(XMLNode tNode = root.child("melds").child("tile"); tNode; tNode = tNode.sibling()) {
            std::string meldName = tNode.get<std::string>("type");
            if(meldName.size() > 0) rawMelds.push_back(meldName);
        }
    }
    
    if(root.child("allowErrs")) allowTilingErrs = true;
    if(root.child("disregardAdj")) disregardAdj = true;
    if(root.child("disregardNorm")) disregardNorm = true;
    
    XMLNode multis;
    if((multis = root.child("multi"))) {
        multiChance = multis.get<unsigned char>("chance", DEFAULT_MULTICHANCE);//(unsigned char)clamp(atoi(), 0, 255);
        rampChance = multis.get<unsigned char>("ramp", 255);
    }
    
    if(root.child("lock")) {
        for(XMLNode tNode = root.child("lock").child(); tNode; tNode = tNode.sibling()) {
            const std::string kName = tNode.name();
            if(kName == "mirrorX") minMult.x = 1;
            else if(kName == "mirrorY") minMult.y = 1;
            else if(kName == "diagonal") minDiagonal = 1;
        }
    }

    XMLNode rend;
    if ((rend = root.child("render"))) {
        if (rend.child("offset")) {
            renderOffset.x = rend.child("offset").get<float>("x", 0.f);
            renderOffset.y = rend.child("offset").get<float>("y", 0.f);
            renderOffset.z = rend.child("offset").get<float>("z", 0.f);
        }
        if (rend.child("transparent")) allOpaque = false;

        if (rend.child("obscures")) {
            if(rend.child("obscures").get<float>("str", 1.f) < 0.5f) allOpaque = false;
        }
    }
    
    XMLNode phys;
    if((phys = root.child("physics"))) {
        if(phys.child("collision")) {
            allCollisionEnabled = phys.child("collision").get<bool>("enabled", true);
        }
    }
    
    
    return true;
}
void Tile::unload() {
    
}

Tile* Tile::Factory(const std::string p) {
    Tile* ret = new Tile();
    if(p.size() == 0) return ret;
    
    if(ret->load(p)) return ret;
    
    delete ret;
    return nullptr;
}

bool Tile::meldsWith(const Tile *const& o) const {
    if(o == this) return true;
    
    for(const std::string& meldStr : rawMelds) {
        if(o == nullptr) {
            if(meldStr.size() == 0) return true;
        } else if(o->getName() == meldStr) return true;
        else if(meldStr == "*") return true;
    }
    return false;
}

TileFrameInstance Tile::calculateFrame(TileInstance *ins, int stage) {
    if(frames.size() == 0) return nullptr;
    
    TileFrameInstance bestFrame = TileFrameInstance(*frames.begin());
    
    BLOCKDBG std::cout << "\n\nCALC FOR " << ins->coords().x << ", " << ins->coords().y << "\n\n";
    
    std::vector<TileFrameInstance> bestFrames;
    bestFrames.reserve(10);
    int leastErrors = 100;
    bool hasRamp = false;
    
    for(TileFrame* frame : frames) {
        bool isSecondPass = frame->isMulti() && !frame->isRamp;
        if(stage == 0 && isSecondPass) continue;//Skip non-ramp multiblocks on first pass
        else if(stage == 1 && !isSecondPass) continue;
        else if(frame->isRamp && (rampChance == 0 || (rampChance != 255 && rand()%rampChance == 0))) continue;
        
        int errs = 0;
        TileFrameInstance frameIns;
        int score = frame->match(ins, frameIns, &errs);
        _unused(score);
        /*if(score > bestScore) {
            bestScore = score;
            bestFrame = frameIns;
        }*/
        if((!frame->isMulti() && errs <= leastErrors) || (frame->isMulti() && allowMultiErrs && errs < std::min(leastErrors, 10)) || errs == 0) {
            BLOCKDBG std::cout << "ADDFRAME " << frame->name << ", errs="<<errs<<"\n";
            if(errs < leastErrors) {
                bestFrames.clear();
                hasRamp = false;
                BLOCKDBG std::cout << "\tCLEARFRAMES, bestFrames.size() = " << bestFrames.size() << "\n";
            }
            
            leastErrors = errs;
            BLOCKDBG std::cout << "\tbefore push, bestFrames.size() = " << bestFrames.size() << "\n";
            bestFrames.push_back(frameIns);
            /*std::cout << "\tafter, bestFrames.size() = " << bestFrames.size() << "\n\t\tVec = ";
            for(TileFrameInstance& lFrameIns : bestFrames) {
                TileFrame *frame = _derive<TileFrame>(lFrameIns.sprite);
                std::cout <<frame->name << ", ";
            }
            std::cout << "\n";*/
            if(frame->isRamp) hasRamp = true;
        }
    }
    
    for(auto it = bestFrames.begin(); it != bestFrames.end(); ) {
        auto next = std::next(it);
        TileFrame *frame = _derive<TileFrame>(it->sprite);
        
        if(hasRamp && (frame == nullptr || !frame->isRamp)) it = bestFrames.erase(it);
        else it = next;
    }
    
    bool keepPreviousFrame = stage == 1 && (multiChance == 0 || ((multiChance != 255 && rand()%multiChance == 0) ));
    keepPreviousFrame &= ins->hasFrame();
    
    if(bestFrames.size() > 0 && (leastErrors <= 0 || allowTilingErrs) && !keepPreviousFrame) {
        unsigned int sel = rand() % bestFrames.size();
        BLOCKDBG std::cout << "\nMULTIPLE FRAMES " << ins->coords().x << ", " << ins->coords().y << " ("<<bestFrames.size()<<", chose " << sel << ", hasRamp="<<hasRamp<<"): ";
        for(TileFrameInstance& lFrameIns : bestFrames) {
            TileFrame *frame = _derive<TileFrame>(lFrameIns.sprite);
            BLOCKDBG std::cout << frame->name << " ";
        }
        //bestFrame = bestFrames[sel];
        BLOCKDBG std::cout << "\n";
        BLOCKDBG std::cout << "FRAME ORIENT: mult=" << VP(bestFrames[sel].mult) << ", diag=" << VP(bestFrames[sel].diagonalMult) << ", offPX=" << VP(bestFrames[sel].offsetPX) << "\n";
        return bestFrames[sel];
    } else if(!ins->hasFrame()) {//Couldn't match, draw an err tile for debugging
        BLOCKDBG std::cout << "\nNO FRAMES " << ins->coords().x << ", " << ins->coords().y << ", setting to err...\n";
        return TileFrameInstance(TileManager::instance()->get("dev.err")->getDefault());
    } else {
        return TileFrameInstance();//Skip me
    }
}

void Tile::addFrame(TileFrame* frame) {
    frames.push_back(frame);
    frame->setTile(this);
}

/* 
 * File:   TileFrame.cpp
 * Author: galen
 * 
 * Created on April 3, 2017, 2:23 PM
 */

#include "TileFrame.h"
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include "TileInstance.h"
#include "Tile.h"
#include "sprite/SpriteFrame.h"
#include "render/DebugGroup.h"

#define TILE_ERR_SOFT (1)
#define TILE_ERR_HARD (10)

#ifndef BLOCKDBG
#define BLOCKDBG if(false)
//#define BLOCKDBG if(true)
#endif

TileFrame::TileFrame() : Sprite(), children(), nullChild(nullptr), renderOffset(V3(0.f)), tile(nullptr) {
    this->initTileVars();
}

TileFrame::TileFrame(const std::string& p) : Sprite(p), children(), nullChild(nullptr), renderOffset(V3(0.f)), tile(nullptr) {
    this->initTileVars();
}

TileFrame::~TileFrame() {
}

bool TileFrame::load(const std::string& p) {
    bool loadedOK = Sprite::load(p);
    return loadedOK;
}


void TileFrame::setConsidered(TileInstance *ins, bool s, SpriteFrame *orient) {
    if(!this->isMulti()) {
        ins->setConsidered(s, ins);//considered = s;
        return;
    }
    
    TCoord cp(0);
    for (cp.y = 0; cp.y < size.y; cp.y++) {
        for (cp.x = 0; cp.x < size.x; cp.x++) {
            TileFrame *& child = this->getChild(cp);
            TileInstance *tl; TCoord useCP = (orient == nullptr)? cp : orient->multiply(cp);
            
            tl = ins->getSibling(useCP);
            BLOCKDBG std::cout << "\t\t\tconsidered " << VP(useCP) << " from cp="<<VP(cp)<<", got tl=" << tl << ", child="<<child<<", IGNORE " << VP(useCP+ins->coords())<<"\n";
            
            if(tl != nullptr && child != nullptr) {
                tl->setConsidered(s, ins);
                BLOCKDBG std::cout << "\t\t\t\t TL SET CONSIDERED = " << tl->isConsidered()<< " AT " << VP(tl->coords()) << "\n";
            }
        }
    }
}

int TileFrame::allOrientationMultiMatch(TileInstance *ins, TileFrameInstance& targetFrameInfo, int *errs) {
    int rErrCnt = 0;
    if (errs == nullptr) errs = &rErrCnt;
    std::vector<TileFrameInstance> bestFrameInfo;
    SpriteFrame localFrameOrient;
    int lowestErrCnt = 100;
    
    BLOCKDBG std::cout << "\nAO_MULTI " << ins->coords().x << ", " << ins->coords().y << ": " << (this->name)<< "\n";

    for(int xd = 1; xd >= tile->minDiagonal; xd--) {//For every possible diagonalization- 1 to -1 was redundent, only need one diagonalization to mult to any value
        for(int yd = 1; yd >= 1; yd--) {
            if(abs(xd)+abs(yd) != 2) continue;
            
            for(int mx = 1; mx >= tile->minMult.x; mx--) {
                for(int my = 1; my >= tile->minMult.y; my--) {//And for every possible multiplier (happens 8 times)
                    if(abs(mx)+abs(my) != 2) continue;
                    
                    localFrameOrient.sprite = this;
                    localFrameOrient.makeIdentity();
                    localFrameOrient.diagonalMult = TCoord(xd, yd);
                    localFrameOrient.mult = localFrameOrient.multiply(TCoord(mx, my));
                    BLOCKDBG std::cout << "\n\t\t AO_DIAG = " << xd << ", " << yd << ", AO_MULT = " << mx << ", " <<  my <<"\n";

                    int childErrs = 0;
                    TileFrameInstance childFrameInfo;
                    this->multiMatch(ins, childFrameInfo, &childErrs, &localFrameOrient);
                    
                    //Offset AFTER matching
                    //bool flipAxis = localFrameOrient.diagonalMult != TCoord(1);
                    /*TCoord plusSize = (this->drawSizePX() - (this->size) * PX_PER_UNIT);// * inputNormal;
                    TCoord offBase = (this->size - TCoord(1)) * PX_PER_UNIT;// * inputNormal;//;/ 2;
                    //localFrameOrient.offsetPX = TCoord(0);//localFrameOrient.multiply(plusSize) + localFrameOrient.multiply(offBase);
                    TCoord offSelect = (((localFrameOrient.mult) - TCoord(1)) / -2);
                    //if(localFrameOrient.diagonalMult != TCoord(1)) offSelect.x = 1 - offSelect.x;
                    offSelect = localFrameOrient.multiplyQ1(offSelect);
                    localFrameOrient.offsetPX =  (offBase + (plusSize)*offSelect)*  localFrameOrient.diagonalPermute(inputNormal);
                    //if(localFrameOrient.diagonalMult != TCoord(1)) localFrameOrient.offsetPX *= -1;
                     * //
                    */
                    TCoord offBase(this->drawSizePX() - 1*PX_PER_UNIT);//(this->size - TCoord(1)) * PX_PER_UNIT;
                    TCoord offSel = TCoord(1) - (localFrameOrient.mult + TCoord(1))/2;
                    offBase *= offSel;
                    if(inputNormal.y< 0) offBase.y = this->drawSizePX().y - offBase.y - size.y*PX_PER_UNIT ;
                    //else if(inputNormal.y == 0) offBase.y += (this->drawSizePX().y - size.y*PX_PER_UNIT);
                    //if(inputNormal.x< 0) offBase.x = this->drawSizePX().x - offBase.x - size.x*PX_PER_UNIT ;
                    //if(localFrameOrient.diagonalMult != TCoord(1)) offBase += offSel * inputNormal * -localFrameOrient.mult * TCoord(-1, 1);
                    //if(inputNormal.y < 0 && localFrameOrient.mult.x == localFrameOrient.mult.y && localFrameOrient.mult.x > 0) offBase.y += 1;
                    //if(inputNormal.x < 0 && localFrameOrient.mult.x < 0) offBase.x = -50;//this->drawSizePX().x - offBase.x - PX_PER_UNIT;
                    localFrameOrient.offsetPX =  -localFrameOrient.diagonalPermute(offBase);// * -localFrameOrient.diagonalPermute((inputNormal));// * localFrameOrient.mult;
                    
                    
                    //localFrameOrient.offsetPX = TCoord(-offBase.y, 0);
                    if(childErrs <= lowestErrCnt) {
                        if(childErrs < lowestErrCnt) bestFrameInfo.clear();
                        
                        lowestErrCnt = childErrs;
                        bestFrameInfo.push_back(localFrameOrient);
                        BLOCKDBG std::cout << "\t\tVALID AO TRANSFORM: errs=" << childErrs << "\n";
                    }
                }
            }
        }
    }
    
    (*errs) += lowestErrCnt;
    if(bestFrameInfo.size() > 0) {
        targetFrameInfo = bestFrameInfo[rand()%bestFrameInfo.size()];;
    }
    return 100 - (100*lowestErrCnt);
}

int TileFrame::multiMatch(TileInstance *ins, TileFrameInstance& targetFrameInfo, int *errs, SpriteFrame *const orient) {
    int errCnt = 0;
    if (errs == nullptr) errs = &errCnt;
    TileFrameInstance localFrameInfo;

    TCoord cp = TCoord(0);
    
    BLOCKDBG std::cout << "MULTI " << ins->coords().x << ", " << ins->coords().y << ": " << (this->name)<< "\n";
    
    int childScores = 0, childCnt = 0;;
    for (cp.y = 0; cp.y < size.y; cp.y++) {
        for (cp.x = 0; cp.x < size.x; cp.x++) {
            TileFrame *& child = this->getChild(cp);
            TCoord useCP = ((orient == nullptr)? cp : orient->multiply(cp));
            TileInstance *tl = ins->getSibling(useCP);
            BLOCKDBG std::cout << "\tCP = " << VP(cp) << " -> USECP " <<  VP(useCP) << ", got "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<"\n";
            
            if((tl == nullptr && child == nullptr) || (tl != nullptr && tl->tile == nullptr && child == nullptr)) {
                BLOCKDBG std::cout << "\tNulls at ["<<cp.x<<","<<cp.y<<"] "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", \n";
                continue;
            }
            
            if((tl== nullptr)) {//Child is thus NOT a nullptr
                (*errs) += TILE_ERR_HARD;
                BLOCKDBG std::cout << "\tChild mismatch at ["<<cp.x<<","<<cp.y<<"] "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", " << child << " and " << tl <<"\n";
                break;//continue;
            } else if((tl->tile != this->tile && child != nullptr)||
                      (tl->tile != nullptr && child == nullptr && tl->tile == this->tile)) {
                 (*errs) += TILE_ERR_HARD;
                BLOCKDBG std::cout << "\tChild["<<cp.x<<","<<cp.y<<"]  diff tile at "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", "
                        <<tl->tile << " != "<< this->tile<<" from child=" << child << ", tl at " << VP(tl->coords())<<"\n";
                break;//continue;
            }
            
            //if() {
                 //BLOCKDBG std::cout << "\tNO CHILDat ["<<cp.x<<","<<cp.y<<"] "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", " << child << " and " << tl <<"\n";
                  //(*errs)++;
                  //break;//continue;
            //}
            
            //Skip blocks that are already spoken for
            if(tl->isConsidered()) {
                BLOCKDBG std::cout << "\tAlready considered tile ["<<cp.x<<","<<cp.y<<"] "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", " << child << " and " << tl <<"\n";
                (*errs) += TILE_ERR_HARD;
                break;
            }
            
            int childErrs = 0;
            localFrameInfo.offsetPX = TCoord(0);
            if(cp.x == 0) localFrameInfo.offsetPX.x += 1;
            if(cp.y == 0) localFrameInfo.offsetPX.y += 1;
            if(cp.x == size.x-1) localFrameInfo.offsetPX.x += -1;
            if(cp.y == size.y-1) localFrameInfo.offsetPX.y += -1;
            
            if(child != nullptr) {
                int childScore = child->match(tl, localFrameInfo, &childErrs, orient);
                childScores += childScore;
                (*errs) += childErrs;
                BLOCKDBG std::cout << "\tchild score = " << childScore << ", errs = "<<childErrs<<", considered=" << tl->isConsidered() << "\n";
                childCnt++;
            } else {
                  BLOCKDBG std::cout << "\tSkipping tile ["<<cp.x<<","<<cp.y<<"] "<<(ins->coords().x+useCP.x) << ", " << (useCP.y+ins->coords().y) <<", " << child << " and " << tl <<"\n";
            }
        }
        
        if(cp.x != size.x) break;//Inner loop broke
    }
    
    if(orient == nullptr) {
        targetFrameInfo.mult = targetFrameInfo.diagonalMult = TCoord(1);
    } else {
        targetFrameInfo.makeIdentity();
        targetFrameInfo.diagonalMult = orient->diagonalMult;
        targetFrameInfo.mult = -orient->mult;//targetFrameInfo.multiply(orient->mult);//Make valid, even with our diagonal transform
        //targetFrameInfo.offset = (targetFrameInfo.multiplyQ1(TCoord(1)) - TCoord(1)) * this->size / 2;
    }
    targetFrameInfo.sprite = this;
    
    int rScore = 0;
    if(childCnt != 0) rScore = (childScores/childCnt);
    BLOCKDBG std::cout << "\tMulti score = " << rScore << " from " <<childScores<<"/"<<childCnt<<", errs = " << *errs << "\n";
    
    return rScore;
}

int TileFrame::match(TileInstance *ins, TileFrameInstance& targetFrameInfo, int* errs, SpriteFrame *const orient) {
    if(ins == nullptr) {
        if(errs != nullptr) (*errs) += 100;
        return 0;
    } else if(this->tile == nullptr) {
        Console::Err("TileFrame::match called with null tile ptr!");
        if(errs != nullptr) (*errs) += 100;
        return 0;
    }
    
    //Not yet implemented, so disable
    if(this->isTransition()) {
        if(errs != nullptr) (*errs) += 100;
        return 0;
    }
    
    if(this->isMulti()) return this->allOrientationMultiMatch(ins, targetFrameInfo, errs);
    
    TCoord multiblockNeighborDir = TCoord(0);
    if(fake && orient != nullptr)  multiblockNeighborDir = targetFrameInfo.offsetPX;//As passed by multimatch
    
    //Multiblocks never actually reach this code (matchOrientations or matchMulti, so this is always 1)
    int rad = 1;//std::max(size.x, size.y);
    int score = 100;
    
    TCoord sameVec = TCoord(0), anyVec = TCoord(0), localVec = TCoord(0), allVec = TCoord(0);
    
    if(this->rawGrad > 1 || this->span > 1 || this->dmgLevel >= 0 || this->brokeLevel >= 0) {
        if(errs != nullptr) (*errs) += 100;
        return 0;
    }
    
    BLOCKDBG std::cout << "begin match...\n";
    
    for(int ox = -rad; ox <= rad; ox++) {
        for(int oy = -rad; oy <= rad; oy++) {
            if(ox == 0 && oy == 0) continue;
            TCoord off = TCoord(ox, oy);
            TileInstance *neighbor = ins->getSibling(off);
            if(neighbor == nullptr || neighbor->tile == nullptr) continue;
            
            bool meldsTile = this->tile != nullptr && neighbor->tile != nullptr && this->tile->meldsWith(neighbor->tile);
            
            //if(off == -getInputNormal(orient)) score += 1;
            if(abs(ox)+abs(oy) == 1) {
                BLOCKDBG std::cout << "\tneighbor at " << ox << ", " << oy << ", myTile=" << this->tile << " and neighborTile=" << neighbor->tile <<"\n";
                if(meldsTile) {
                    sameVec += off;
                }
                
                anyVec += off;
            }
            if(abs(ox) <= 1 && abs(oy) <= 1 && meldsTile) {
                localVec += off;
            }
            
            allVec += off;
        }
    }
    
    //bool restrictSame = false;
    TCoord desiredNormal = TCoord(0);
    
    //Prefer melding normals, but if it would be ambiguous (center-mid or corner), consider others
    if(tcLength2(sameVec) == 1) desiredNormal = -sameVec;
    if(desiredNormal == TCoord(0)) desiredNormal = -tcNormalize(localVec);
    if(desiredNormal == TCoord(0)) desiredNormal = -anyVec;//sameVec;
    
    
    if((this->inverse || desiredNormal == TCoord(0)) && sameVec != TCoord(0)) {
        desiredNormal = -sameVec;
        //restrictSame = true;
    }
    if(desiredNormal == TCoord(0)) desiredNormal = -tcNormalize(allVec);
    
    BLOCKDBG std::cout << ins->coords().x << ", " << ins->coords().y << ": " << (this->name)<< "\n\t" <<
            "sameVec=" << VP(sameVec) << ", anyVec=" << VP(anyVec) << ", localVec=" << VP(localVec) << ", des=" << VP(desiredNormal)<<"\n";
    
    TCoord resultingNormal;
    if(orient == nullptr) {
        targetFrameInfo = TileFrameInstance(this);
        TCoord &mult = targetFrameInfo.mult, &diag = targetFrameInfo.diagonalMult;
        mult = tcDivide(desiredNormal, getInputNormal(orient));

        diag = TCoord(1,1); 
        bool needsDiagonal = (mult == TCoord(0) && desiredNormal != TCoord(0) && getInputNormal(orient) != TCoord(0));
        TCoord multPos = desiredNormal - mult * getInputNormal(orient);
        BLOCKDBG std::cout << "\trawMult=" << VP(mult) << ", needsDiag=" << needsDiagonal << "\n";
        if(needsDiagonal && multPos.x == 0) diag = TCoord(-multPos.y, multPos.y);
        else if(needsDiagonal && multPos.y == 0) diag = TCoord(multPos.x, -multPos.x);
        if(diag.x == 0) diag.x = 1;
        if(diag.y == 0) diag.y = 1;

        if(needsDiagonal) {
            mult = TCoord(1,1);//For multiply to work, by reference
            BLOCKDBG std::cout << "pre-adjust projection: " << VP(targetFrameInfo.multiply(getInputNormal(orient))) << "\n";
            mult = tcDivide(targetFrameInfo.multiply(getInputNormal(orient)), desiredNormal);
            mult = targetFrameInfo.diagonalPermute(mult);
            BLOCKDBG std::cout << "\tadjusted mult to " << VP(mult) << "\n";
        }
        if(!fake && mult.x == 0) mult.x = 1;
        if(!fake && mult.y == 0) mult.y = 1;
        
        mult = max(tile->minMult, mult);
        if(diag.y < 1) {
            diag.y = 1;
            diag.x *= -1;
        }
        diag.x = max(diag.x, tile->minDiagonal);
        
        resultingNormal = targetFrameInfo.multiply(getInputNormal(orient));
    } else if(orient != nullptr) {
        targetFrameInfo = *_derive<TileFrameInstance>(orient);
        resultingNormal = getInputNormal(orient);
    }
    
    TCoord relXAxis = targetFrameInfo.multiply(TCoord(1,0)), relYAxis = targetFrameInfo.multiply(TCoord(0,1));
    
    //Ignore normal errors in an ignored direction
    TCoord closeFreeVec = (1-getOpens(orient).x)*relXAxis + (1-getOpens(orient).y)*relYAxis;
    //TCoord closeMult = tcDivide(desiredNormal, closeFreeVec);
    
    //Use best, but don't allow "free" results if we're using a forced orientation
    int resErrMult = tcLength(resultingNormal);
    //if(orient != nullptr) resErrMult = 1;
    
    int resultingError = std::min(tcLength2(abs(desiredNormal - resultingNormal)) * resErrMult,
                                  tcLength2(abs(desiredNormal - resultingNormal) + closeFreeVec) * resErrMult);
    if(inputNormal == TCoord(0) && getOpens(orient) == TCoord(0) && this->fake) resultingError = 0;//Closed blocks can't disable themselves (in a multiblock)
    
    //if(resultingError > 0) return 0;
    if(errs != nullptr && !tile->disregardNorm) (*errs) += resultingError * TILE_ERR_SOFT;
    
    int errorMult = (1+resultingError )* 10 + tcHamming(targetFrameInfo.mult)*3;
    int rewardMult = 10;;
    score -= resultingError*25 + std::max(tcHamming(targetFrameInfo.diagonalMult), tcHamming(targetFrameInfo.mult)) + tcHamming(getOpens(orient) * 2 - 1)*2;
    //score -= tcHamming(diag)*5;
    //score -= tcHamming(mult)*3;
    
    /*if(this->inverse) {
        resultingNormal *= -1;
        desiredNormal *= -1;
    }*/
    
    BLOCKDBG std::cout << "\tcur Err = " << (errs == nullptr? -1 : (*errs)) <<", resErr="<<resultingError<<", errMult = " << errorMult<<", rewardMult = " << rewardMult << "\n";
    BLOCKDBG std::cout << "\tmult=" << VP(targetFrameInfo.mult) << ", diag=" << VP(targetFrameInfo.diagonalMult) << ", getOpens(orient)=" << VP(getOpens(orient)) << ", inverse=" << this->inverse << "\n";
    BLOCKDBG std::cout << "\tin="<<VP(getInputNormal(orient))<<", out="<<VP(resultingNormal) << ", desired=" << VP(desiredNormal) << "\n";
    BLOCKDBG std::cout << "\trelX=" << VP(relXAxis) << ", relY=" << VP(relYAxis) << ", closeFree="<<VP(closeFreeVec) << "\n";
    
    int densityScore = 0, densityCount = 0;
    //Score by required adjacentcies
    for(int ox = -rad; ox <= rad; ox++) {
        for(int oy = -rad; oy <= rad; oy++) {
            if(ox == 0 && oy == 0) continue;
            TCoord off = TCoord(ox, oy); int dist = tcLength(off), sqDist=tcLength2(off);
            TileInstance *neighbor;
            if(orient != nullptr && false) neighbor = ins->getSibling(orient->multiply(off));
            else neighbor = ins->getSibling(off);
            //if(neighbor == nullptr) continue;
            
            TCoord inputOffset = TCoord(tcContrib(off, relXAxis), tcContrib(off, relYAxis) );
            int dot = tcDot(resultingNormal, tcNormalize(off));//, idealDot = tcDot(desiredNormal, tcNormalize(off));
            int contrib = -tcContrib(resultingNormal, off);
            if(tcLength2(resultingNormal) == 1 && contrib == 0) contrib = tcLength(inputOffset * getOpens(orient));
            
            int density = int(neighbor == nullptr || neighbor->tile != nullptr) * 2 - 1;
            
            int openContrib = sgn(tcContrib((TCoord(1)-getOpens(orient)), inputOffset));
            if(contrib == 0 && openContrib != 0) contrib = -1;//openContrib;
            else if(openContrib != 0) contrib *= -openContrib;
            
            bool meldedNeighbor = ins->tile != nullptr && neighbor != nullptr && ins->tile->meldsWith(neighbor->tile);
            if(density != 0 && !meldedNeighbor /*&&restrictSame*/) density = -1;//*= 1 - 2*int(neighbor == nullptr || neighbor->tile != ins->tile);
            
            //It should be one (and not sq length) because a lack of direct neighbors implies the diagonals (consider tiles in a checkerboard pattern)
            if(dist == 1 && sqDist <= std::max(1, tcLength2(resultingNormal))) {//std::max(1, tcLength2(resultingNormal))) {
                BLOCKDBG std::cout << "\t\t\t"<<off.x<<", "<<off.y<<"  dot="<<dot<<", contrib="<<contrib<<", openContrib="<<openContrib<<" dist="<<dist << ", density="<<density<<", inputOffset=" << VP(inputOffset) << "\n";
                BLOCKDBG std::cout << "\t\t\tOFF=" << VP(off) << ", rNorm=" << VP(resultingNormal) << ", inv=" << inverse << "\n";
                
                int expectedDensity = sgn(/*tcLength(inputOffset * getOpens(orient)) **/ contrib);// * (this->inverse? -1 : 1));
                if(inputNormal == TCoord(0) && openContrib == 0 && getOpens(orient) != TCoord(0)) {//Mid-centers expect to be surrounded
                    expectedDensity = 1;
                } else if((resultingNormal == off)) {// || (inverse && resultingNormal == off*-1)) {// tcLength2(resultingNormal) > 1) {//Corners don't care about kittycorner c
                    if(tcLength2(resultingNormal) >= 2) expectedDensity = 0;
                    else expectedDensity = -1;
                    BLOCKDBG std::cout << "\t\t\tZEROING EX DENSITY\n";
                } else if(this->inverse && expectedDensity < 0) {//Invert corners only differ on these (the blocks corners negate, ie, "in front" of normal but not exact normal
                    expectedDensity *= -1;
                }
                
                //Multiblock rules override normal ones
                if(getOpens(orient) != TCoord(1) && this->fake) {//Unless they're multis
                    int multiPosContrib = tcContrib(inputOffset, multiblockNeighborDir);
                    BLOCKDBG std::cout << "\t\t\tCHECKMPC: " << VP(inputOffset) << ", " <<VP(multiblockNeighborDir) << " = " << multiPosContrib << "\n";
                    if(multiPosContrib > 0) expectedDensity = 0;//We're an edge, and we're querying inside our own block
                }
                
                //Allow edges facing other non-interacting blocks to be drawn
                if(tcLength2(resultingNormal) == contrib && contrib > 0 &&
                        neighbor != nullptr && neighbor->tile != nullptr && 
                        !tile->meldsWith(neighbor->tile)) expectedDensity = 0;
                
                //if(this->inverse) expectedDensity *= -1;
                int dDensity = density * expectedDensity;// * sgn(tcLength(inputOffset * getOpens(orient)));
                BLOCKDBG std::cout << "\t\t\t\texpected=" << expectedDensity<<" for dDensity=" << dDensity << "\n";
                densityScore += dDensity;//*select(dDensity,rewardMult, errorMult);
                densityCount++;
                if(dDensity < 0 && errs != nullptr && !tile->disregardAdj) {
                    (*errs) = (*errs) + TILE_ERR_SOFT;
                    BLOCKDBG std::cout << "ERROR DETECTED, errs="<< (*errs) << "\n";
                }
            }
        }
    }
    int totalDensityScore = float(densityScore)/float(densityCount) * float(50);
    score += totalDensityScore;
    
    //bool flipAxis = targetFrameInfo.diagonalMult != TCoord(1);
    //TCoord plusSize = (this->drawSizePX() - (this->size) * PX_PER_UNIT) * inputNormal;
    //TCoord offSelect = (targetFrameInfo.mult - TCoord(1)) / -2;
    //TCoord offBase = (this->size - TCoord(1)) * PX_PER_UNIT / 2;
    //targetFrameInfo.offsetPX = plusSize*offSelect;//this->drawSizePX()/2;//targetFrameInfo.multiply(plusSize) + targetFrameInfo.multiply(offBase);
    TileFrameInstance &localFrameOrient = targetFrameInfo;
            
                    TCoord offBase(this->drawSizePX() - 1*PX_PER_UNIT);//(this->size - TCoord(1)) * PX_PER_UNIT;
                    offBase *= TCoord(1) - (localFrameOrient.mult + TCoord(1))/2;
                    if(inputNormal.y< 0) offBase.y = this->drawSizePX().y - offBase.y - PX_PER_UNIT;
                    if(inputNormal.y < 0 && localFrameOrient.mult.x == localFrameOrient.mult.y && localFrameOrient.mult.x > 0) offBase.y += 1;
                    //if(inputNormal.x < 0 && localFrameOrient.mult.x < 0) offBase.x = -50;//this->drawSizePX().x - offBase.x - PX_PER_UNIT;
                    localFrameOrient.offsetPX =  -localFrameOrient.diagonalPermute(offBase);// * -localFrameOrient.diagonalPermute((inputNormal));// * localFrameOrient.mult;
    
    //std::cout << ins->coords().x << ", " << ins->coords().y << ": " << (this->name)<< "\n";
    BLOCKDBG std::cout << "\tmult=" << VP(targetFrameInfo.mult) << ", diag=" << VP(targetFrameInfo.diagonalMult) << "\n";
    //std::cout << "\tin="<<VP(getInputNormal(orient))<<", out="<<VP(resultingNormal) << ", desired=" << VP(desiredNormal) << "\n";
    BLOCKDBG std::cout << "\tscore " << score << " from err=" << (errs == nullptr? -1 : (*errs)) << ", hamming=" <<tcHamming(targetFrameInfo.diagonalMult) << "\n";
    //score -= tcHamming(mult)*4;
    //if(this->inverse) targetFrameInfo.mult *= -1;//Inverse normal on inverse tiles

    return score;
}

TCoord TileFrame::getInputNormal(SpriteFrame *const orient) const {
    if(orient == nullptr) return inputNormal;
    else return orient->multiply(inputNormal);
}
TCoord TileFrame::getOpens(SpriteFrame *const orient) const {
    if(orient == nullptr) return opens;
    else return orient->diagonalMultiply(opens);
    //orient->multiplyQ1(opens);
}

N2 TileFrame::drawSize(bool flip) const {
    //if(useSize.x < 0 || useSize.y < 0) useSize = size;
    N2 useSize = N2(1.f);
    N2 sizeMod = this->imgSize() / N2(float(DEF_TILESIZE));
    if(flip) return N2(float(useSize.x)*sizeMod.y, float(useSize.y)*sizeMod.x);
    else return N2(float(useSize.x)*sizeMod.x, float(useSize.y)*sizeMod.y);
}

template<typename T, typename K = T>
inline static bool procTFTag(const std::string& tag, const std::string &dat, T& val, const K& defVal = T(1)) {
    if(dat.size() < tag.size()) return false;
    
    auto it = dat.find(tag);
    if(it == std::string::npos || it != 0) return false;
    
    if(dat.size() > tag.size()) {
        const std::string numStr = dat.substr(tag.size(), dat.size()-tag.size());
        val = T(atoi(numStr.c_str()));
    } else val = defVal;
    
    //std::cout << "\t\t\t\tTFSET " << tag << " = " << val << "\n";
    return true;
}

void TileFrame::prepare() {
    
    std::string base = this->getBaseName(this->subPath), part;
    //std::cout << "\t\tBASE: " << base << "\n";
    BLOCKDBG std::cout << "\t\tPREP TILE: " << this->name << "\n";
    std::istringstream issBase(base);
    
    unsigned int partIdx = 0;
    TCoord *readNormal = &(this->inputNormal);
    
    while((std::getline(issBase, part, '_'))) {
        if(partIdx == 0) { 
            partIdx++;
            continue;
        }
        //std::cout << "\t\t\tpart:" << part << "\n";
        bool matched = false;
        matched |= procTFTag("dmg"     , part, this->dmgLevel       , 1);
        matched |= procTFTag("broke"   , part, this->brokeLevel     , 1);
        matched |= procTFTag("ramp"    , part, this->isRamp         ,  true);
        
        matched |= procTFTag("up"      , part, readNormal->y        ,  1);
        matched |= procTFTag("cen"     , part, readNormal->y        ,  0);
        matched |= procTFTag("down"    , part, readNormal->y        , -1);
        
        matched |= procTFTag("left"    , part, readNormal->x        , -1);
        matched |= procTFTag("mid"     , part, readNormal->x        ,  0);
        matched |= procTFTag("right"   , part, readNormal->x        ,  1);
        
        matched |= procTFTag("inv"     , part, this->inverse        ,  true);
        
        matched |= procTFTag("closeX"  , part, this->opens.x        ,  0);
        matched |= procTFTag("closeY"  , part, this->opens.y        ,  0);
        matched |= procTFTag("det"     , part, this->detailID       ,  0);
        
        matched |= procTFTag("trans"   , part, this->transID        ,  0);
        if(this->transID != 0) {
            readNormal = &(this->transVec);
        }
        
        if(!matched) {//Potential collisions
            matched |= procTFTag("x"       , part, this->size.x    ,  1);
            matched |= procTFTag("y"       , part, this->size.y    ,  1);
            matched |= procTFTag("v"       , part, this->versionID ,  0);
        }
        
        partIdx++;
    }
    
    //Correct ramps that start from a null block
    if(isRamp && this->inputNormal == TCoord(-1, -1)) {
        Sprite::flip(true, true);
        inputNormal = -inputNormal;
    }
    
    //TODO: auto-resize to nearest tile multiple
    
    //Having corrected, we may prepare the sprite
    Sprite::prepare();
    //If we can, omit faces of the generated polygon that won't be interacted with ("below" normal)
    if(this->polygon() != nullptr && this->inputNormal != TCoord(0)/* && this->name == "srx.rock_up_right_v1"*/) {
        BLOCKDBG std::cout << "\nVIEW CULL " << name << "\n";
        Poly::Viewpoint view;
        view.start = (N2( inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
        view.end   = (N2(-inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
        view.ortho = true;
        view.bounded = true;
        if(tcLength2(inputNormal) > 1) {//Instead of defaults, pass special axes for diagonals (limit to the corner of the image);
            view.axes[0] = N2(inputNormal.x, 0.f);
            view.axes[1] = N2(0.f, -inputNormal.y);
        } else {
            view.admissable = true;
        }
        std::cout << "PROJECTING " << this->name << "\n";
        this->polygon()->projectOnto(view);
        std::cout << "END PROJECT\n\n";
        /*
        float eps = 0.8f;
        float relEps = 0.0f;
        unsigned int prevSize = this->polygon()->size(), curSize;
        while (prevSize - (this->polygon()->simplify(eps, relEps), curSize = this->polygon()->size()) > (2 + prevSize / 10)) {
            prevSize = curSize;
        }
        this->polygon()->repair();*/
    }
    
    
    
    cap = opens.x != 1 || opens.y != 1;
    if(isRamp) {
        ramp = TCoord(-this->inputNormal.x, this->inputNormal.y);
        if(size.y == 1) size.y = 2;
    }
    
    if(this->isMulti()) {
        children.resize(int(size.x * size.y));
        TCoord cp = TCoord(0);
        TCoord rampStart;
        
        //if(this->inverse) ramp *= TCoord(-1, 1);
        if(ramp.x > 0 && ramp.y < 0) rampStart = TCoord(size.x-1, 0);//down left
        if(ramp.x < 0 && ramp.y < 0) rampStart = TCoord(0, 0);//down right
        if(ramp.x < 0 && ramp.y > 0) rampStart = TCoord(0, size.y-1);//up right
        if(ramp.x > 0 && ramp.y > 0) rampStart = TCoord(size.x-1, size.y-1);//up left
        
        BLOCKDBG std::cout <<"Assembling multi, rampStart = " << VP(rampStart) << ", ramp=" << VP(ramp) << ":\n";
        for(cp.y = 0; cp.y < size.y; cp.y++) {
            for(cp.x = 0; cp.x < size.x; cp.x++) {
                TileFrame *& child = this->getChild(cp);
                
                bool isVoid = false;
                isVoid |= isRamp && cp == tcRollover(rampStart + TCoord(ramp.x, 0), size);
                if(isVoid) {
                    child = nullptr;
                    continue;
                }
                
                child = new TileFrame();
                
                TCoord childNormal = inputNormal, childOpens = TCoord(1);
                
                bool isInternal = false;
                isInternal |= !isRamp && cp.x > 0 && cp.y > 0 && cp.x < size.x-1 && cp.y < size.y-1;
                isInternal |= isRamp && cp != rampStart && cp != tcRollover(rampStart + ramp, size);
                
                BLOCKDBG std::cout << "\t\t\tCP="<<cp.x<<", " << cp.y << ", isInternal=" << isInternal << "\n";
                
                if(isInternal) {
                    child->inputNormal = TCoord(0);
                    child->opens = TCoord(1,1);
                    bool isSubEdge = isRamp;
                    if(isSubEdge) {
                        child->inputNormal = inputNormal;
                        child->inverse = true;
                    }
                } else {
                    bool isEdge = false;
                    isEdge |= isRamp && (cp == rampStart || cp == tcRollover(rampStart + ramp, size));
                    isEdge |= !isRamp && !isInternal;
                    //TCoord edgeMult = TCoord(1 - int(cp.x == 0 || cp.x == size.x-1), int(cp.y == 0 || cp.y == size.y-1));
                    
                    BLOCKDBG std::cout << "\t\t\tisEdge=" << isEdge << ", opens="<<VP(opens)<<", sz=" << VP(size) << ", defChildNorm=" << VP(childNormal) << "\n";
                    if(isRamp) {
                        if(this->inverse) {
                            if(cp == tcRollover(rampStart + ramp, size)) {
                                child->inputNormal = TCoord(0, inputNormal.y);
                            } else {
                                child->inputNormal = TCoord(inputNormal.x, 0);
                            }
                        } else {
                            if(cp == tcRollover(rampStart + ramp, size)) {
                                child->inputNormal = childNormal * TCoord(0, 1);
                            } else {
                                child->inputNormal = childNormal;
                            }
                        }
                    } else {
                        child->inputNormal = childNormal;
                        
                        if(cp.x == 0 || cp.x == size.x-1) {
                            childOpens.x = opens.x;
                            if(opens.x != 1 && size.x > 1) child->inputNormal.x += (cp.x == 0)? -1 : 1;
                        }
                        if(cp.y == 0 || cp.y == size.y-1) {
                            childOpens.y = opens.y;
                            if(opens.y != 1 && size.y > 1) child->inputNormal.y += (cp.y == 0)? -1 : 1;
                        }
                        
                    }
                    BLOCKDBG std::cout << "\t\t\t\t Got normal=" << VP(child->inputNormal) << "\n";
                }
                
                child->opens = childOpens;
                child->fake = true;
                //Not yet valid, wait for setTile()
                //child->tile = this->tile;
            }
        }
        
        for(cp.y = size.y-1; cp.y >= 0; cp.y--) {
            for(cp.x = 0; cp.x < size.x; cp.x++) {
                TileFrame *& child = this->getChild(cp);
                BLOCKDBG std::cout << ((child == nullptr)?"0 " : (child->inputNormal == TCoord(0)? "H" : "X")) << " ";
            }
            BLOCKDBG std::cout << "\n";
        }
        
        if(this->isRamp) {
            this->inverse = false;
        }
    }
    //gradient = inputNormal * TCoord(span, rawGrad);
    //std::cout << "GOT Name=" << this->name << "\n";
    //if(name == "srx.grass_up_left_ramp_x2_v0") this->generatePhysics();
}

//#include "lib/inlDev.h"




void TileFrame::drawDebug(DebugGroup *dbg, SpriteInstance *ins) {
    Sprite::drawDebug(dbg, ins);
    return;
    //N2 rawStart = (N2( inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
    //N2 rawEnd = (N2(-inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
    if(inputNormal == TCoord(0)) return;
    Poly::Viewpoint view;
        view.start = (N2( inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
        view.end   = (N2(-inputNormal)*N2(1.f, -1.f) + N2(1.f))/2.f * this->imgSize();
        view.ortho = true;
        if(tcLength2(inputNormal) > 1) {//Instead of defaults, pass special axes for diagonals (limit to the corner of the image);
            view.axes[0] = N2(inputNormal.x, 0.f);
            view.axes[1] = N2(0.f, -inputNormal.y);
        } else {
            view.axes[0] = N2(-inputNormal.y, inputNormal.x);
            view.axes[1] = -view.axes[0];
            view.admissable = true;
        }
    //Poly::Projection proj = this->polygon()->projection(view);
    
    N2 pS =  ins->localToParent(view.start);
    N2 pE =  ins->localToParent(view.end);
    
    dbg->setLevel(1);
    dbg->drawPoint(pS, ColorRGBA(0, 0, 255, 255), 0.08f);
    dbg->drawPoint(pE, ColorRGBA(0, 255, 0, 255), 0.08f);
    
    
    /*
    if(proj) {
        dbg->drawPoint(ins->localToParent(proj.intersections[0]), ColorRGBA(127, 127, 255, 255), 0.03f);
        dbg->drawPoint(ins->localToParent(proj.intersections[1]), ColorRGBA(127, 127, 255, 255), 0.03f);

        dbg->drawPoint(ins->localToParent(proj.points[0]->positionConst()), ColorRGBA(255, 255, 255, 255), 0.04f);
        dbg->drawPoint(ins->localToParent(proj.points[1]->positionConst()), ColorRGBA(255, 255, 255, 255), 0.04f);
        
        for(PhysicsPt* pt = proj.points[0]->sibling(proj.winding); pt != proj.points[1]; pt = pt->sibling(proj.winding)) {
            dbg->drawPoint(ins->localToParent(pt->positionConst()), ColorRGBA(0, 0, 0, 255), 0.03f);
        }
    }*/
    
    N2 normAE = ins->localToParent(view.axes[0]*5.f+ view.end), 
            normBE = ins->localToParent(view.axes[1]*5.f + view.end);
    dbg->drawLine(pE, normAE, ColorRGBA(255, 0, 0), 0.033);
    dbg->drawLine(pE, normBE, ColorRGBA(255, 255, 0), 0.033);
}

void TileFrame::generatePhysics() {
    Sprite::generatePhysics();
    
    /*if(this->poly != nullptr && inputNormal != TCoord(0)) {
        N2 localNormal = N2(inputNormal.x, -inputNormal.y);
        N2 localCastDir = -localNormal;
        N2 axisMult = (localCastDir + N2(1.f) / 2.f);
        
        N2 axisA, axisB, basis;
        //if(tcLength2(inputNormal) == 1) {//only one axis
            
        //} else {
            basis = axisMult * this->imgSize();
            axisA = normalize(N2(axisMult.x, 1.f - axisMult.y) * localNormal);
            axisB = normalize(N2(1.f - axisMult.y, axisMult.y) * localNormal);
        //}
        
        this->poly->projectOnto(localCastDir, basis, axisA, axisB);
    }*/
}

std::string TileFrame::tileName() const {
    return this->getBaseName(this->subPath, "_", "/.");
}

bool TileFrame::isMulti() const {
    return this->size.x > 1 || this->size.y > 1;
}

void TileFrame::setTile(Tile *const tPtr) {
    this->tile = tPtr;
    renderOffset += tPtr->renderOffset;
    opaque &= tPtr->allOpaque;
    filling &= tPtr->allCollisionEnabled;
    collides &= tPtr->allCollisionEnabled;
    if(children.size() > 0) {
        for(TileFrame *const& child : children) if(child != nullptr) child->setTile(tPtr);
    }
}

TileFrame*& TileFrame::getChild(TCoord off, SpriteFrame *const orient) {
    if(orient != nullptr) {
        //std::cout << "\t\t\tgetChild Transform Q1: " << VP(off) << ", orientMult=" << VP(orient->mult)<<", diag="<<VP(orient->diagonalMult) <<"\n";
        off = orient->multiplyQ1(off, this->size);
        //std::cout << "\t\t\tgot off=" << VP(off) << "\n";
    }
    unsigned int idx = (unsigned int)(off.y*size.x + off.x);
    //std::cout << "\t\t\tfor childIdx=" << idx << " from size=" << VP(size) << "\n";
    if(idx >= children.size()) return nullChild;
    else return children[idx];
}

TileFrame* TileFrame::Factory(const std::string p) {
    TileFrame* ret = new TileFrame();
    if (p.size() == 0) return ret;

    if (ret->load(p)) return ret;

    delete ret;
    return nullptr;
}

void TileFrame::initTileVars() {
    parentID = 0;
    inputNormal =  TCoord();
    versionID = 0;
    transID = 0;
    span = 1;
    rawGrad = 1;
    dmgLevel = brokeLevel = -1;
    detailID = 0;
    opens = TCoord(1,1);
    size = TCoord(1);
    transVec = TCoord(0);
    
    isRamp = false;
    cap = false;
    corner = false;
    round = false;
    airtight = true;
    opaque = true;
    grabable = true;
    inverse = false;
    ramp = TCoord(0);
    fake = false;
    filling = true;
    collides = true;
    //for(int i = 0; i < 4; i++) {
    //    opens[i] = opens[i] = TCoord(0,0);
    //}
}


bool TileFrame::isConstrained() const {
    if(this->img == nullptr) return true;
    
    if(this->img->width % DEF_TILESIZE != 0 || this->img->height % DEF_TILESIZE != 0) return false;
    else return true;
}

float TileFrame::getZOffset() const {
    if(this->renderOffset.z != 0.f) return this->renderOffset.z;
    else if(this->isRamp) return 0.25f;
    else if(!this->isConstrained()) return 0.15f;
    else if(this->inputNormal != TCoord(0)) return 0.05f;
    else return 0.0f;
}
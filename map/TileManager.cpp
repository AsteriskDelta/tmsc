#include "TileManager.h"

TileManager* TileManager::insPtr = nullptr;

TileFrameManager::TileFrameManager(TileManager *parent) : LoadableManager<TileFrame>(), tiles(parent) {
    
}
TileFrameManager::~TileFrameManager() {
    
}

void TileFrameManager::wasLoaded(TileFrame *frame) {
    LoadableManager<TileFrame>::wasLoaded(frame);
    if(frame == nullptr) {
        Console::Err("TileFrameManager::wasLoaded(nullptr)");
        return;
    }
    tiles->addFrame(frame->tileName(), frame);
}


TileManager::TileManager() : frames(this){
    insPtr = this;
}

TileManager::~TileManager() {
    if(insPtr == this) insPtr = nullptr;
}

void TileManager::wasLoaded(Tile *tile) {
    LoadableManager<Tile>::wasLoaded(tile);
    //std::cout << "TILE: " << tile->name << " @ " << tile->path << " sub " << tile->subPath << "\n";
}

void TileManager::loadFrames() {
    for(auto& sourceDir : sourceDirs) frames.loadFromDir(sourceDir);
}

void TileManager::addFrame(const std::string& tileName, TileFrame *frame) {
    Tile* tile = this->get(tileName);
    if(tile == nullptr || frame == nullptr) return;
    //std::cout << "\tADDFRAME " << tileName << " with " << frame->path << "\n";
    frame->prepare();
    tile->addFrame(frame);
}

void TileManager::buildCrossref() {
    
}

TileManager* TileManager::instance() {
    return insPtr;
}
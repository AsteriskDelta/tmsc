#ifndef TILE_H
#define TILE_H
#include "../TMSCInclude.h"
#include <ARKE/Loadable.h>
#include <ARKE/Image.h>
#include <NVX/NPolygon.h>
#include "TileFrameInstance.h"

class TileFrame;
class TileInstance;

class Tile : public virtual Loadable {
friend class TileManager;
public:
    Tile();
    Tile(const std::string& p);
    virtual ~Tile();
    
    inline virtual std::string getName() const override {
        return this->getBaseName(subPath, "", "/.");
    }

    virtual bool couldLoad(const std::string& p) const override;
    virtual bool load(const std::string& p) override;
    virtual void unload() override;
    inline virtual void* rawInstancePtr() override { return reinterpret_cast<void*>(this); };
    
    static Tile* Factory(const std::string p = "");
    
    void addFrame(TileFrame* frame);
    TileFrameInstance calculateFrame(TileInstance *ins, int stage = -1);
    
    bool meldsWith(const Tile *const& o) const;
    
    idint id;
    
    int minDiagonal;
    TCoord minMult;
    bool allowTilingErrs, allowMultiErrs;
    unsigned char multiChance, rampChance;
    bool disregardAdj, disregardNorm;
    bool allOpaque, allCollisionEnabled;
    
    V3 renderOffset;
    
    inline TileFrame* getDefault() {
        if(frames.size() == 0) return nullptr;
        else return frames[0];
    }
private:
    std::vector<TileFrame*> frames;
    std::vector<std::string> rawMelds;
};

#endif /* TILE_H */


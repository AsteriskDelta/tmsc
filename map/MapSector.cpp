#include "MapSector.h"
#include "render/SpriteGroup.h"
#include "render/DebugGroup.h"
#include "render/EntityGroup.h"
#include "TileInstance.h"
#include <ARKE/Renderer.h>
#include "entity/Entity.h"

MapSector::MapSector() : width(MAPSECTOR_BASE_DIM), height(MAPSECTOR_BASE_DIM), order(0), parentPos(TCoord(0,0)), spriteGroup(nullptr), dbgGroup(nullptr), entityDbg(nullptr), entityGroup(nullptr) {
}

/*MapSector::MapSector(const MapSector& orig) {
}*/

MapSector::~MapSector() {
    if(spriteGroup != nullptr) delete spriteGroup;
    if(dbgGroup != nullptr) delete dbgGroup;
    if(entityGroup != nullptr) delete entityGroup;
    if(entityDbg != nullptr) delete entityDbg;
}

MapSector* MapSector::get(CoordStack c) { 
    if(leaf) {
        Console::Err("MapSector::get with pre-transform coords called on a leaf");
        return nullptr;
    }
    
    if(!hasIdx(c.coords())) {
        MapSector* par = this->parentSector();
        if(par != nullptr) return par->getOffset(this, c);
        else {
            //Console::Err("MapSector::get on  branch has no parent and ", c.coords().x, ", ", c.coords().y, " is out of range");
            return nullptr;
        }
    }
    
    const unsigned int id = this->idx(c.coords());
    c.pop();
    MapSector* child = tiles[id];
    return child;
}

MapSector::Entry MapSector::getOffset(MapSector *child, CoordStack c) {
    TCoord &top = c.coords();
    c.push(child->parentPos);
    
    while(top.x < 0){
        c.coords().x--;
        child = this->get(c);
        if(child == nullptr) return Entry();
        top.x += child->width;
    }
    while(top.x >= child->width) {
        top.x -= child->width;
        c.coords().x++;
        child = this->get(c);
        if(child == nullptr) return Entry();
    }
    while(top.y < 0){
        c.coords().y--;
        child = this->get(c);
        if(child == nullptr) return Entry();
        top.y += child->height;
    }
    while(top.y >= child->height) {
        top.y -= child->height;
        c.coords().y++;
        child = this->get(c);
        if(child == nullptr) return Entry();
    }
    
    c.pop();
    
    Entry ret;
    if(c.size() == 1) ret.tile = child->get(c.coords());
    else ret.sec = child->get(c);
    
    return ret;
}

void MapSector::add(Entity *entity) {
    if(entity == nullptr) return;
    else if(!this->willHold(entity)) {
        return parentSector()->add(entity);
    }
    
    if(entityGroup == nullptr) {
        entityGroup = new EntityGroup(256);
        //entityGroup->setParent(this);
        //entityGroup->span() = N2(width, height);
    }
    if(entityDbg== nullptr) {
        entityDbg= new DebugGroup(256*32, true);
        //entityDbg->setParent(this);
        //entityDbg->span() = N2(width, height);
    }
    //std::cout << "MAPSECTOR ADD ENTTITY\n";
    ////entity->setSector(this);
    entity->setGroup(entityGroup);
    entity->setDebugGroup(entityDbg);
    Proxy::add<Entity*>(entity);
}
void MapSector::remove(Entity *entity) {
    if(entity == nullptr) return;
    
    //Assumed to have changed parents, don't set anything
    //entity->clearParent(this);
    entity->setGroup(nullptr);
    entity->setDebugGroup(nullptr);
    Proxy::remove<Entity*>(entity);
}

bool MapSector::canHold(Entity *entity) {
    _unused(entity);
    return true;
}
bool MapSector::willHold(Entity *entity) {
    _unused(entity);
    if(parent() != nullptr) return false;
    else return true;
}

TileInstance* MapSector::get(const TCoord& c) {
    if(!leaf) {
        if(tiles.size() > 0 && tiles[0].sec != nullptr) return tiles[0].sec->get(c);
        Console::Err("MapSector::get with finite coord called on branch, size=",tiles.size(), ", sec=",(tiles.size()>0?tiles[0].sec : 0));
        //return nullptr;
    }
    if(!hasIdx(c)) {
        MapSector* par = this->parentSector();
        if(par != nullptr) return par->getOffset(this, CoordStack(c));
        else {
            Console::Err("MapSector::get on leaf has no parent and ", c.x, ", ", c.y, " is out of range");
            return nullptr;
        }
    }
    
    const unsigned int id = this->idx(c);
    TileInstance* tile = tiles[id];
    return tile;
}

void MapSector::onSetParent() {
    this->order = parentSector()->order - 1;
    this->position() = parentSector()->getSpanTo(parentPos);
    this->span() = N2(this->getTotalSpan());
    
    sectorBounds.zero();
    sectorBounds.setType(Bounds<N2>::Square);
    sectorBounds.add(N2(0.f));//center
    sectorBounds.add(this->span());
    //std::cout << "mapsector set parent \n";
    this->computeAgnostics();
}

void MapSector::set(const TCoord& c, const Entry& entry) {
    if(!hasIdx(c)) {
        //MapSector* par = this->parentSector();
        //if(par != nullptr) return par->getOffset(this, CoordStack(c));
        //else {
            Console::Err("MapSector::set ", c.x, ", ", c.y, " is out of range");
            return;
        //}
    }
    
    const unsigned int id = this->idx(c);
    //std::cout << "MapSector::set["<<id<<"] from " << c<<"\n";
    if(leaf) {
        if(entry.tile != nullptr) {
            tiles[id] = entry;
            entry.tile->secCoords = c;
            this->add(entry.tile);
            //entry.tile->sector = this;
        } else {
            //Console::Out("MapSector::set: deleting ", c.x, ", ", c.y, "");
            if(this->get(c) != nullptr) this->remove(this->get(c));
            tiles[id] = entry;
        }
    } else {
        //std::cout << "sector set got " << entry.sec << " on self with order" << order << "\n";
        if(entry.sec != nullptr) {
            tiles[id] = entry;
            //std::cout << "updatated tile map! tiles["<<id<<"] = " << entry.sec << "\n";
            if(order > 0) {
                entry.sec->parentPos = c;
                this->add(entry.sec);
                entry.sec->setParent(this);
                entry.sec->computeAgnostics();
                //entry.sec->position() = N2(c);
                //entry.sec->size() = N2(getChildSpan());
                //entry.sec->depth = this->depth+1;
                //entry.sec->order = this->order-1;
            } else {
                Console::Err("MapSector of order 0 asked to add child, skipping addition...");
            }
        } else {
            if(this->get(c) != nullptr) this->remove(this->get(c));
            tiles[id] = entry;
        }
    }
}

MapSector* MapSector::siblingSector(const TCoord& off) {
    MapSector *const par = this->parentSector();
    if(par == nullptr) return nullptr;
    
    return par->get(CoordStack(this->parentPos + off));
}

void MapSector::computeAgnostics() {
    phyCachedBounds.setType(Bounds<N2>::Square);
    phyCachedBounds.zero();
    
    phyCachedBounds.add(this->localToAgnostic(bounds()->center) );
    phyCachedBounds.add(this->localToAgnostic(bounds()->center + bounds()->axes[0]));
    
    //std::cout << "agnostic  bounds=("<<VP(phyCachedBounds.center)<<", " <<VP(phyCachedBounds.farPoint()) << "\n";
}


const Bounds<N2>* MapSector::bounds() const {
    return &sectorBounds;
}
/*
std::unordered_set<PhysicalObject*> MapSector::getPotentialColliders(PhysicalObject* obj) {
    
}

void getPotentialCollidersRecurse(PhysicalObject *const& obj, std::unordered_set<PhysicalObject*> *const & ret) {
    
}*/

bool MapSector::hasRenderTransform() {
    return true;//this->parentSector() != nullptr;
}

Transform MapSector::getRenderTransform() {
    //if(this->parentSector() == nullptr) return Transform();
    V3 rScale = V3(1.f);//V3(childWidth, childHeight, 1.f);
    /*if(width > 1 && height > 1 && this->parentSector() != nullptr) rScale /= V3(width, height, 1.f);
    
    V3 parentScale = V3(1.f);
    if(parentSector() != nullptr) {
        parentScale = parentSector()->getRenderTransform().ratio;
    }*/
    
    return Transform(V3(V2(parentPos * this->getTotalSpan()), 0.f), Quat(), rScale);
}

void MapSector::update(float deltaTime)  {
    Updatable::update(deltaTime);
    /*if(entityGroup != nullptr && entityGroup->isDirty()) {
        entityGroup->upload();
        if(entityDbg != nullptr) entityDbg->upload();
    }*/
}

void MapSector::updateGroups() {
    //if(parentSector() != nullptr) 
    
    //std::cout << "set sector bounds to:  bounds=("<<VP(bounds()->center)<<", " <<VP(bounds()->farPoint()) << " from span="<<VP(this->span())<<"\n";
    this->computeAgnostics();
    
    const bool doDbg = true;
    if(spriteGroup ==  nullptr) {
        spriteGroup = new SpriteGroup(width*height);
        //spriteGroup->setParent(this);
        //spriteGroup->span() = N2(width, height);
        spriteGroup->registerForPass(Renderable::Pass::Map);
    }
    
    if(doDbg && dbgGroup ==  nullptr) {
        dbgGroup = new DebugGroup(width*height*32);
        //dbgGroup->setParent(this);
        //dbgGroup->span() = N2(width, height);
    } else if(doDbg) dbgGroup->clear();
    
    if(leaf) {
        TCoord pos;
        for(pos.x = 0; pos.x < width; pos.x++) {
            for(pos.y = 0; pos.y < height; pos.y++) {
                TileInstance *const tile = this->get(pos);
                if(tile == nullptr || tile->tile == nullptr || tile->isConsidered(true)) continue;
                
                if(!tile->hasGroup()) spriteGroup->attach(tile);
                else if(tile->isConsidered(true)) {
                    spriteGroup->remove(tile, false);
                    continue;
                }
                
                //std::cout << "\t\tcalling tile->updateSprite():\n";
                tile->update();
                //if(tile->isCollisionEnabled()) tile->computeAgnostics();
                if(dbgGroup != nullptr && tile->isCollisionEnabled()) tile->drawDebug(dbgGroup);
            }
        }
    }
    
    if(dbgGroup != nullptr) {
        //V3 gScale = this->getRenderTransform().ratio;//this->parent() == nullptr? (1.f/V3(childWidth, childHeight, 1.f)) : V3(1.f);//1.f /this->getRenderTransform().ratio;//V3(childWidth, childHeight, 1.f);
        V3 gOff = V3(0.0f, 0.0f, 0.f);//this->parent() == nullptr? -V3(0.5f, 0.5f, 0.f) / this->getRenderTransform().ratio *gScale : V3(-0.5f, -0.5f, 0.f) * gScale;
        gOff.z += 5.f;
        
        /*V3 gLL = V3(0.f) * gScale + gOff,
           gUR = V3(width, height, 0.f) * gScale + gOff;
        V3 gUL = V3(gLL.x, gUR.y, gLL.z), gLR = V3(gUR.x, gLL.y, gLL.z);*/
        V3 gLL = V3(V2(this->localToRender(N2(0.f))), 0.f),
                gUR = V3(V2(this->localToRender(N2(span().x, span().y))), 0.f),
                gUL = V3(V2(this->localToRender(N2(0.f, span().y))), 0.f),
                gLR = V3(V2(this->localToRender(N2(span().x, 0.f))), 0.f);
        float gw = 0.022f * sqrt(float(order)) / 2.f;// * sqrt(1.f/(depth+1)) * max(gScale.x, gScale.y);
        
        ColorRGBA col;
        if(leaf) col = ColorRGBA(0, 127, 127, 160);
        else col = ColorRGBA(0, 255, 255, 160);
        
        dbgGroup->setLevel(2);
        dbgGroup->drawLine(gLL, gUL, col, gw);
        dbgGroup->drawLine(gLL, gLR, col, gw);
        dbgGroup->drawLine(gUR, gUL, col, gw);
        dbgGroup->drawLine(gUR, gLR, col, gw);
    }
}
/*
void MapSector::prerender(int pass) {
    //std::cout << "\tMapSector::prerender(), size= " << this->size() << ", leaf=" << leaf << "\n";
    const bool doDbg = true;
    if(spriteGroup ==  nullptr) {
        spriteGroup = new SpriteGroup(width*height);
        spriteGroup->registerPass(Renderable::Map);
    }
    
    if(doDbg && dbgGroup ==  nullptr) dbgGroup = new DebugGroup(width*height*32);
    else if(doDbg) dbgGroup->clear();
    
    if(leaf) {
        TCoord pos;
        for(pos.x = 0; pos.x < width; pos.x++) {
            for(pos.y = 0; pos.y < height; pos.y++) {
                TileInstance *const tile = this->get(pos);
                if(tile == nullptr || tile->tile == nullptr) continue;
                
                //std::cout << "\t\tcalling tile->updateSprite():\n";
                tile->updateSprite();
                if(dbgGroup != nullptr && tile->sprite != nullptr && tile->isCollisionEnabled()) tile->sprite->drawDebug(dbgGroup);
            }
        }
    }
    
    if(dbgGroup != nullptr) {
        V3 gScale = this->getRenderTransform().ratio;//this->parent() == nullptr? (1.f/V3(childWidth, childHeight, 1.f)) : V3(1.f);//1.f /this->getRenderTransform().ratio;//V3(childWidth, childHeight, 1.f);
        V3 gOff = -V3(0.5f, 0.5f, 0.f);//this->parent() == nullptr? -V3(0.5f, 0.5f, 0.f) / this->getRenderTransform().ratio *gScale : V3(-0.5f, -0.5f, 0.f) * gScale;
        gOff.z += 5.f;
        
        V3 gLL = V3(0.f) * gScale + gOff,
           gUR = V3(width, height, 0.f) * gScale + gOff;
        V3 gUL = V3(gLL.x, gUR.y, gLL.z), gLR = V3(gUR.x, gLL.y, gLL.z);
        float gw = 0.022f * sqrt(float(order)) / 2.f;// * sqrt(1.f/(depth+1)) * max(gScale.x, gScale.y);
        
        ColorRGBA col;
        if(leaf) col = ColorRGBA(0, 127, 127, 160);
        else col = ColorRGBA(0, 255, 255, 160);
        
        dbgGroup->setLevel(2);
        dbgGroup->drawLine(gLL, gUL, col, gw);
        dbgGroup->drawLine(gLL, gLR, col, gw);
        dbgGroup->drawLine(gUR, gUL, col, gw);
        dbgGroup->drawLine(gUR, gLR, col, gw);
    }
    
    Renderable::prerender(-1);
}
void MapSector::uploadRender(int pass) {
    if(spriteGroup != nullptr) spriteGroup->upload();
    if(dbgGroup != nullptr) dbgGroup->upload();
    if(entityGroup != nullptr) {
        //std::cout << "uploading entity group, sz=" << entityGroup->size() << "\n";
        entityGroup->upload();
    }
    
    Renderable::uploadRender(-1);
}*/

Transform MapSector::getRelWorldTransform() {
    Transform ret = this->getRenderTransform();
    /*try {
    ret.ratio = V3(1.f) / ret.ratio;
    } catch(...){};*/
    return ret;
}
/*
extern int ARKE_DBGMODE;
void MapSector::render(int pass) {
    //if(this->hasRenderTransform()) Renderer::PushTransform(this->getRelWorldTransform());
    //std::cout << "\tMAPSECTOR render begin, depth=" << depth << "\n";;
    //const bool globalPass = pass == 0;
    
    //if((pass == Renderable::Map) && spriteGroup != nullptr && spriteGroup->size() > 0) {
     //   spriteGroup->draw();
     //   //std::cout << "\t\tDraw spritegroup\n";
    //}

    //std::cout << "\t\tInvoking children\n";
    Renderable::render(pass);
    if((pass == Renderable::Dbg) && dbgGroup != nullptr && dbgGroup->size() > 0 && (ARKE_DBGMODE > 0||true)) {
        //std::cout << "\t\tDrag debugGroup, d=" << depth << "\n";
        dbgGroup->draw();
    }
    
    //std::cout << "\t\tEND DEPTH= " << depth << "\n";
    
    
    if((globalPass || pass == 1) && entityGroup != nullptr && entityGroup->size() > 0) {
        this->constructEntityDbg();
        Renderer::PushTransform(this->getRelWorldTransform());
        entityGroup->draw();
        entityDbg->draw();
        Renderer::PopTransform();
    }
    //if(this->hasRenderTransform()) Renderer::PopTransform();
}*/

SpriteGroup* MapSector::getSpriteGroup() const {
    return spriteGroup;
}

TCoord MapSector::getChildSpan() {
    if(order == 0) return TCoord(1);
    else return TCoord( int(ipow(MAPSECTOR_BASE_DIM, order)) );
}
TCoord MapSector::getTotalSpan() {
    //if(leaf) return TCoord(width, height);
    //else;
    return TCoord(MAPSECTOR_BASE_DIM)*getChildSpan();
}
TCoord MapSector::getSpanTo(const TCoord& to) {
    return to * getChildSpan();
}

void MapSector::constructEntityDbg() {
    entityDbg->clear();
    for(auto it = this->begin<Entity>(); it != this->end<Entity>(); ++it) {
        Entity *entity = &(*it);
        entity->drawDebug(entityDbg);
    }
    entityDbg->upload();
}
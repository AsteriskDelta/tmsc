#ifndef TILEMANAGER_H
#define TILEMANAGER_H
#include "../TMSCInclude.h"
#include <ARKE/LoadableManager.h>
#include "Tile.h"
#include "TileFrame.h"

class TileManager;

class TileFrameManager : public LoadableManager<TileFrame> {
    friend class TileManager;
public:
    TileFrameManager(TileManager *parent);
    virtual ~TileFrameManager();

    TileManager* tiles;
    virtual void wasLoaded(TileFrame *frame) override;
};

class TileManager : public LoadableManager<Tile> {
public:
    TileManager();
    virtual ~TileManager();

    virtual void wasLoaded(Tile *tile) override;
    
    void loadFrames();
    void addFrame(const std::string& tileName, TileFrame *frame);
    
    void buildCrossref();

    TileFrameManager frames;
    
    static TileManager* instance();
private:
    static TileManager *insPtr;

};

#endif /* TILEMANAGER_H */


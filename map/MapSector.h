#ifndef MAPSECTOR_H
#define MAPSECTOR_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include <unordered_set>
#include <vector>
#include "CoordStack.h"
#include <ARKE/Transform.h>

#define MAPSECTOR_BASE_DIM (5)

class SpriteGroup;
class Entity;
class DebugGroup;
class EntityGroup;

class TileInstance;
class MapSector : public virtual PhysicalProxy, public virtual PhysicalObject, public virtual Updatable {
public:
    MapSector();
    MapSector(const MapSector& orig) = delete;
    virtual ~MapSector();
    
    bool leaf;
    const unsigned short width, height;
    unsigned short order;//unsigned short depth;
    //unsigned short childWidth, childHeight;
    TCoord parentPos;
    
    struct Entry {
        TileInstance* tile = nullptr;
        MapSector *sec = nullptr;
        inline bool isTile() const { return tile != nullptr; };
        inline bool isSector() const { return sec != nullptr; };
        inline operator MapSector*() { return sec; };
        inline operator TileInstance*() { return tile; };
    };
    std::vector<Entry> tiles;

    void add(Entity *entity);
    void remove(Entity *entity);
    using Proxy::add;
    using Proxy::remove;
    
    MapSector* get(CoordStack c);
    TileInstance* get(const TCoord& c);
    Entry getOffset(MapSector *child, CoordStack c);
    
    inline TCoord coords() const { return parentPos; };
    
    void set(const TCoord& c, const Entry& entry);
    inline void set(const TCoord& c, TileInstance* ptr) {
        Entry entry;
        entry.tile = ptr;
        this->set(c, entry);
    }
    inline void set(const TCoord& c, MapSector* ptr) {
        Entry entry;
        entry.sec = ptr;
        this->set(c, entry);
    }
    
    inline MapSector* operator[](CoordStack c) { return this->get(c); };
    inline TileInstance* operator[](const TCoord& c) { return this->get(c); };
    inline virtual MapSector *parentSector() const { return Adapter::parent_t<MapSector>();}//_adapt<MapSector>(Proxy::parent()); };
    virtual MapSector* siblingSector(const TCoord &off);
    
    virtual void constructEntityDbg();
    
    virtual void onSetParent() override;
    virtual void update(float deltaTime) override;
    void updateGroups();
    
    virtual void computeAgnostics() override;
    virtual const Bounds<N2>* bounds() const override;
    inline virtual PhysicsPoly* poly() const override { return nullptr; };
    inline virtual Sprite* sprite() const override { return nullptr; };
    inline virtual bool isComplete() const override { return true; };
    
    
    //virtual std::unordered_set<PhysicalObject*> getPotentialColliders(PhysicalObject* obj) override;
    //virtual void getPotentialCollidersRecurse(PhysicalObject *const& obj, std::unordered_set<PhysicalObject*> *const & ret) override;
    
    /*virtual void prerender(int pass) override;
    virtual void uploadRender(int pass) override;
    virtual void render(int pass) override;*/
    /*inline virtual void render(int pass) override {
        return render(0);
    };*/
    
    virtual SpriteGroup* getSpriteGroup() const;
    
    virtual bool hasRenderTransform();
    virtual Transform getRenderTransform();
    virtual Transform getRelWorldTransform();
    
    TCoord getChildSpan();
    TCoord getTotalSpan();
    TCoord getSpanTo(const TCoord& to);
    /*inline virtual N2 internalSpan() const override { 
        return N2(width, height);
    };*/
//private:
    SpriteGroup *spriteGroup;
    DebugGroup *dbgGroup, *entityDbg;
    EntityGroup *entityGroup;
    
    Bounds<N2> sectorBounds;
    
    virtual bool canHold(Entity *entity);
    virtual bool willHold(Entity *entity);
    
    inline bool hasIdx(const TCoord &c) const { return c.x >= 0 && c.x < width && c.y >= 0 && c.y < height; };
    inline unsigned int idx(const TCoord &c) const { 
        //std::cout << "height=" << height << ", res=" << (c.x + c.y*height) << " from " << c << " casting gives " << (c.x + c.y*height).to<unsigned int>() << "\n";
        return (c.x + c.y*height).to<unsigned int>(); };
};

#endif /* MAPSECTOR_H */


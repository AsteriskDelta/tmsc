
/* 
 * File:   CoordStack.h
 * Author: galen
 *
 * Created on April 2, 2017, 7:33 PM
 */

#ifndef COORDSTACK_H
#define COORDSTACK_H
#include "../TMSCInclude.h"
#include <list>

class CoordStack {
public:
    inline CoordStack() : coordList() {};
    inline CoordStack(const TCoord& c) : coordList({c}) {};
    inline CoordStack(const CoordStack& orig) : coordList(orig.coordList) {};
    inline virtual ~CoordStack() {}
    
    inline TCoord& coords() { return coordList.back(); };
    
    inline TCoord& push(const TCoord& c) { coordList.push_back(c); return *coordList.rbegin(); };
    inline TCoord pop() { 
        if(this->size() > 0) {
            TCoord ret = coordList.back();
            coordList.pop_back();
            return ret;
        } else return TCoord(0);
    }
    inline unsigned int size() const { return coordList.size(); };
private:
    std::list<TCoord> coordList;
};

#endif /* COORDSTACK_H */


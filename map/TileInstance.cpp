#include "TileInstance.h"
#include "MapSector.h"
#include "Tile.h"
#include "TileFrame.h"
#include "render/SpriteGroup.h"
#include "sprite/PhysicalSpriteInstance.h"

TileInstance::TileInstance() : tile(nullptr), data(), considered(false), consideredOwner(nullptr) {
}

TileInstance::~TileInstance() {
    
}
/*
void TileInstance::makeSprite(SpriteGroup *grp) {
    if(grp == nullptr) {
        Console::Error("TileInstance::makeSprite given null group!");
        return;
    }
    
    if(sprite != nullptr) this->freeSprite();
    //sprite = grp->add(frame);
    sprite = new PhysicalSpriteInstance();
    sprite->frame = frame;
    grp->attach(sprite);
    //sprite->setParent(this);
    this->updateSprite();
}

void TileInstance::freeSprite() {
    if(sprite != nullptr) delete sprite;
}
*/

MapSector* TileInstance::sector() const {
    return Adapter::parent_t<MapSector>();
}

void TileInstance::updateSprite() {
    if(isConsidered(true)) return;
    if(frame.sprite == nullptr) {
        Console::Err("TileInstance::updateSprite called, but has null sprite (",sprite," and ",frame.sprite,")");
        return;
    }/* else if(sprite == nullptr && sector != nullptr) {
        this->makeSprite(sector->getSpriteGroup());
        if(sprite == nullptr) return;
    }*/
    
    this->updateCollisionsEnabled();
    //std::cout << "updateSprite() to " << to_string(coords()) << "\n";
    
    TileFrame *tileFrame = frame.sprite->derive<TileFrame>();
    if(tileFrame == nullptr) {
        Console::Error("TileInstance::updateSprite called on non-tileframe sprite!");
        return;
    }

    TCoord frameOff = TCoord(0);
    TCoord frameStart = coords();
    //this->span() = N2(1.f);
    this->span() = tileFrame->drawSize(frame.diagonalMult != TCoord(1));
    //if(tileFrame->isMulti()) {
    //    std::cout << "\tgot multi with offsetPX=" << VP(frame.offsetPX) << "\n";
    //}

    const bool debugOrientation = false;
    if(debugOrientation) {
        if (frame.mult.x < 0 && frame.mult.y < 0) {
            tint = ColorRGBA(64, 64, 0);
        } else if (frame.mult.x < 0) {
            tint = ColorRGBA(64, 255, 0);
        } else if (frame.mult.y < 0) {
            tint = ColorRGBA(255, 64, 0);
        } else {
            tint = ColorRGBA(255, 255, 0);
        }
        if (frame.diagonalMult != TCoord(1, 1)) tint.b = 255;
    }

    this->position() = N2(frameStart.x + float(frameOff.x), frameStart.y + float(frameOff.y)) +
            N2(tileFrame->renderOffset);
    //sprite->position() = N2(0.f);
    this->zOffset = tileFrame->renderOffset.z;
    //sprite->transform.position.z += tileFrame->getZOffset();
    this->snapToPixel = true;
    
    //Get sprite info- must be done in order ot have physical information available
    SpriteInstance::update();
    
    //Ensure our neighbors have a reference to us if need be
    this->computeAgnostics();
    
    /*if(sector()->coords() == TCoord(0)) {
        std::cout << " SEC0 got tile :\n";
        auto parBounds = this->parentBounds();
        std::cout<< VBD(parBounds) << " vs sector " << VBD(*sector()->bounds()) << "\n";
        std::cout << "\ttransformed from locals = " << VBD(*this->bounds()) << "\n";
        std::cout << "manually: " << VP(this->localToParent(this->bounds()->nearPoint())) << " and " <<  VP(this->localToParent(this->bounds()->farPoint())) << "\n";
    }*/
    const auto parBounds = this->parentBounds();
    if(!sector()->bounds()->fullyContains(parBounds) /*|| sector()->coords() == TCoord(0)*/) {
        //std::cout << VP(sector()->coords()) << " : " << VP(this->coords()) << " is not fully contained\n";
        const auto agBounds = this->agnosticBounds();
        for(int ox = -1; ox <= 1; ox++) {
            for(int oy = -1; oy <= 1; oy++) {
                if(ox == 0 && oy == 0) continue;
                
                const TCoord offset = TCoord(ox, oy);
                MapSector* parSib = sector()->siblingSector(offset);
                //std::cout << "\t off="<<VP(offset)<<", parSib = " << parSib << "\n";
                if(parSib == nullptr || !parSib->isComplete()) continue;
                //std::cout << "\t\ttesting sib bounds " << VP(parSib->coords()) << "\n";
                //If we intrude upon the other sector's bounds, add us to their collision list (will automatically be removed if this is deleted)
                parSib->computeAgnostics();
                const auto sibBounds = parSib->agnosticBounds();
                //std::cout << "\t\tintersect this=" << VBD(*agBounds) << ", sib=" << VBD(*sibBounds) << "\n";
                
                if(sibBounds->intersects(*agBounds)) {
                    parSib->addFriend(this);
                    //std::cout << "\t\tADDED COLLISION FRIEND " << VP(parSib->coords()) << "\n";
                } else parSib->removeFriend(this);//If we're not already friends, this won't do anything
            }
        }
    }
    
    /*N2 ratio = sprite->size / tileFrame->drawSize(frame.diagonalMult != TCoord(1));
    N2 rOverflow = ratio - N2(1.f);//mod(ratio, N2(PX_PER_UNIT));
    if(tileFrame->drawSizePX().x % PX_PER_UNIT != 0 || tileFrame->drawSizePX().y % PX_PER_UNIT != 0) {
        std::cout << "Tile has unusual size: " << VP(tileFrame->drawSizePX()) << ", ratio=" << VP(ratio) << "\n";;
    }
    
    if(fabs(rOverflow.x) > 0.02f || fabs(rOverflow.y) > 0.02f) {
        std::cout << "Bad tile ratio, "<<VP(sprite->size) << " and drawSize=" << VP(tileFrame->drawSize()) << "\n";
    }*/
    
    //sprite->update();
    //this->computeAgnostics();
}

void TileInstance::setConsidered(bool state, TileInstance *owner) {
    considered = state;
    consideredOwner = owner;
}

TileInstance* TileInstance::getSibling(const TCoord& off) {
    if(sector() == nullptr) return nullptr;
    
    return sector()->get(this->coords() + off);
}
void TileInstance::update() {
    if(isConsidered(true)) return;
    this->updateSprite();
    SpriteInstance::update();
}
void TileInstance::characterize(int stage) {
    if(tile == nullptr || isConsidered(true)) {
        return;
    }
    //std::cout << "\t\t\tTileInstance::characterize()\n";
    
    TileFrameInstance newFrame = tile->calculateFrame(this, stage);
    if(newFrame.sprite == nullptr) return;//We were skipped for this pass
    
    frame = newFrame;
    
    TileFrame *tFrame = nullptr;
    if(frame.sprite != nullptr) tFrame = _derive<TileFrame>(frame.sprite);
    
    if(tFrame != nullptr) {
        //std::cout << "CHARFRAME: " << tFrame->name << ", isMulti()=" << tFrame->isMulti() << "\n";
        if(tFrame->isMulti()) tFrame->setConsidered(this, true, &frame);
    }
}
bool TileInstance::needsUpdate() const {
    return false;
}

bool TileInstance::hasFrame() const {
    return frame.sprite != nullptr;
}

void TileInstance::updateCollisionsEnabled() {
    if((tileFrame() != nullptr && !tileFrame()->isPhysical()) ||
            this->isConsidered(true)) {
        data.collisionCache = false;
        return;
    }
    for(int ox = -1; ox <= 1; ox++) {
        for(int oy = -1; oy <= 1; oy++) {
            if(ox == 0 && oy == 0) continue;
            //Since we don't check normals, skip diagonals
            //if(abs(ox) + abs(oy) > 1) continue;
            
            TCoord off = TCoord(ox, oy);
            
            TileInstance *sib = this->getSibling(off);
            if(sib == nullptr || !sib->coversTile(-off, this)) {
                data.collisionCache = true;
                return;
            }
        }
    }
    
    data.collisionCache = false;
}

bool TileInstance::coversTile(TCoord off, TileInstance *const o, bool fully) {
    _unused(off);
    if(o == this) return true;
    if(this->considered && consideredOwner != this) {
        if( consideredOwner != nullptr) return consideredOwner->coversTile(off, this, fully);//return false;
        //else return true;
    }
    
    TileFrame *const tFrame = this->tileFrame();
    if(tFrame == nullptr) return false;
    
    if(!tFrame->isFullyOpaque() && fully) return false;
    else if(!tFrame->isFilling()) return false;
    //TODO: Normal checking code?
    
    return true;
}

bool TileInstance::isCollisionEnabled() const {
    return data.collisionCache;
}

TileFrame* TileInstance::tileFrame() const {
    if (frame.sprite == nullptr) return nullptr;
    else return _derive<TileFrame>(frame.sprite);
}
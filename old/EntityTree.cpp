#include "EntityTree.h"
#include "render/SpriteGroup.h"
#include "render/DebugGroup.h"

EntityTree::EntityTree() : spriteGroup(nullptr), dbgGroup(nullptr) {
}

EntityTree::~EntityTree() {
    
}

void EntityTree::add(Entity* entity) {
    return this->localAdd(entity);
}
void EntityTree::remove(Entity* entity) {
    return this->localRemove(entity);
}

void EntityTree::localAdd(Entity *entity) {
    Proxy::add(entity);
}
void EntityTree::localRemove(Entity *entity) {
    Proxy::remove(entity);
}

bool EntityTree::hasRenderTransform() {
    return true;//this->parentSector() != nullptr;
}

Transform EntityTree::getRenderTransform() {
    return Transform();
}

void EntityTree::prerender(int pass) {
    //std::cout << "\tMapSector::prerender(), size= " << this->size() << ", leaf=" << leaf << "\n";
    const bool doDbg = true;
    if(spriteGroup ==  nullptr) spriteGroup = new SpriteGroup(512);
    
    if(doDbg && dbgGroup ==  nullptr) dbgGroup = new DebugGroup(512*8);
    else if(doDbg) dbgGroup->clear();

    Renderable::prerender(-1);
}
void EntityTree::uploadRender(int pass) {
    if(spriteGroup != nullptr) {
        spriteGroup->upload();
        if(dbgGroup != nullptr) dbgGroup->upload();
    }
    
    Renderable::uploadRender(-1);
}

extern int ARKE_DBGMODE;
void EntityTree::render() {
    if(this->hasRenderTransform()) Renderer::PushTransform(this->getRenderTransform());
    if(spriteGroup != nullptr && spriteGroup->size() > 0) {
        spriteGroup->draw();
    }
    if(dbgGroup != nullptr && dbgGroup->size() > 0 && ARKE_DBGMODE > 0) dbgGroup->draw();
    
    Renderable::render();
    
    if(this->hasRenderTransform()) Renderer::PopTransform();
}
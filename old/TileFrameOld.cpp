#include "TileFrameOld.h"
#include <NVX/NPolygon.h>
#include <ARKE/Image.h>
#include <string>

#define PX_FLAG(x) (0x1 << x)
#define PX_OFFX(i) (-(((int)i+1)%2))
#define PX_OFFY(i) (-(((int)(i/2 + 1)%2)))
#define LL 0
#define LR 1
#define UL 2
#define UR 3

TileFrame::TileFrame() : Loadable(), poly(nullptr), origPoly(nullptr),bounds(), img(nullptr), atlasID(0) {

}

TileFrame::TileFrame(const std::string& p) : TileFrame() {
    this->load(p);
}

TileFrame::~TileFrame() {
    if (img != nullptr) delete img;
    if (poly != nullptr) delete poly;
}

bool TileFrame::couldLoad(const std::string& p) const {
    return p.size() >= 4 && p.find(".png") != std::string::npos;
}

bool TileFrame::load(const std::string& p = "") {
    if (p.size() > 0) path = p;
    if (!Loadable::load(path)) return false;

    std::cout << "TILEFRAME::LOAD " << name << " from " << path << "\n";
    img = new ImageRGBA(p, false);
    img->repColor(ColorRGBA(0, 0, 0, 255), ColorRGBA(0, 0, 0, 0));
   //if (this->path == "/mnt/Working/Projects/TMSC/build/Desktop/../../data/tiles/corridor/grate_grad_d_rc_v1.png") {
        //std::cout << "SAVING\n";
        //img->Save("testOut.png");
        this->calculatePolygon(true);
  //}


    return true;
}


void TileFrame::unload() {
    if (img != nullptr) delete img;
}

const static bool pixDbg = false;

static void setPixProcFlags(ImageRGBA * const& img, int x, int y, bool state) {
    //unsigned short rx = x, ry = y;
    for (unsigned char i = 0; i < 4; i++) {
        if (!img->hasPixel(x + PX_OFFX(i), y + PX_OFFY(i))) continue;
        if (pixDbg) std::cout << "\tSETPF " << (x + PX_OFFX(i)) << ", " << (y + PX_OFFY(i)) << " : " << int(i) << "\n";
        if (state) (img->getPixelRef(x + PX_OFFX(i), y + PX_OFFY(i))).b |= PX_FLAG(i); //true
        else (img->getPixelRef(x + PX_OFFX(i), y + PX_OFFY(i))).b &= ~PX_FLAG(i); //false
    }
}

static bool getPixProcFlags(ColorRGBA * const &cols, bool withAlpha = false) {
    bool valid = true; bool hasAlpha = false, hasNonalpha = false;
    bool interest = !withAlpha;
    if (pixDbg) std::cout << "\t\t\t\tProcFlags{";
    for (unsigned char i = 0; i < 4; i++) {
        valid &= ((cols[i].b & PX_FLAG(i)) == 0 || cols[i].b == 0xFF);
        hasAlpha |= (cols[i].alpha() > 0);
        hasNonalpha |= (cols[i].alpha() == 0);
        if (pixDbg) std::cout << ((cols[i].b & PX_FLAG(i)) != 0) << ":" << int(cols[i].alpha()) << ", ";
    }
    interest = hasAlpha && hasNonalpha;
    valid &= interest;
    if (pixDbg) std::cout << "}=" << valid << "\n";
    return valid;
}

static bool getPixInterestFlags(ColorRGBA * const &cols, bool strict) {
    bool ofInterest = false;
    bool hasAlpha = false, hasNonalpha = false;
    for (unsigned char i = 0; i < 4; i++) {
        //ofInterest |= (((cols[i].b & PX_FLAG(i)) == 0 || (!strict && cols[i].b == 0xFF)) && cols[i].alpha() > 0);
        hasAlpha |= (((cols[i].b & PX_FLAG(i)) == 0 || (!strict && cols[i].b == 0xFF)) && cols[i].alpha() > 0);
        hasNonalpha |= (((!strict && cols[i].b == 0xFF)) || cols[i].alpha() == 0);
    }
    ofInterest = hasAlpha && hasNonalpha;
    return ofInterest;
}

static void getPixSample(ImageRGBA * const& img, int x, int y, ColorRGBA * const &cols) {
    for (unsigned char i = 0; i < 4; i++) {
        cols[i] = img->getPixelSafe(x + PX_OFFX(i), y + PX_OFFY(i), ColorRGBA(0));
        if (pixDbg) std::cout << "\tGETPIXS " << (x + PX_OFFX(i)) << ", " << (y + PX_OFFY(i)) << " = " << cols[i].toString() << "\n";
    }
}

float getEffAlpha(ColorRGBA * const &cols, unsigned short i) {
    if (cols[i].b == 0xFF) return 0.f;
    else return cols[i].getAlpha();
}

static N2 getPixNorm(ColorRGBA * const &cols) {
    const float uPart = (getEffAlpha(cols, UL) + getEffAlpha(cols, UR)) / 2.f,
            rPart = (getEffAlpha(cols, UR) + getEffAlpha(cols, LR)) / 2.f,
            dPart = (getEffAlpha(cols, LR) + getEffAlpha(cols, LL)) / 2.f,
            lPart = (getEffAlpha(cols, UL) + getEffAlpha(cols, LL)) / 2.f;
    const N2 rawNorm = (
            N2_UP * uPart +
            N2_RIGHT * rPart +
            N2_DOWN * dPart +
            N2_LEFT * lPart
            );
    if (pixDbg) std::cout << "\t\t\t\tPN{U=" << uPart << ", R=" << rPart << ", D=" << dPart << ", L=" << lPart << "}\n";
    if (rawNorm == N2(0.f)) return rawNorm;
    else return -normalize(rawNorm);
}

static unsigned int tracePixPoly(ImageRGBA * const& img, TileFrame::Poly * const &poly, int x, int y, int lastX = -1, int lastY = -1, int dx = 1, int dy = 1) {
    const bool dbgPrint = false;

    //ColorRGBA& col = img->getPixelRef(x,y);
    //struct { bool up = true, down = true, left = true, right = true; } consider;
    //col.b = 1;
    ColorRGBA cols[4];
    getPixSample(img, x, y, cols);

    const N2 pos = N2(x, y);
    const N2 norm = getPixNorm(cols);
    poly->add(PhysicsPt(pos, norm));
    setPixProcFlags(img, x, y, true);
    if (dbgPrint) std::cout << "\t\tadded " << to_string(pos) << " with norm " << to_string(norm) << "\n";

    /*if(col.r > 127) consider.right = false;
    if(col.r < 127) consider.left = false;
    if(col.g < 127) consider.down = false;
    if(col.g > 127) consider.up = false;*/

    //int dx = x - lastX, dy = y - lastY;
    //if(lastX == -1 || lastY == -1) dx = dy = 1;
    if (lastX != x) dx = x - lastX;
    if (lastY != y) dy = y - lastY;

    float bestDist = 999.f, altDist = 999.f;
    int bestX = -1, bestY = -1, altX = -1, altY = -1;
    //if(dbgPrint) std::cout << "\t\tat " << x << ", " << y << ": R"<<consider.right << " L"<<consider.left<< " U"<<consider.up << " D"<<consider.down << "\n";

    const int searchRadius = 1;
    for (int ox = -searchRadius; ox <= searchRadius; ox++) {
        for (int oy = -searchRadius; oy <= searchRadius; oy++) {
            if ((ox == 0 && oy == 0) || (abs(ox) + abs(oy) > searchRadius)) continue;
            const int cx = x + ox, cy = y + oy;
            if ((cx < 0 || cx > img->width) || (cy < 0 || cy > img->height) ||
                    (cx == lastX && cy == lastY)) continue;

            //Multiply by dx or dy!!!!
            /*int prefX = x + (int(col.r) - 127) / 64 * -sgn(dx), prefY = y + (int(col.g) - 127) / 64 * -sgn(dy);
            
            const ColorRGBA& cur = img->getPixel(cx,cy);
            if(cur.b != 0 || cur.alpha() == 0) continue;
            int dnx = (int(cur.r) - 127) / 64 - (int(col.r) - 127) / 64, dny = (int(cur.g) - 127) / 64 - (int(col.g) - 127) / 64;
            delX -= sgn(dx); delY -= sgn(dy);*/
            //int delX = ox, delY = oy;

            ColorRGBA curs[4];
            getPixSample(img, cx, cy, curs);
            bool valid = getPixProcFlags(curs, true);
            if (dbgPrint) std::cout << "\t\t\t" << cx << ", " << cy << " valid: " << valid << "\n";
            if (!valid) continue;

            const N2 cNorm = getPixNorm(curs);
            if (cNorm == N2(0.f)) continue;

            //Position culling
            /*if((ox < 0 && oy == 0 && !consider.left) || 
               (ox > 0 && oy == 0 && !consider.right) ||
               (oy < 0 && ox == 0 && !consider.down) ||
               (oy > 0 && ox == 0 && !consider.up) ||
               (ox < 0 && oy < 0 && !consider.left && !consider.down) || 
               (ox < 0 && oy > 0 && !consider.left && !consider.up) || 
               (ox > 0 && oy < 0 && !consider.right && !consider.down) || 
               (ox > 0 && oy > 0 && !consider.right && !consider.up)
                    ) continue;*/


            //int dist = (abs(delX*delX) + abs(delY*delY))*4 + 4*(dnx*dnx+ dny*dny);
            float dist = sqrt(float(ox * ox) + float(oy * oy)) * pow((1.5f - dot(norm, cNorm) / 2.f), 2.f);

            /*if(prefX != x && cx == prefX) dist /= 2;
            else if(prefX != x && abs(ox) != 0) dist *= 2;
            
            if(prefY != y && cy == prefY) dist /= 2;
            else if(prefY != y && abs(oy) != 0) dist *= 2;*/

            //Normal culling
            //if(false &&prefX == cx && prefY == cy) dist /= 3;
            //else if(false&&(((col.r > 127 && cur.r < 127) || (col.r < 127 && cur.r > 127))/* && (col.g == 127 || cur.g == 127)*/) ||
            //   (((col.g > 127 && cur.g < 127) || (col.g < 127 && cur.g > 127))/* && (col.r == 127 || cur.r == 127)*/)
            //        ) dist *= 4;
            //else 
            //if((col.r == cur.r && col.g == cur.g)) dist /= 4;

            if (dbgPrint) std::cout << "\t\t\tcomp " << cx << ", " << cy << ": dist=" << dist << " from n1=" << to_string(norm) << ", n2=" << to_string(cNorm) << ", rdist=" << (sqrt(float(ox * ox) + float(oy * oy))) << "\n";

            if (dist <= bestDist) {
                if (dbgPrint) std::cout << "\t\t\tnew best dist " << dist << "\n";
                altX = bestX;
                altY = bestY;
                altDist = bestDist;

                bestDist = dist;
                bestX = cx;
                bestY = cy;
            } else if (dist <= altDist) {
                altDist = dist;
                altX = cx;
                altY = cy;
            }
        }
    }

    if (dbgPrint) std::cout << "\t\t\tbest x,y = " << bestX << ", " << bestY << " @ " << bestDist << "\n";
    if (bestX != -1 && bestY != -1) {
        unsigned int depth = tracePixPoly(img, poly, bestX, bestY, x, y, dx, dy);
        bool satisficted = depth > 5 || poly->closeDistance() < 3.f;

        if (!satisficted && altX != -1 && altY != -1) {
            if (dbgPrint) std::cout << "\t\tREVERT to " << x << ", " << y << " d=" << depth << ", use alts " << altX << ", " << altY << "\n";
            for (unsigned int depthSub = depth; depthSub > 0; depthSub--) {
                const unsigned int edgeIdx = poly->size() - depthSub;
                unsigned int rmx = round(poly->get(edgeIdx)->positionConst().x), rmy = round(poly->get(edgeIdx)->positionConst().y);
                //ColorRGBA &rmcol = img->getPixelRef(rmx, rmy);
                if (dbgPrint) std::cout << "\t\t\tREVNULL " << rmx << ", " << rmy << "\n";
                setPixProcFlags(img, rmx, rmy, false);
                //rmcol.b = 0;
            }
            poly->removeLast(depth);
            depth = tracePixPoly(img, poly, altX, altY, x, y, dx, dy);
            return depth + 1;
        }/* else if(!satisficted) {
            
        } */
        return depth + 1;
    }
    return 1;
}

void TileFrame::calculatePolygon(bool dbg) {
    if (poly != nullptr) delete poly;

    ImageRGBA *proc = new ImageRGBA(img->width, img->height);
    proc->zero();
    PolygonSetDbg(nullptr, 1, 0);
    bounds.zero();

    //Find edges, get normals
    for (int x = 0; x < img->width; x++) {
        for (int y = 0; y < img->height; y++) {
            if (img->getAlpha(x, y) == 0) continue;

            bool isEdge = false;
            int dx = 0, dy = 0, adj = 0;
            if (x == 0 || img->getAlpha(x - 1, y) == 0) {
                dx--;
                adj++;
            }
            if (x == img->width - 1 || img->getAlpha(x + 1, y) == 0) {
                dx++;
                adj++;
            }
            if (y == 0 || img->getAlpha(x, y - 1) == 0) {
                dy--;
                adj++;
            }
            if (y == img->height - 1 || img->getAlpha(x, y + 1) == 0) {
                dy++;
                adj++;
            }

            _unused(adj);
            isEdge = (dx != 0 || dy != 0 || (adj % 2 == 0 && adj > 0)); // && (adj < 2 || (dx != 0 && dy != 0));//Exclude 1-pixel groups

            if (isEdge) {
                proc->setPixel(x, y, ColorRGBA(127 + 64 * dx, 127 + 64 * dy, 0, 255));
            } else {
                proc->setPixel(x, y, ColorRGBA(0, 0, 0, 255));
            }
        }
    }

    //Cellular automata to cull loose edges (tehe-pero)
    bool wasUpdated = true;
    while (wasUpdated) {
        unsigned int updateCount = 0;
        for (int x = 0; x < proc->width; x++) {
            for (int y = 0; y < proc->height; y++) {
                ColorRGBA& col = proc->getPixelRef(x, y);
                if (col.b == 0xFF) continue;

                int minNeighbors = 2, minAdjacent = 2;
                bool flipAlpha = false;
                int neighbors = 0, adjNeighbors = 0, nonNormNeighbors = 0, freeNeighbors = 0, adjFreeNeighbors = 0;
                if (col.b == 0xFB) {
                    minNeighbors += 1;
                    //minAdjacent++;
                    col.b = 0;
                } else if (col.b == 0xFC) {
                    col.b = 0xFA;
                    updateCount++;
                    continue;
                } else if (col.b == 0xFA) {
                    flipAlpha = true;
                    col.b = 0xFF;
                }

                for (int ox = -1; ox <= 1; ox++) {
                    for (int oy = -1; oy <= 1; oy++) {
                        if (ox == 0 && oy == 0) continue;
                        const int cx = x + ox, cy = y + oy;
                        if ((cx < 0 || cx >= proc->width) || (cy < 0 || cy >= proc->height)) {
                            freeNeighbors++;
                            if (abs(ox) + abs(oy) == 1) adjFreeNeighbors++;
                            continue;
                        }

                        const ColorRGBA& cur = proc->getPixel(cx, cy);
                        if (cur.alpha() == 0 || cur.b == 0xFF) continue;

                        neighbors++;
                        if (abs(ox) + abs(oy) == 1) adjNeighbors++;
                        if ((col.r < 127 && ox >= 0) ||
                                (col.r > 127 && ox <= 0) ||
                                (col.g < 127 && oy >= 0) ||
                                (col.g > 127 && oy <= 0) ||
                                (col.r == cur.r && col.g == cur.g)
                                ) nonNormNeighbors++;
                    }
                }

                minNeighbors -= freeNeighbors;
                //minAdjacent -= freeNeighbors;

                //if(x == 0 || x == proc->width-1) std::cout << "\tQ " << x << ", " << y << " neighbors=" << neighbors << " <? " << (minNeighbors) << ", adj=" << adjNeighbors << " <? " << (minAdjacent) << ", nNN=" << nonNormNeighbors << "\n";
                if (flipAlpha && col.alpha() == 0 && adjNeighbors == 4 - adjFreeNeighbors) {
                    col = ColorRGBA(0, 0, 0, 255);
                    updateCount++;
                } else if (col.alpha() == 0) {
                    if (adjNeighbors == 4 - adjFreeNeighbors) {
                        for (int ox = -1; ox <= 1; ox++) {
                            for (int oy = -1; oy <= 1; oy++) {
                                const int cx = x + ox, cy = y + oy;
                                if ((cx < 0 || cx >= proc->width) || (cy < 0 || cy >= proc->height)) continue;

                                ColorRGBA& cur = proc->getPixelRef(cx, cy);
                                if (abs(ox) + abs(oy) == 1 && cur.alpha() > 0) {
                                    cur.b |= 0xFB;
                                    updateCount++;
                                }
                            }
                        }
                        col.b = 0xFC;
                    } else col.b = 0xFF;
                    updateCount++;
                } else if (neighbors < minNeighbors || (adjNeighbors < minAdjacent/* && nonNormNeighbors == 0*/)) {
                    ColorRGBA& cur = proc->getPixelRef(x, y);
                    cur.b = 0xFF;
                    //cur.v[3] = 0;
                    updateCount++;
                    //std::cout << "\tCULL " << x << ", " << y << " neighbors=" << neighbors << ", adj=" << adjNeighbors << ", nNN=" << nonNormNeighbors << "\n";
                }
            }
        }

        wasUpdated = updateCount > 0;
    }

    for (int x = 0; x < proc->width; x++) {
        for (int y = 0; y < proc->height; y++) {
            ColorRGBA& col = proc->getPixelRef(x, y);
            if (col.b == 0xFB || col.b == 0xFC) col.b = 0x0;
        }
    }

    //Trace path, convert to polygon, WORKS ON POINTS BETWEEN PIXELS
    for (int x = 0; x <= img->width; x++) {
        for (int y = 0; y <= img->height; y++) {
            ColorRGBA cols[4];
            getPixSample(proc, x, y, cols);
            bool ofInterest = getPixInterestFlags(cols, true);

            //ColorRGBA& col = proc->getPixelRef(x, y);
            if (ofInterest && (true || poly == nullptr || !poly->contains(N2(x, y)))) {
                //col.b = 1;
                setPixProcFlags(proc, x, y, true);
                //std::cout << "START POLY" << x << ", " << y << "\n";

                Poly *newPoly = new Poly();
                tracePixPoly(proc, newPoly, x, y);
                float prevArea;
                bool newPolyContained = poly != nullptr && poly->contains(N2(x,y));
                if (newPolyContained || newPoly->size() < 3 || newPoly->closeDistance() > 1.2f || (prevArea = newPoly->area()) < (img->width + img->height) / 4.f) {
                    delete newPoly;
                    continue;
                }

                unsigned int prevSize = newPoly->size(), curSize;
                newPoly->fixNormals();
                float eps = 0.37f;
                float relEps = 0.5f;
                if (origPoly == nullptr) origPoly = new Poly(*newPoly);
                else origPoly->chain(new Poly(*newPoly));

                while (prevSize - (newPoly->simplify(eps, relEps), curSize = newPoly->size()) > (2 + prevSize / 10)) {
                    prevSize = curSize;
                }
                newPoly->fixNormals();

                //std::cout << "\tpoly of size " << newPoly->size() << ", area=" << newPoly->area() << "\n";
                /*for (unsigned int i = 0; i < newPoly->size(); i++) {
                    //std::cout << "\t\t" << to_string((*newPoly)[i]) << "\n";
                    
                    //Offset via normal
                    ColorRGBA polyCol = proc->getPixel((*newPoly)[i].x, (*newPoly)[i].y);
                    if(polyCol.r < 127) (*newPoly)[i].x = floor((*newPoly)[i].x);
                    else if(polyCol.r > 127) (*newPoly)[i].x = ceil((*newPoly)[i].x);
                    if(polyCol.g < 127) (*newPoly)[i].y = floor((*newPoly)[i].y);
                    else if(polyCol.g > 127) (*newPoly)[i].y = ceil((*newPoly)[i].y);
                }*/

                bounds.add(newPoly->bounds);
                if (poly == nullptr) poly = newPoly;
                else poly->chain(newPoly);

            }
        }
    }
    

    std::string procName = "dbg/" + this->getBaseName(path) + "_proc.png";
    proc->Save(procName);

    if(dbg) this->renderDebug(proc);

    delete proc;
}

std::string TileFrame::tileName() const {
    return this->getBaseName(this->subPath, "_", "/.");
}

TileFrame* TileFrame::Factory(const std::string p) {
    TileFrame* ret = new TileFrame();
    if (p.size() == 0) return ret;

    if (ret->load(p)) return ret;

    delete ret;
    return nullptr;
}

void TileFrame::renderDebug(ImageRGBA *proc) {
    const int pFac = 8;
    polyDbgFac = pFac;
    pDO = pFac*2;
    ImageRGBA *polyDbg = new ImageRGBA(proc->width*pFac + 2*pDO, proc->height * pFac + 2*pDO);
    PolygonSetDbg(reinterpret_cast<void*>(polyDbg), pFac, pDO);
    polyDbg->zero();
    for (int x = pDO; x < polyDbg->width-pDO; x++) {
        for (int y = pDO; y < polyDbg->height-pDO; y++) {
            ColorRGBA pix = img->getPixel((x-pDO) / pFac, (y-pDO) / pFac); // + (proc->getPixel(x/pFac, y/pFac).lerped(ColorRGBA(), 0.6f));
            polyDbg->setPixel(x, y, pix);
            //polyDbg->setPixel(x,y,ColorRGBA(255,255,255,255));
        }
    }
    
    
    //if(false) {
    for (int i = 0; i <= (proc->width*proc->height); i++) {
        //int sx = i, sy = proc->height*3/4;//
        int sx =rand()%(proc->width+2*pDO) - pDO, sy = rand() % (proc->height+2*pDO) - pDO;
        //if(i == 0) sx = sy = 0;
        ColorRGBA col;
        polyDbgImg = polyDbg;
        if (poly->contains(N2(sx, sy))) col = ColorRGBA(0, 255, 0, 90);
        else col = ColorRGBA(255, 0, 0, 90);
        polyDbg->drawPoint(pDO+sx*pFac, pDO+sy*pFac, col, 2);
    }
    //}
    
    /*for (int i = 0; i <= (proc->width/2); i++) {
        int sx = rand()%(proc->width+1), sy = rand() % (proc->height+1);
        int ex = rand()%(proc->width+1), ey = rand() % (proc->height+1);
        if(sx == ex && sy == ey) continue;
        
        ColorRGBA col;
        polyDbgImg = polyDbg;
        N2 inter;
        if (poly->intersect(N2(sx, sy), N2(ex, ey), inter, false)) col = ColorRGBA(0, 255, 0, 255);
        else col = ColorRGBA(255, 0, 0, 255);
        
        polyDbg->drawPoint(pDO+sx*pFac, pDO+sy*pFac, col, 2);
        polyDbg->drawLine(pDO+sx* pFac, pDO+sy* pFac, pDO+ex* pFac, pDO+ey* pFac, col);
        if(inter != V2_NULL) polyDbg->drawPoint(pDO+inter.x*pFac, pDO+inter.y*pFac, ColorRGBA(255, 0, 255, 255), 2);
    }*/

    Poly *polyPtr = origPoly;
    /*while (polyPtr != nullptr) {
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            N2 ls = polyPtr->get(i)->pt, le = polyPtr->get((i + 1) % polyPtr->size())->pt;
            polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+le.x* pFac, pDO+le.y* pFac, ColorRGBA(0, 255, 255, 255));
            
            N2 ln = polyPtr->get(i)->normalConst()*float(pFac);
            //std::cout << "drawnorm " << (ls.x* pFac) << ", " <<( ls.y* pFac)<<", "<<(ls.x* pFac + ln.x) << ", "<<( ls.y* pFac + ln.y) << "\n";
            polyDbg->drawLine(ls.x* pFac, ls.y* pFac, ls.x* pFac + ln.x, ls.y* pFac + ln.y, ColorRGBA(0, 127, 255, 127));
        }
        polyPtr = polyPtr->next;
    }*/
    polyPtr = poly;
    while (polyPtr != nullptr) {
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            N2 ls = polyPtr->get(i)->pt, le = polyPtr->get((i + 1) % polyPtr->size())->pt;
            polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+le.x* pFac, pDO+le.y* pFac, ColorRGBA(255, 255, 0, 255));
            
            N2 ln = polyPtr->get(i)->normalConst()*float(pFac);
            polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x,pDO+ ls.y* pFac + ln.y, ColorRGBA(255, 127, 0, 127));
        }
        N2 ls = polyPtr->bounds.center, ln = polyPtr->bounds.axes[0];
        ColorRGBA bCol = ColorRGBA(255, 255, 255, 255);
        polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x*pFac,pDO+ ls.y* pFac            , bCol);
        polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac            ,pDO+ ls.y* pFac + ln.y*pFac, bCol);
        polyDbg->drawLine(pDO+ls.x* pFac + ln.x*pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x*pFac,pDO+ ls.y* pFac + ln.y*pFac, bCol);
        polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac + ln.y*pFac, pDO+ls.x* pFac + ln.x*pFac,pDO+ ls.y* pFac + ln.y*pFac, bCol);
        
        polyPtr = polyPtr->next;
    }




    std::string polyName = "dbg/" + this->getBaseName(path) + "_poly.png";
    polyDbg->Save(polyName);
    delete polyDbg;
}
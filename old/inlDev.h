#ifndef INLDEV_H
#define INLDEV_H
#ifndef BLOCKDBG
#define BLOCKDBG if(false)
//#define BLOCKDBG if(true)
#endif

//B is the vector we have, A (second arg) is the vector to intersect it with.
template<typename T, typename PT_T>
inline bool NPolygon<T, PT_T>::intersectRays(const T& bStart, const T& bNorm, const T& aStart, const T& aNorm, T& result, float* t) const {
        T baseToP = bStart - aStart;
        T baseToPNorm = normalize(baseToP);
        
        //Select most aligned axis to intersect with
        float thetaBase = acos(dot(aNorm, baseToPNorm));
        float thetaP = acos(dot(bNorm, -baseToPNorm));
        float thetaInter = M_PI - thetaBase - thetaP;
        
        /*std::cout << "\t\tbToP="<<VP(baseToP)<<", btPNorm=" << VP(baseToPNorm) << 
                " thetaBase=" << thetaBase << ", thetaP=" << thetaP << ", thetaInter = " << thetaInter << "\n\t\t"<<
                "sTI=" << (sin(thetaInter)) << ". sTB=" << sin(thetaBase) << "\n";*/
        
        if(sin(thetaInter) == 0.f) {
            result = aStart;
            //std::cout <<"ZERO SIGN\n";
            return true;
        }
        
        float interDist =  sin(thetaBase) * length(baseToP) / sin(thetaInter);
        if(t != nullptr) (*t) = float(interDist);
        T interVec = bStart + bNorm * float(interDist);
        result = interVec;
        return true;
}

template<typename T, typename PT_T>
inline typename NPolygon<T, PT_T>::Projection NPolygon<T, PT_T>::projection(const NPolygon<T, PT_T>::Viewpoint &view) {
    const T& dStart = view.start;
    const T& testAxisBase = view.end;
    const T axisMidNorm = normalize(testAxisBase - dStart);
    const T axisNormCross = T(axisMidNorm.y*-1.f, axisMidNorm.x);
    const bool& ortho = view.ortho;
    
    T axisA, axisB;
    if(view.axes[0] == T(0.f)) {//Construct perpandicular to end-start
        axisA = axisNormCross;
    } else axisA = view.axes[0];
    if(view.axes[1] == axisA || view.axes[1] == T(0.f)) {
        axisB = -axisA;
    } else axisB = view.axes[1];
    
    const T axisANorm = normalize(axisA), axisBNorm = normalize(axisB);
    
    float axisADistSq = 0.f, axisBDistSq = 0.f;
    float minDistA = 10000.f, minDistB = 10000.f;
    float bestDotA = 2.f, bestDotB = 2.f;
    T axisABest = T(0.f), axisBBest = T(0.f);
    Point *edgeA = nullptr, *edgeB = nullptr;
    
    for (unsigned int i = 0; i < this->size(); i++) {
        Point *pt = this->get(i);
        T ls = this->get(i)->pt;//, le = this->get((i + 1) % this->size())->pt;

        //std::cout << "projecting " << VP(ls) << "\n";
        T lineStart, dLine;
        const T *selAxis, *otherAxis;
        ColorRGBA col;
        if (ortho) {
            lineStart = ls - axisMidNorm; // + dStart;
            if (sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(lineStart - dStart, axisNormCross))) {
                selAxis = &axisANorm; otherAxis = &axisBNorm;
                dLine = -axisBNorm; //T(-selAxis->y, selAxis->x);//axisMidNorm;
                col = ColorRGBA(255, 0, 0);
            } else {
                selAxis = &axisBNorm; otherAxis = &axisANorm;
                dLine = -axisANorm; //T(-selAxis->y, selAxis->x);//axisMidNorm;
                col = ColorRGBA(0, 0, 255);
            }
        } else {
            lineStart = dStart;
            dLine = normalize(ls - lineStart);
            if (sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(dLine, axisNormCross))) {
                selAxis = &axisANorm; otherAxis = &axisBNorm;
                col = ColorRGBA(255, 0, 0);
            } else {
                selAxis = &axisBNorm; otherAxis = &axisANorm;
                col = ColorRGBA(0, 0, 255);
            }
        }
        
        //std::cout << "\tusing start="<<VP(lineStart)<<", dLine=" << VP(dLine) << "\n\t axis=" << VP(*selAxis) << ", base=" << VP(testAxisBase) << "\n";

        T edgePt;
        float edgeTime;
        if(ortho) {
            edgeTime = dot(*selAxis, ls - testAxisBase);
            edgePt = edgeTime * (*selAxis) + testAxisBase;
            float axisDist = distance2(edgePt, ls);
            
            //Handle bounded cases
            //bool axisPass = false;
            if(view.bounded) {
                float edgeTime2 = dot(*otherAxis, ls - dStart);
                T edgePt2 = edgeTime2 * (*otherAxis) + dStart;
                float axisDist2 = distance2(edgePt2, ls);
                if(axisDist2 < 0.1f) axisDist = -1.f;//Close enough to bounding axis, we can be constrained here
            }
            
            if (selAxis == &axisANorm) {
                if ((view.admissable && edgeTime > axisADistSq) || (!view.admissable && axisDist+0.1f < minDistA)) {
                    edgeA = pt;
                    axisADistSq = edgeTime;
                    axisABest = edgePt;
                    minDistA = axisDist;
                }
            } else {
                if ((view.admissable && edgeTime > axisBDistSq) || (!view.admissable && axisDist+0.1f < minDistB)) {
                    edgeB = pt;
                    axisBDistSq = edgeTime;
                    axisBBest = edgePt;
                    minDistB = axisDist;
                }
            }
        } else {
        bool foundInter = this->intersectRays(lineStart, dLine, testAxisBase, *selAxis, edgePt, &edgeTime);

        if (foundInter) {
            const float d2 = dot(*selAxis, edgePt);//dot(*selAxis, (edgePt - testAxisBase)); //distance2(lineStart, ls);
            //std::cout << "\tgot inter pt=" << VP(edgePt) << ", t=" << edgeTime << ", d2=" << d2 << "\n";

            //polyDbg->drawLine(pDO + lineStart.x* pFac, pDO + lineStart.y* pFac, pDO + edgePt.x* pFac, pDO + edgePt.y* pFac, col / 2);
            //polyDbg->drawLine(pDO + lineStart.x* pFac, pDO + lineStart.y* pFac, pDO + ls.x* pFac, pDO + ls.y* pFac, col);
            //polyDbg->drawPoint(pDO + edgePt.x*pFac, pDO + edgePt.y*pFac, col, 2);

            const T ptNorm = normalize(edgePt - lineStart);
            const float ptDot = dot(ptNorm, axisMidNorm);

            if (selAxis == &axisANorm) {
                if ((!ortho && ptDot < bestDotA) || (ortho && (d2 > axisADistSq))) {
                    edgeA = pt;
                    axisADistSq = d2;
                    axisABest = edgePt;
                    bestDotA = ptDot;
                }
            } else {
                if ((!ortho && ptDot < bestDotB) || (ortho && (d2 > axisBDistSq))) {
                    edgeB = pt;
                    axisBDistSq = d2;
                    axisBBest = edgePt;
                    bestDotB = ptDot;
                }
            }

        }
        }

        //T ln = this->get(i)->normalConst()*float(pFac);
        //polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x,pDO+ ls.y* pFac + ln.y, ColorRGBA(255, 127, 0, 127));
    }


    Projection ret;
    ret.intersections[0] = axisABest;
    ret.intersections[1] = axisBBest;
    ret.points[0] = edgeA;
    ret.points[1] = edgeB;
    
    //Projection failed, we're colinear
    if(edgeA == edgeB) edgeA = edgeB = nullptr;
    
    if (edgeA != nullptr && edgeB != nullptr) {
        if(!view.admissable && (!view.bounded && view.ortho)) {//The hard way, sum all the norm dots and choose the most aligned with dStart
            float dotSumP = 0.f, dotSumN = 0.f; 
            int dotCountP = 0, dotCountN = 0;
            for(Point* pt = edgeA->right(); pt != edgeB; pt = pt->right()) {
                dotSumP += dot(pt->normalConst(), axisMidNorm);
                dotCountP++;
            }
            for(Point* pt = edgeA->left(); pt != edgeB; pt = pt->left()) {
                dotSumP += dot(pt->normalConst(), axisMidNorm);
                dotCountN++;
            }
            
            if(dotCountP > 0) dotSumP /= float(dotCountP);
            if(dotCountN > 0) dotSumN /= float(dotCountN);
            
            if(dotSumP > dotSumN) ret.winding = 1;
            else ret.winding = -1;
        } else {
            T leftNorm = normalize(edgeA->left() ->positionConst() - edgeA->positionConst());
            T rightNorm = normalize(edgeA->right()->positionConst() - edgeA->positionConst());
            const T toOrigin = normalize(dStart - edgeA->positionConst());

            const float lnDot = dot(toOrigin, leftNorm), rnDot = dot(toOrigin, rightNorm);

            if(rnDot < lnDot) ret.winding = 1;
            else ret.winding = -1;
        }
        /*
        if (ortho) ret.winding = 1;
        else ret.winding = (dot(leftNorm, axisMidNorm) > dot(rightNorm, axisMidNorm)) ? 1 : -1;
         * */
    }
    return ret;
}
inline static void printPoly(NPolygon<N2,PhysicsPt>* poly) {
    BLOCKDBG std::cout << "\tPrintPoly size=" << poly->size() << "\n";
    for(unsigned int i = 0; i < poly->size(); i++) {
        BLOCKDBG std::cout << "\t\t"<<i<<"\t"<<VP(poly->get(i)->positionConst()) << "\n";
    }
}

template<typename T, typename PT_T>
inline void NPolygon<T, PT_T>::projectOnto(const NPolygon<T, PT_T>::Viewpoint &view) {
    if(this->size() <= 4) return;//No point, no verts can be eliminated safely
    
    const Projection proj = this->projection(view);
    if(!proj) return;
    
    BLOCKDBG std::cout << "\tProjectOnto sz="<<size()<<" got start="<<this->index(proj.points[0]) << ":" << VP(proj.points[0]->positionConst()) << ", end=" <<
            this->index(proj.points[1]) << ":" << VP(proj.points[1]->positionConst()) << " with winding " << int(proj.winding) << "\n"; 
    
    int eraseInc = proj.winding;
    N2 ptA = proj.points[0]->positionConst(), ptB = proj.points[1]->positionConst();
    int eraseStart = this->index(proj.points[0]) + eraseInc, 
          eraseEnd = this->index(proj.points[1]) - eraseInc;
    
    if(eraseStart + eraseInc == eraseEnd || eraseEnd - eraseInc == eraseStart) return;//Don't delete the entire polygon (or direct siblings)
    
    printPoly(this);
    BLOCKDBG std::cout << "\terasing from " << eraseStart << " to " << eraseEnd << "\n";
    for(int i = eraseStart; ; i = (i+eraseInc < 0)? size()-1 : (i+eraseInc)%size()) {
        BLOCKDBG std::cout << "\t\tShould erase " << i << "\n";
        if( i == sgnmod(eraseEnd, int(this->size()))) break;
    }
    
    int newIdx = this->removeRange(eraseStart, eraseEnd, eraseInc);// + 2*eraseInc;
    BLOCKDBG std::cout << "\tnewIdx = " << newIdx << "\n";
    printPoly(this);
    BLOCKDBG std::cout << "END PROJECT\n";
    
    
    //this->insert(newIdx, view.end, -1);
    if(eraseInc == 1) {
        if(distance2(proj.intersections[1], ptB) > 0.1f) this->insert(newIdx, proj.intersections[1], -1);
        this->insert(newIdx, view.end, -1);
        if(distance2(proj.intersections[0], ptA) > 0.1f) this->insert(newIdx, proj.intersections[0], -1);
        //newIdx = std::min(eraseStart,eraseEnd)+eraseInc;
        //if(distance2(proj.intersections[1], ptB) > 0.1f) newIdx = this->insert(newIdx, proj.intersections[1], -1);
        //this->insert(newIdx, view.end, -eraseInc);
        //this->insert(newIdx, view.start, 1);
        //if(distance2(proj.intersections[0], ptA) > 0.1f) this->insert(newIdx, proj.intersections[0], -1);
    } else {
        newIdx = this->insert(newIdx, view.end, 1);
        if(distance2(proj.intersections[0], ptA) > 0.1f) this->insert(newIdx, proj.intersections[0], 1);
        if(distance2(proj.intersections[1], ptB) > 0.1f) this->insert(newIdx, proj.intersections[1], -1);
        /*this->insert(newIdx, proj.intersections[1], -1);
        this->insert(newIdx, view.end, -1);
        this->insert(newIdx, proj.intersections[0], -1);*/
    }

    this->repair();
    /*castDir = normalize(castDir);
    Self *rt = this->root();
    T oobOffset = castDir * (this->bounds.dimensions() + T(1.f));
    
    for(unsigned int vid = 0; vid < this->size(); vid++) {
        T interMin, interMax; Point* pt = this->get(vid);
        const T& ptPos = pt->positionConst(), &ptNorm = pt->normalConst();
        const bool intersected = rt->intersect(ptPos + oobOffset, ptPos - oobOffset, interMin, interMax);
        
        if(!intersected || distance2(ptPos, interMin) > 0.1f) continue;
        
        T baseToP = interMin - castBase;
        T baseToPNorm = normalize(baseToP);
        
        //Select most aligned axis to intersect with
        T *const selAxis = (dot(baseToP, axisA) >= dot(baseToP, axisB)? &axisA : &axisB);
        float thetaBase = acos(dot(*selAxis, baseToPNorm));
        float thetaP = acos(dot(castDir, -baseToPNorm));
        float thetaInter = float(M_PI) - thetaBase - thetaP;
        
        if(sin(thetaInter) == 0.f) continue;
        
        float interDist = length(baseToP) / sin(thetaInter) * sin(thetaBase);
        T interVec = castDir * interDist;
        
        pt->position() = ptPos + interVec;
    }*/
}


#endif /* INLDEV_H */


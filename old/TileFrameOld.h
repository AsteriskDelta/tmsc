#ifndef TILEFRAME_H
#define TILEFRAME_H
#include "../TMSCInclude.h"
#include <ARKE/Loadable.h>
#include <ARKE/Image.h>
#include <NVX/NPolygon.h>
#include "physics/PhysicsPt.h"

class TileFrame : public virtual Loadable {
public:
    TileFrame();
    TileFrame(const std::string& p);
    virtual ~TileFrame();
    
    typedef NPolygon<N2, PhysicsPt> Poly;
    
    inline virtual std::string getName() const override {
        return this->getBaseName(subPath, "", "/.");
    }

    virtual bool couldLoad(const std::string& p) const override;
    virtual bool load(const std::string& p) override;
    virtual void unload() override;
    inline virtual void* rawInstancePtr() override { return reinterpret_cast<void*>(this); };
    
    std::string tileName() const;
    
    static TileFrame* Factory(const std::string p = "");
    
    inline Poly* polygon() { return this->poly; };
    inline const Poly* polygon() const { return this->poly;};
    
    void renderDebug(ImageRGBA *proc);
private:
    Poly *poly, *origPoly;
    Bounds<N2> bounds;
    
    ImageRGBA *img;
    unsigned int atlasID;
    
    void calculatePolygon(bool dbg = false);
};

#endif /* TILEFRAME_H */


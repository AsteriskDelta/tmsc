#ifndef ENTITYTREE_H
#define ENTITYTREE_H
#include "../TMSCInclude.h"
#include "TMSCFundamental.h"
#include <vector>

class Entity;
class MapSector;
class SpriteGroup;
class DebugGroup;

class EntityTree : public virtual Proxy, public virtual Renderable, public virtual PhysicalObject {
public:
    EntityTree();
    virtual ~EntityTree();
    
    void add(Entity* entity);
    void remove(Entity* entity);
    
    inline virtual EntityTree* parent() const override {
        return _derive<EntityTree>(Proxy::_parentPtr);
    }
    
    bool hasRenderTransform();  
    Transform getRenderTransform();
    
    virtual void prerender(int pass) override;
    virtual void uploadRender(int pass) override;
    virtual void render() override;
protected:
    SpriteGroup *spriteGroup;
    DebugGroup *dbgGroup;
private:
    void localAdd(Entity *entity);
    void localRemove(Entity *entity);
    
    //Both are x-ref'd by Proxy
    std::vector<Entity*> entities;
    std::vector<EntityTree*> branches;
};

#endif /* ENTITYTREE_H */


#include "SpriteFrame.h"
#include "Sprite.h"

TCoord SpriteFrame::diagonalPermute(TCoord norm, bool mul) {
    if(diagonalMult.x != 1 || diagonalMult.y != 1) norm = TCoord(norm.y, norm.x);
    
    if(mul) norm *= mult;
    return norm;
}

TCoord SpriteFrame::diagonalMultiply(TCoord norm) {
    /*int vdot = abs(sgn(norm.x) + sgn(norm.y));
    norm *= (vdot == 2)? TCoord(diagonalMult.y) : TCoord(1);
    norm *= (vdot == 0)? TCoord(diagonalMult.x) : TCoord(1);
    
    if(vdot == 1 && (diagonalMult.x != 1 || diagonalMult.y != 1)) {//I multiplied by these before, and have no idea why... they seemed to mess up diagonal transforms, so..
        if(norm.x == 0) norm = TCoord(norm.y, norm.x);//TCoord(diagonalMult.x)*
        else if(norm.y == 0) norm = TCoord(norm.y, norm.x);//TCoord(diagonalMult.y)*
    }*/
    norm = this->diagonalPermute(norm);
    //if(norm.x == norm.y) norm *= -1;
    
    return norm;
}

TCoord SpriteFrame::multiply(TCoord norm) {
    norm = this->diagonalMultiply(norm * mult);
    //if(mult.x != mult.y && diagonalMult != TCoord(1)) norm *= -1.f;
    return norm;
}

TCoord SpriteFrame::multiplyQ1(TCoord p, TCoord max) {
    TCoord zeroOff = p;
    p = p * 2 - zeroOff;
    p = this->multiply(p);
    p = (p + zeroOff)/2;
    if(max.x > -1 && max.y > -1) p = tcRollover(p, max);
    return p;
}

TCoord SpriteFrame::diagonalMultiplyQ1(TCoord p, TCoord max) {
    TCoord zeroOff = p;
    p = p * 2 - zeroOff;
    p = this->diagonalMultiply(p);
    p = (p + zeroOff)/2;
    if(max.x > -1 && max.y > -1) p = tcRollover(p, max);
    return p;
}

TCoord SpriteFrame::multiplyTex(TCoord norm) {
     /*norm *= TCoord(mult.x, -mult.y);//Transform to texture (y-minus) space
    
    int vdot = abs(norm.x+norm.y)/2;
    norm *= (vdot == 1)? TCoord(diagonalMult.y) : TCoord(1);
    norm *= (vdot == 0)? TCoord(diagonalMult.x) : TCoord(1);
    return norm;*/
    // norm *= TCoord(mult.x, -mult.y);//Transform to texture (y-minus) space
    
    //int vdot = abs(norm.x+norm.y)/2;
    //norm *= (vdot == 1)? TCoord(diagonalMult.y) : TCoord(1);
    //norm *= (vdot == 0)? TCoord(diagonalMult.x) : TCoord(1);
    //norm = this->multiply(norm/**TCoord(1,-1)*/);//ADjust to image space
    //if(mult.x == mult.y && diagonalMult != TCoord(1)) norm *= -1;
    return norm;
}

TCoord SpriteFrame::effectiveVert(const TCoord& vert) {
    TCoord effVert = vert*2 - TCoord(1);
    //std::cout << "effVert got " << to_string(vert) << ", transformed to " << to_string(effVert) << " via " <<
    //        (mult.x < 0) << ", " << (mult.y < 0) << " from " << VP(mult) << "\n";
    
    //effVert.y = (effVert.y+1)%2;
    return (this->multiplyTex(effVert)+TCoord(1))/2;
}

N2T<Uint16, 1> SpriteFrame::getTexCoords(const TCoord& vert, const TCoord& skew) {
    if (sprite == nullptr) return N2T<Uint16, 1>(0, 0);
    return sprite->getTexCoords(this->effectiveVert(vert), this->multiplyTex(skew));
}

N2T<Uint16, 1> SpriteFrame::getOffsetCoords(const TCoord& vert, const TCoord& skew) {
    if (sprite == nullptr) return N2T<Uint16, 1>(0, 0);
    return sprite->getOffsetCoords(this->effectiveVert(vert), this->multiplyTex(skew));
}

unsigned short SpriteFrame::getAtlasPage() {
    if (sprite == nullptr) return 0;
    else return sprite->getAtlasPage();
}

void SpriteFrame::makeIdentity() {
    mult = TCoord(1);
    diagonalMult = TCoord(1);
    offsetPX = TCoord(0);
}

N2 SpriteFrame::diagonalPermute(N2 norm) const {
    if(diagonalMult.x != 1 || diagonalMult.y != 1) return N2(norm.y, norm.x);
    else return norm;
}

N2 SpriteFrame::multiply(N2 norm) const {
    norm = this->diagonalPermute(norm * N2(mult.x, mult.y));
    //if(mult.x != mult.y && diagonalMult != TCoord(1)) norm *= -1.f;
    //if(mult.x == mult.y && diagonalMult != TCoord(1)) norm *= -1;
    return norm;
}
N2 SpriteFrame::multiplyQ1(N2 p) const {
    p = this->diagonalPermute(p);
    
    if(mult.x < 0) p.x = 1.f-p.x;
    if(mult.y < 0) p.y = 1.f-p.y;
    if(mult.x != mult.y && diagonalMult != TCoord(1)) p = N2(1.f) - p;
    return p;
    /*N2 zeroOff = p;
    p = p * 2.f - zeroOff;
    p = this->multiply(p);
    p = (p + zeroOff)/2.f;
    return p;*/
}

N2 SpriteFrame::invMultiply(N2 norm) const {
    norm = this->invDiagonalPermute(norm);
    norm = norm * -N2(mult);
    return norm;
}
N2 SpriteFrame::invMultiplyQ1(N2 p) const {
    if(mult.x != mult.y && diagonalMult != TCoord(1)) p = N2(1.f) - p;
    
    if(mult.y < 0) p.y = 1.f-p.y;
    if(mult.x < 0) p.x = 1.f-p.x;
    
    return this->invDiagonalPermute(p);
}
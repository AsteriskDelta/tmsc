#include "SpriteInstance.h"
#include "render/SpriteGroup.h"
#include "render/SpriteVert.h"
#include <ARKE/QuadGroup.h>
#include "sprite/Sprite.h"
#include "render/DebugGroup.h"

SpriteInstance::SpriteInstance() : group(nullptr), subID(0), frame(),tint(ColorRGBA(255,255,255,255)), snapToPixel(false), zOffset(0.f) {
    
}

SpriteInstance::SpriteInstance(SpriteGroup *par, unsigned int sID, const SpriteFrame& frm) : group(par), subID(sID), frame(frm),tint(ColorRGBA(255,255,255,255)), snapToPixel(false), zOffset(0.f) {
    this->updateBounds();
}

SpriteInstance::~SpriteInstance() {
    if(group != nullptr) group->remove(this, false);
}

Sprite* SpriteInstance::sprite() const {
    return frame.sprite;
}
void SpriteInstance::setFrame(const SpriteFrame& fr) {
    frame = fr;
    this->updateBounds();
}

N2 SpriteInstance::getOffsetVector() const {
    return N2(float(frame.offsetPX.x)/**UNIT_PER_PX*/, float(frame.offsetPX.y)/**UNIT_PER_PX*/);
}
N2 SpriteInstance::base() const {
    N2 offsetVec = this->getOffsetVector();
    //offsetVec = frame.diagonalPermute(offsetVec);
    //if(frame.diagonalMult != TCoord(1)) offsetVec *= N2(-1.f, 1.f);
    
    N2 ll = N2(0.f);//-N2(internalSpan()/2.f);
    ll += offsetVec;
    return ll;
}

N2 SpriteInstance::internalSpan() const {
    return sprite()->imgSize();
}
PhysicsPoly* SpriteInstance::poly() const {
        return sprite()->polygon();
    }
const Bounds<N2>* SpriteInstance::bounds() const {
        return &(sprite()->bounds);
    }

void SpriteInstance::update() {
    this->updateBounds();
    this->genVerts();
}
void SpriteInstance::updateBounds() {
    if(sprite() == nullptr) return;
    localBounds.zero();
    localBounds.add(sprite()->bounds.center);
    localBounds.add(sprite()->bounds.farPoint());
}

void SpriteInstance::genVerts() {
    if(group == nullptr) {
        Console::Err("SpriteInstance::genVerts() called without assigned group");
        return;
    }
    
    SpriteVert *verts = group->getVertexPtr(subID);
    if(verts == nullptr) {
        Console::Err("SpriteInstance::genVerts() given null vert ptr by group ", group, " for id ", subID);
    }
    
    /*if(snapToPixel) {
        transform.position = V3(round(N2(transform.position)*float(DEF_TILESIZE)) / float(DEF_TILESIZE), transform.position.z);
        size = round(N2(size)*float(DEF_TILESIZE)) / float(DEF_TILESIZE);
    }*/
    
    //V3 ll = V3(this->base(), 0.f), sz = V3(internalSpan(), 0.f);
    
    /*if(floor(ll) + V3(0.5f, 0.5f, 0.f) != ll) {
        std::cout << "\tsprite has off-grid pos, ll=" << VP(ll) << ", sz=" << VP(size) << "\n";
    }*/
    
    //std::cout << "\t\t\tSpriteInstance::genVerts() to " << subID << " on group " << group << "\n";
    static const TCoord corners[4] = {TCoord(0,0), TCoord(1,0), TCoord(1,1), TCoord(0,1)};
    for(int i = 0; i < 4; i++) {
        const TCoord& crd = corners[i];
        const TCoord skew = crd*2 - TCoord(1);
        const N2 vPos = this->internalSpan() * N2(crd);
        //verts[i].position = V3(V2(vPos), 0.f);//ll + sz*V3(crd.x, crd.y, 0.f);
        
        //const unsigned short skewScale = 8;
        //N2T<Uint16, 1> texPixOff(static_cast<unsigned short>(skew.x), static_cast<unsigned short>(skew.y));
        
        verts[i].texCoords = frame.getTexCoords(crd, skew);
        verts[i].offsetCoords = frame.getOffsetCoords(crd, skew);
        //std::cout << "\t\tset vert " << i << " to coords " << to_string(verts[i].texCoords) << "\n";
        
        verts[i].tint = tint;//ColorRGBA(255, 255, 255, 255);
        verts[i].atlas = frame.getAtlasPage();
        
        verts[i].position = V3(V2(this->localToRender(vPos)), zOffset);//this->matrix(false) * verts[i].position;
        if(true||snapToPixel) {
            verts[i].position.x = round(verts[i].position.x * DEF_TILESIZE) / float(DEF_TILESIZE);
            verts[i].position.y = round(verts[i].position.y * DEF_TILESIZE) / float(DEF_TILESIZE);
            //verts[i].texCoords = (verts[i].texCoords / skewScale) * skewScale;
        }
        /*if(this->tint.r == 13) std::cout << "\t\tVERT " << to_string(ll + sz*V3(crd.x, crd.y, 0.f)) << " -> " << to_string(verts[i].position) <<
                " via Transform" << to_string(transform.position)<<" x"<<to_string(transform.ratio) << "\n";*/
    }
}

N2 SpriteInstance::parentToLocal(N2 parPos) const {
    const N2 srcPos = Transformable::parentToLocal(parPos);
    N2 relPos = (srcPos - this->base()) / this->internalSpan();
    relPos = frame.invMultiplyQ1(relPos);
    relPos.y = 1.f - relPos.y;
    return relPos * sprite()->imgSize();
}
N2 SpriteInstance::localToParent(N2 locPos) const {
    N2 relPos = locPos / N2(sprite()->imgSize());
    //relPos = frame.multiplyQ1(relPos);
    relPos.y = 1.f - relPos.y;
    relPos = frame.multiplyQ1(relPos);
    /*relPos = relPos*2.f - N2(1.f);
    relPos = frame.multiply(relPos*N2(1.f,-1.f));//ADjust to image space
    relPos = (relPos + N2(1.f))/2.f;*/
    //relPos = frame.diagonalPermute(relPos);
    //relPos = frame.multiplyQ1(relPos);
    //relPos.y *= -1.f;//Image to normal coord system
    
    N2 rawPos = (this->base()) +  (relPos) * this->internalSpan();// * size;//frame.diagonalPermute(this->base()) + relPos * frame.multiply(size);
    //std::cout << "\tSprite::localparent["<<VP(locPos)<<"], relPos=" << VP(relPos) << " rawPos="<<VP(rawPos)<<"\n";
    
    return Transformable::localToParent(rawPos);//N2(transform * V3(rawPos, 0.f));
}

void SpriteInstance::drawDebug(DebugGroup *dbg) {
    if(sprite() == nullptr) return;
    
    sprite()->drawDebug(dbg, this);
}
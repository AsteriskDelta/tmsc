#include "SpriteUser.h"

SpriteUser::SpriteUser() : phyCachedPoly(nullptr), phyCachedBounds() {
}

SpriteUser::SpriteUser(const SpriteUser& orig) : Adapter(orig), Transformable(orig), phyCachedPoly(nullptr), phyCachedBounds(orig.phyCachedBounds) {//Don't copy poly cache, it's per-object
}

SpriteUser::~SpriteUser() {
    this->freeAgnostics();
}

Sprite* SpriteUser::sprite() const {
    return nullptr;
}
PhysicsPoly* SpriteUser::poly() const {
    return nullptr;
}

void SpriteUser::computeLocals() {
    phyLocalBounds.zero();
    //Nothing need be done for the poly
    //But add it to our bounds
    if(isComplete() && this->poly() != nullptr) {
        phyLocalBounds.add(this->poly()->bounds);
    }
}

Bounds<N2> SpriteUser::getParentBoundsOf(const Bounds<N2>& bnd) const {
    Bounds<N2> ret;
    ret.add(this->localToParent(bnd.nearPoint()));
    ret.add(this->localToParent(bnd.farPoint()));
    return ret;
}

Bounds<N2> SpriteUser::getParentBounds() const {
    if(!isComplete() || this->bounds() == nullptr) return Bounds<N2>();
    else return this->getParentBoundsOf( *(this->bounds()) );
}

void SpriteUser::computeAgnostics() {
    //if(this->poly() == nullptr) return this->freeAgnostics();
    if(this->isComplete() && this->poly() != nullptr) {
        PhysicsPoly *srcPoly = this->poly(), *destPoly;
        if(phyCachedPoly == nullptr) phyCachedPoly = new PhysicsPoly();
        phyCachedPoly->resize(srcPoly->size());
        destPoly = phyCachedPoly;

        for(unsigned int i = 0; i < srcPoly->size(); i++) {
            PhysicsPt *srcPt = &srcPoly->get(i), *destPt = &destPoly->get(i);
            destPt->position() = this->localToAgnostic(srcPt->position());
            destPt->normal() = this->localNormToAgnostic(srcPt->position(), srcPt->normal());
            destPt->flags() = srcPt->flags();
            destPt->meta = srcPt->meta;
            //destPt->parentPtr = destPoly;

            //phyCachedBounds.add(destPt->pt);
        }
        destPoly->repair();
    }
    
    phyCachedBounds.setType(Bounds<N2>::Square);
    phyCachedBounds.zero();
    //std::cout << "spriteuser::comp\n";
    if(this->isComplete() && bounds() != nullptr) {
        //std::cout << "SpriteUser computeAg: adding " <<VP(bounds()->center) << ", " << VP(bounds()->farPoint()) << "\n";
        phyCachedBounds.add(this->localToAgnostic(bounds()->center));
        phyCachedBounds.add(this->localToAgnostic(bounds()->farPoint()));
        //std::cout << "SpriteUser Got ag: " <<VP(this->localToAgnostic(bounds()->center)) << ", " << VP(this->localToAgnostic(bounds()->farPoint())) << "\n";
    }
    
}

PhysicsPoly* SpriteUser::agnosticPoly() {
    return phyCachedPoly;
}
const Bounds<N2>* SpriteUser::bounds() const {
    return &phyLocalBounds;
}
const Bounds<N2>* SpriteUser::agnosticBounds() const {
    return &phyCachedBounds;
}


void SpriteUser::freeAgnostics() {
    if(phyCachedPoly != nullptr) {
        delete phyCachedPoly;
        phyCachedPoly = nullptr;
    }
}
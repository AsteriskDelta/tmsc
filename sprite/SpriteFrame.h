#ifndef SPRITEFRAME_H
#define SPRITEFRAME_H
#include "../TMSCInclude.h"
#include <NVX/N2.h>

class Sprite;

class SpriteFrame {
public:
    inline SpriteFrame() : sprite(nullptr), offsetPX(0), mult(1), diagonalMult(1) {};
    inline SpriteFrame(Sprite *f, const TCoord& c = TCoord(0,0), const TCoord& m = TCoord(1,1), const TCoord& dm = TCoord(1,1)) : sprite(f), offsetPX(c), mult(m), diagonalMult(dm) {};
    inline SpriteFrame(const SpriteFrame& orig) : sprite(orig.sprite), offsetPX(orig.offsetPX), mult(orig.mult), diagonalMult(orig.diagonalMult) {};
    inline virtual ~SpriteFrame() {};
    
    inline operator bool() const { return sprite != nullptr; };
    
    Sprite *sprite;
    TCoord offsetPX;
    TCoord mult, diagonalMult;//For orientation
    
    TCoord effectiveVert(const TCoord& vert);
    N2T<Uint16, 1> getTexCoords(const TCoord& vert, const TCoord& skew = TCoord(0,0));
    N2T<Uint16, 1> getOffsetCoords(const TCoord& vert, const TCoord& skew = TCoord(0,0));
    unsigned short getAtlasPage();
    
    //Just diagonal
    TCoord diagonalMultiply(TCoord p);
    TCoord diagonalMultiplyQ1(TCoord p, TCoord max = TCoord(-1, -1));
    //Preserve values- just swap them between axes
    TCoord diagonalPermute(TCoord p, bool mul = false);
    
    //Diagonal and mult transforms
    TCoord multiply(TCoord norm);
    TCoord multiplyQ1(TCoord p, TCoord max = TCoord(-1,-1));
    void makeIdentity();
    
    N2 diagonalPermute(N2 norm) const;
    N2 multiply(N2 norm) const;
    N2 multiplyQ1(N2 norm) const;
    
    N2 invMultiply(N2 norm) const;
    N2 invMultiplyQ1(N2 norm) const;
    inline N2 invDiagonalPermute(N2 norm) const {//Is its own inverse
        return this->diagonalPermute(norm);
    }
    
    inline TCoord unit() {
        return this->multiply(TCoord(1));
    }
private:
    TCoord multiplyTex(TCoord norm);
};

#endif /* SPRITEFRAME_H */


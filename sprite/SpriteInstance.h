#ifndef SPRITEINSTANCE_H
#define SPRITEINSTANCE_H
#include "../TMSCInclude.h"
#include "../physics/Transformable.h"
#include "SpriteFrame.h"
#include "SpriteUser.h"
#include <NVX/Bounds.h>

class Sprite;
class SpriteGroup;
class DebugGroup;

class SpriteInstance : public virtual Transformable, public virtual SpriteUser {
    friend class SpriteGroup;
public:
    SpriteInstance();
    SpriteInstance(SpriteGroup *par, unsigned int sID, const SpriteFrame& frm);
    virtual ~SpriteInstance();
    
    SpriteGroup *group;
    unsigned int subID;
    inline bool hasGroup() const { return group != nullptr; };
    
    virtual void setFrame(const SpriteFrame& fr);
    
    //Transform transform;
    //N2 size;
    SpriteFrame frame;
    ColorRGBA tint;
    bool snapToPixel;
    float zOffset;
    
    virtual void update();
    void updateBounds();
    
    virtual void drawDebug(DebugGroup *dbg);
    
    N2 getOffsetVector() const;
    N2 base() const;//lower left
    
    virtual N2 internalSpan() const override;
    
    virtual bool isLinearTransform() const override { return false; };
    virtual N2 parentToLocal(N2 parPos) const override;
    virtual N2 localToParent(N2 locPos) const override;
    
    virtual Sprite* sprite() const override;
    virtual PhysicsPoly* poly() const override;
    
    virtual const Bounds<N2>* bounds() const override;
    inline virtual Bounds<N2> parentBounds() const {
        Bounds<N2> ret;
        ret.add(this->localToParent(localBounds.nearPoint()));
        ret.add(this->localToParent(localBounds.farPoint()));
        return ret;
    }
private:
    Bounds<N2> localBounds;
    void genVerts();
};

#endif /* SPRITEINSTANCE_H */


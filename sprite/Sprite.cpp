#include "Sprite.h"
#include "SpriteInstance.h"
#include <NVX/NPolygon.h>
#include <ARKE/Image.h>
#include <string>
#include "render/DebugGroup.h"

#define PX_FLAG(x) (0x1 << x)
#define PX_OFFX(i) (-(((int)i+1)%2))
#define PX_OFFY(i) (-(((int)(i/2 + 1)%2)))
#define LL 0
#define LR 1
#define UL 2
#define UR 3

TextureAtlas* Sprite::spriteAtlas[SPRITE_MULTI_ATLAS];
bool Sprite::spriteAtlasInit = false, Sprite::spriteAtlasDirty = true;

static const short atlasSize = 512;

void Sprite::AllocateAtlas() {
    for(int i = 0; i < SPRITE_MULTI_ATLAS; i++) {
        spriteAtlas[i] = new TextureAtlas(atlasSize, atlasSize, 2);
        spriteAtlas[i]->setName("spriteAtlas"+std::to_string(i));
        spriteAtlas[i]->setMipmap(true);
        spriteAtlas[i]->setPadding(1);
    }
    spriteAtlasInit = true;
}

void Sprite::UploadAtlas() {
    if(!spriteAtlasInit) {
        Console::Err("Sprite::UploadAtlas() called, but atlas not yet initialized!");
        return;
    }
    for(int i = 0; i < SPRITE_MULTI_ATLAS; i++) {
        spriteAtlas[i]->upload();
    }
    Console::Out("Sprite atlas uploaded OK...");
    spriteAtlasDirty = false;
}

void Sprite::CacheAtlas() {
    if(!spriteAtlasInit) {
        Console::Err("Sprite::CacheAtlas() called, but atlas not yet initialized!");
        return;
    }
    for(int i = 0; i < SPRITE_MULTI_ATLAS; i++) {
        spriteAtlas[i]->saveCache();
    }
}

void Sprite::BindAtlas(Shader *shader) {
     if(!spriteAtlasInit) {
        Console::Err("Sprite::BindAtlas() called, but atlas not yet initialized!");
        return;
    }
    
    for(int i = 0; i < SPRITE_MULTI_ATLAS; i++) {
        spriteAtlas[i]->bind(shader, 3+i, 13+i);
    }
}

Sprite::Sprite() : Loadable(), poly(nullptr), origPoly(nullptr), bounds(), img(nullptr), detail(nullptr), mask(nullptr), atlasID(0) {
    
}

Sprite::Sprite(const std::string& p, bool rel) : Sprite() {
    this->load(p, rel);
}

Sprite::~Sprite() {
    this->unload();
}

bool Sprite::couldLoad(const std::string& p) const {
    return p.size() >= 4 && p.find(".png") != std::string::npos;
}

bool Sprite::load(const std::string& p, bool rel) {
    if (p.size() > 0) path = p;
    //Console::Out("Sprite::load(", p, ")");
    if (!Loadable::load(path)) return false;

    img = new ImageRGBA(p, rel);
    img->repColor(ColorRGBA(0, 0, 0, 255), ColorRGBA(0, 0, 0, 0));
    return true;
}

void Sprite::unload() {
    if (img != nullptr) delete img;
    if (detail != nullptr) delete img;
    if (mask != nullptr) delete img;
    if (poly != nullptr) delete poly;
    if (origPoly != nullptr) delete poly;
}

void Sprite::flip(bool doX, bool doY) {
    if(img == nullptr) return;
    img->flip(doX, doY);
}

N2 Sprite::imgSize() const {
    if(img != nullptr) return N2(img->width, img->height);
    else return N2(0.f);
}

const short skewFac = 8;
N2T<Uint16, 1> Sprite::getTexCoords(const TCoord& vert, const TCoord& skew) const {
    N2T<Int16, 1>useVert(vert.x, vert.y);
    //std::cout << "gTC " << VP(useVert) << " \n";
    N2T<Int16, 1>raw = N2T<Int16, 1>(skewFac*(N2T<Int16, 1>(texCoordBase) + 
            N2T<Int16, 1>(texCoordSize )*useVert)) - N2T<Int16, 1>(skew*int(skewFac));
    return N2T<Uint16, 1>(clamp(N2T<Int16, 1>(raw), N2T<Int16, 1>(0), N2T<Int16, 1>(atlasSize*skewFac)));
}
N2T<Uint16, 1> Sprite::getOffsetCoords(const TCoord& vert, const TCoord& skew) const {
    N2T<Int16, 1>raw = skewFac*(N2T<Int16, 1>(texCoordOffsetBase) + N2T<Int16, 1>(texCoordOffsetSize)*N2T<Int16, 1>(vert)) - N2T<Int16, 1>(skew*int(skewFac));
    return N2T<Uint16, 1>(raw);
}
unsigned short Sprite::getAtlasPage() const {
    return atlasPage;
}

void Sprite::generatePhysics() {
    this->calculatePolygon(false);//this->calculatePolygon(true);
}

void Sprite::prepare() {
    this->generatePhysics();
    this->upload();
}
void Sprite::upload() {
    if(img == nullptr) return;
    if(!spriteAtlasInit) AllocateAtlas();
    
    atlasID = spriteAtlas[0]->emplace(path, img, false);
    if(detail != nullptr) spriteAtlas[1]->emplace(path, img, false);
    
    TextureAtlas::Lookup* lookup = spriteAtlas[0]->find(atlasID);
    texCoordBase = N2T<Uint16, 1>(lookup->x, lookup->y);
    texCoordSize = N2T<Uint16, 1>(lookup->w, lookup->h);
    texCoordOffsetBase =  N2T<Uint16, 1>(lookup->ox, lookup->oy);
    texCoordOffsetSize = N2T<Uint16, 1>(lookup->ow, lookup->oh);
    //std::cout << "uploaded sprite " << name << " for base="<<VP(texCoordBase) << ", sz=" << VP(texCoordSize) << "\n"
    //        << "\toffBase=" << VP(texCoordOffsetBase) << ", offSize=" << VP(texCoordOffsetSize) << "\n";
    atlasPage = lookup->layer;
    spriteAtlasDirty = true;
}

const static bool pixDbg = false;

static void setPixProcFlags(ImageRGBA * const& img, int x, int y, bool state) {
    //unsigned short rx = x, ry = y;
    for (unsigned char i = 0; i < 4; i++) {
        if (!img->hasPixel(x + PX_OFFX(i), y + PX_OFFY(i))) continue;
        if (pixDbg) std::cout << "\tSETPF " << (x + PX_OFFX(i)) << ", " << (y + PX_OFFY(i)) << " : " << int(i) << "\n";
        if (state) (img->getPixelRef(x + PX_OFFX(i), y + PX_OFFY(i))).b |= PX_FLAG(i); //true
        else (img->getPixelRef(x + PX_OFFX(i), y + PX_OFFY(i))).b &= ~PX_FLAG(i); //false
    }
}

static bool getPixProcFlags(ColorRGBA * const &cols, bool withAlpha = false) {
    bool valid = true; bool hasAlpha = false, hasNonalpha = false;
    bool interest = !withAlpha;
    if (pixDbg) std::cout << "\t\t\t\tProcFlags{";
    for (unsigned char i = 0; i < 4; i++) {
        valid &= ((cols[i].b & PX_FLAG(i)) == 0 || cols[i].b == 0xFF);
        hasAlpha |= (cols[i].alpha() > 0);
        hasNonalpha |= (cols[i].alpha() == 0);
        if (pixDbg) std::cout << ((cols[i].b & PX_FLAG(i)) != 0) << ":" << int(cols[i].alpha()) << ", ";
    }
    interest = hasAlpha && hasNonalpha;
    valid &= interest;
    if (pixDbg) std::cout << "}=" << valid << "\n";
    return valid;
}

static bool getPixInterestFlags(ColorRGBA * const &cols, bool strict) {
    bool ofInterest = false;
    bool hasAlpha = false, hasNonalpha = false;
    for (unsigned char i = 0; i < 4; i++) {
        //ofInterest |= (((cols[i].b & PX_FLAG(i)) == 0 || (!strict && cols[i].b == 0xFF)) && cols[i].alpha() > 0);
        hasAlpha |= (((cols[i].b & PX_FLAG(i)) == 0 || (!strict && cols[i].b == 0xFF)) && cols[i].alpha() > 0);
        hasNonalpha |= (((!strict && cols[i].b == 0xFF)) || cols[i].alpha() == 0);
    }
    ofInterest = hasAlpha && hasNonalpha;
    return ofInterest;
}

static void getPixSample(ImageRGBA * const& img, int x, int y, ColorRGBA * const &cols) {
    for (unsigned char i = 0; i < 4; i++) {
        cols[i] = img->getPixelSafe(x + PX_OFFX(i), y + PX_OFFY(i), ColorRGBA(0));
        //if (pixDbg) std::cout << "\tGETPIXS " << (x + PX_OFFX(i)) << ", " << (y + PX_OFFY(i)) << " = " << cols[i].toString() << "\n";
    }
}

nhighp getEffAlpha(ColorRGBA * const &cols, unsigned short i) {
    if (cols[i].b == 0xFF) return 0.f;
    //std::cout << "\tgetEffAlpha[i] = " << int(cols[i].v[3]) << " for highp=" << nhighp(cols[i].v[3])/255 << "\n";
    return nhighp(cols[i].getAlpha());
}

static N2 getPixNorm(ColorRGBA * const &cols) {
    const nhighp uPart = (getEffAlpha(cols, UL) + getEffAlpha(cols, UR)) / 2.f,
            rPart = (getEffAlpha(cols, UR) + getEffAlpha(cols, LR)) / 2.f,
            dPart = (getEffAlpha(cols, LR) + getEffAlpha(cols, LL)) / 2.f,
            lPart = (getEffAlpha(cols, UL) + getEffAlpha(cols, LL)) / 2.f;
    const N2 rawNorm = (
            N2_UP * uPart +
            N2_RIGHT * rPart +
            N2_DOWN * dPart +
            N2_LEFT * lPart
            );
    if (pixDbg) std::cout << "\t\t\t\tPN{U=" << uPart << ", R=" << rPart << ", D=" << dPart << ", L=" << lPart << "}\n";
    if (rawNorm == N2(0.f)) return rawNorm;
    else return -normalize(rawNorm);
}

static unsigned int tracePixPoly(ImageRGBA * const& img, Sprite::Poly * const &poly, int x, int y, int lastX = -1, int lastY = -1, int dx = 1, int dy = 1) {
    const bool dbgPrint = false;

    //ColorRGBA& col = img->getPixelRef(x,y);
    //struct { bool up = true, down = true, left = true, right = true; } consider;
    //col.b = 1;
    ColorRGBA cols[4];
    getPixSample(img, x, y, cols);

    const N2 pos = N2(x, y);
    const N2 norm = getPixNorm(cols);
    poly->add(PhysicsPt(pos, norm));
    setPixProcFlags(img, x, y, true);
    if (dbgPrint) std::cout << "\t\tadded " << pos << " with norm " << norm << "\n";

    /*if(col.r > 127) consider.right = false;
    if(col.r < 127) consider.left = false;
    if(col.g < 127) consider.down = false;
    if(col.g > 127) consider.up = false;*/

    //int dx = x - lastX, dy = y - lastY;
    //if(lastX == -1 || lastY == -1) dx = dy = 1;
    if (lastX != x) dx = x - lastX;
    if (lastY != y) dy = y - lastY;

    nhighp bestDist = 999.f, altDist = 999.f;
    int bestX = -1, bestY = -1, altX = -1, altY = -1;
    //if(dbgPrint) std::cout << "\t\tat " << x << ", " << y << ": R"<<consider.right << " L"<<consider.left<< " U"<<consider.up << " D"<<consider.down << "\n";

    const int searchRadius = 1;
    for (int ox = -searchRadius; ox <= searchRadius; ox++) {
        for (int oy = -searchRadius; oy <= searchRadius; oy++) {
            if ((ox == 0 && oy == 0) || (abs(ox) + abs(oy) > searchRadius)) continue;
            const int cx = x + ox, cy = y + oy;
            if ((cx < 0 || cx > img->width) || (cy < 0 || cy > img->height) ||
                    (cx == lastX && cy == lastY)) continue;

            //Multiply by dx or dy!!!!
            /*int prefX = x + (int(col.r) - 127) / 64 * -sgn(dx), prefY = y + (int(col.g) - 127) / 64 * -sgn(dy);
            
            const ColorRGBA& cur = img->getPixel(cx,cy);
            if(cur.b != 0 || cur.alpha() == 0) continue;
            int dnx = (int(cur.r) - 127) / 64 - (int(col.r) - 127) / 64, dny = (int(cur.g) - 127) / 64 - (int(col.g) - 127) / 64;
            delX -= sgn(dx); delY -= sgn(dy);*/
            //int delX = ox, delY = oy;

            ColorRGBA curs[4];
            getPixSample(img, cx, cy, curs);
            bool valid = getPixProcFlags(curs, true);
            if (dbgPrint) std::cout << "\t\t\t" << cx << ", " << cy << " valid: " << valid << "\n";
            if (!valid) continue;

            const N2 cNorm = getPixNorm(curs);
            if(dbgPrint) std::cout << "\t\t\t considerding norm=" << cNorm << ", eq " << (cNorm == N2(0)) << " and fp " << (cNorm == N2(0.f)) << "\n";
            if (cNorm == N2(0.f)) continue;

            //Position culling
            /*if((ox < 0 && oy == 0 && !consider.left) || 
               (ox > 0 && oy == 0 && !consider.right) ||
               (oy < 0 && ox == 0 && !consider.down) ||
               (oy > 0 && ox == 0 && !consider.up) ||
               (ox < 0 && oy < 0 && !consider.left && !consider.down) || 
               (ox < 0 && oy > 0 && !consider.left && !consider.up) || 
               (ox > 0 && oy < 0 && !consider.right && !consider.down) || 
               (ox > 0 && oy > 0 && !consider.right && !consider.up)
                    ) continue;*/


            //int dist = (abs(delX*delX) + abs(delY*delY))*4 + 4*(dnx*dnx+ dny*dny);
            nhighp dist = sqrt(nhighp(ox * ox) + nhighp(oy * oy)) * pow((1.5f - dot(norm, cNorm) / 2.f), 2.f);

            /*if(prefX != x && cx == prefX) dist /= 2;
            else if(prefX != x && abs(ox) != 0) dist *= 2;
            
            if(prefY != y && cy == prefY) dist /= 2;
            else if(prefY != y && abs(oy) != 0) dist *= 2;*/

            //Normal culling
            //if(false &&prefX == cx && prefY == cy) dist /= 3;
            //else if(false&&(((col.r > 127 && cur.r < 127) || (col.r < 127 && cur.r > 127))/* && (col.g == 127 || cur.g == 127)*/) ||
            //   (((col.g > 127 && cur.g < 127) || (col.g < 127 && cur.g > 127))/* && (col.r == 127 || cur.r == 127)*/)
            //        ) dist *= 4;
            //else 
            //if((col.r == cur.r && col.g == cur.g)) dist /= 4;

            if (dbgPrint) std::cout << "\t\t\tcomp " << cx << ", " << cy << ": dist=" << dist << " from n1=" << norm << ", n2=" << cNorm << ", rdist=" << (sqrt(nhighp(ox * ox) + nhighp(oy * oy))) << "\n";

            if (dist <= bestDist) {
                if (dbgPrint) std::cout << "\t\t\tnew best dist " << dist << "\n";
                altX = bestX;
                altY = bestY;
                altDist = bestDist;

                bestDist = dist;
                bestX = cx;
                bestY = cy;
            } else if (dist <= altDist) {
                altDist = dist;
                altX = cx;
                altY = cy;
            }
        }
    }

    if (dbgPrint) std::cout << "\t\t\tbest x,y = " << bestX << ", " << bestY << " @ " << bestDist << "\n";
    if (bestX != -1 && bestY != -1) {
        unsigned int depth = tracePixPoly(img, poly, bestX, bestY, x, y, dx, dy);
        bool satisficted = depth > 5 || poly->closingDistance() < 3.f;

        if (!satisficted && altX != -1 && altY != -1) {
            if (dbgPrint) std::cout << "\t\tREVERT to " << x << ", " << y << " d=" << depth << ", use alts " << altX << ", " << altY << "\n";
            for (unsigned int depthSub = depth; depthSub > 0; depthSub--) {
                const unsigned int edgeIdx = poly->size() - depthSub;
                unsigned int rmx = (unsigned int)round(poly->get(edgeIdx).position().x), rmy = (unsigned int)round(poly->get(edgeIdx).position().y);
                //ColorRGBA &rmcol = img->getPixelRef(rmx, rmy);
                if (dbgPrint) std::cout << "\t\t\tREVNULL " << rmx << ", " << rmy << "\n";
                setPixProcFlags(img, rmx, rmy, false);
                //rmcol.b = 0;
            }
            poly->removeLast(depth);
            depth = tracePixPoly(img, poly, altX, altY, x, y, dx, dy);
            return depth + 1;
        }/* else if(!satisficted) {
            
        } */
        return depth + 1;
    }
    return 1;
}

void Sprite::calculatePolygon(bool dbg) {
    if (poly != nullptr) {
        delete poly;
        poly = nullptr;
    }
    if (origPoly != nullptr) {
        delete origPoly;
        origPoly = nullptr;
    }
    
    //if(name != "srx.grass_up_mid_closeX") return;

    ImageRGBA *useImg = this->img;
    if(this->mask != nullptr) useImg = this->mask;
    ImageRGBA *proc = new ImageRGBA(useImg->width, useImg->height);
    proc->zero();
    //PolygonSetDbg(nullptr, 1, 0);
    bounds.zero();

    //Find edges, get normals
    for (int x = 0; x < useImg->width; x++) {
        for (int y = 0; y < useImg->height; y++) {
            if (useImg->getAlpha(x, y) == 0) continue;

            bool isEdge = false;
            int dx = 0, dy = 0, adj = 0;
            if (x == 0 || useImg->getAlpha(x - 1, y) == 0) {
                dx--;
                adj++;
            }
            if (x == useImg->width - 1 || useImg->getAlpha(x + 1, y) == 0) {
                dx++;
                adj++;
            }
            if (y == 0 || useImg->getAlpha(x, y - 1) == 0) {
                dy--;
                adj++;
            }
            if (y == useImg->height - 1 || useImg->getAlpha(x, y + 1) == 0) {
                dy++;
                adj++;
            }

            _unused(adj);
            isEdge = (dx != 0 || dy != 0 || (adj % 2 == 0 && adj > 0)); // && (adj < 2 || (dx != 0 && dy != 0));//Exclude 1-pixel groups

            if (isEdge) {
                proc->setPixel(x, y, ColorRGBA(127 + 64 * dx, 127 + 64 * dy, 0, 255));
            } else {
                proc->setPixel(x, y, ColorRGBA(0, 0, 0, 255));
            }
        }
    }

    //Cellular automata to cull loose edges (tehe-pero)
    bool wasUpdated = true;
    while (wasUpdated) {
        unsigned int updateCount = 0;
        for (int x = 0; x < proc->width; x++) {
            for (int y = 0; y < proc->height; y++) {
                ColorRGBA& col = proc->getPixelRef(x, y);
                if (col.b == 0xFF) continue;

                int minNeighbors = 2, minAdjacent = 2;
                bool flipAlpha = false;
                int neighbors = 0, adjNeighbors = 0, nonNormNeighbors = 0, freeNeighbors = 0, adjFreeNeighbors = 0;
                if (col.b == 0xFB) {
                    minNeighbors += 1;
                    //minAdjacent++;
                    col.b = 0;
                } else if (col.b == 0xFC) {
                    col.b = 0xFA;
                    updateCount++;
                    continue;
                } else if (col.b == 0xFA) {
                    flipAlpha = true;
                    col.b = 0xFF;
                }

                for (int ox = -1; ox <= 1; ox++) {
                    for (int oy = -1; oy <= 1; oy++) {
                        if (ox == 0 && oy == 0) continue;
                        const int cx = x + ox, cy = y + oy;
                        if ((cx < 0 || cx >= proc->width) || (cy < 0 || cy >= proc->height)) {
                            freeNeighbors++;
                            if (abs(ox) + abs(oy) == 1) adjFreeNeighbors++;
                            continue;
                        }

                        const ColorRGBA& cur = proc->getPixel(cx, cy);
                        if (cur.alpha() == 0 || cur.b == 0xFF) continue;

                        neighbors++;
                        if (abs(ox) + abs(oy) == 1) adjNeighbors++;
                        if ((col.r < 127 && ox >= 0) ||
                                (col.r > 127 && ox <= 0) ||
                                (col.g < 127 && oy >= 0) ||
                                (col.g > 127 && oy <= 0) ||
                                (col.r == cur.r && col.g == cur.g)
                                ) nonNormNeighbors++;
                    }
                }

                minNeighbors -= freeNeighbors;
                //minAdjacent -= freeNeighbors;

                //if(x == 0 || x == proc->width-1) std::cout << "\tQ " << x << ", " << y << " neighbors=" << neighbors << " <? " << (minNeighbors) << ", adj=" << adjNeighbors << " <? " << (minAdjacent) << ", nNN=" << nonNormNeighbors << "\n";
                if (flipAlpha && col.alpha() == 0 && adjNeighbors == 4 - adjFreeNeighbors) {
                    col = ColorRGBA(0, 0, 0, 255);
                    updateCount++;
                } else if (col.alpha() == 0) {
                    if (adjNeighbors == 4 - adjFreeNeighbors) {
                        for (int ox = -1; ox <= 1; ox++) {
                            for (int oy = -1; oy <= 1; oy++) {
                                const int cx = x + ox, cy = y + oy;
                                if ((cx < 0 || cx >= proc->width) || (cy < 0 || cy >= proc->height)) continue;

                                ColorRGBA& cur = proc->getPixelRef(cx, cy);
                                if (abs(ox) + abs(oy) == 1 && cur.alpha() > 0) {
                                    cur.b |= 0xFB;
                                    updateCount++;
                                }
                            }
                        }
                        col.b = 0xFC;
                    } else col.b = 0xFF;
                    updateCount++;
                } else if (neighbors < minNeighbors || (adjNeighbors < minAdjacent)) {
                    ColorRGBA& cur = proc->getPixelRef(x, y);
                    cur.b = 0xFF;
                    //cur.v[3] = 0;
                    updateCount++;
                    //std::cout << "\tCULL " << x << ", " << y << " neighbors=" << neighbors << ", adj=" << adjNeighbors << ", nNN=" << nonNormNeighbors << "\n";
                }
            }
        }

        wasUpdated = updateCount > 0;
    }

    for (int x = 0; x < proc->width; x++) {
        for (int y = 0; y < proc->height; y++) {
            ColorRGBA& col = proc->getPixelRef(x, y);
            if (col.b == 0xFB || col.b == 0xFC) col.b = 0x0;
        }
    }

    //std::cout << "trace " << this->name << "\n";
    //bool saved = proc->Save("dbg/" + this->getBaseName(path) + "_proc.png");
    //std::cout << "SAVED TO " << ("dbg/" + this->getBaseName(path) + "_proc.png") << " ? = " << saved << "\n";
    //sleep(6);
    //Trace path, convert to polygon, WORKS ON POINTS BETWEEN PIXELS
    for (int x = 0; x <= proc->width; x++) {
        for (int y = 0; y <= proc->height; y++) {
            ColorRGBA cols[4];
            getPixSample(proc, x, y, cols);
            bool ofInterest = getPixInterestFlags(cols, true);

            //ColorRGBA& col = proc->getPixelRef(x, y);
            if (ofInterest && (poly == nullptr || !poly->contains(N2(x, y)))) {
                //col.b = 1;
                setPixProcFlags(proc, x, y, true);
                //std::cout << "START POLY" << x << ", " << y << "\n";

                Poly *newPoly = new Poly();
                tracePixPoly(proc, newPoly, x, y);
                
                if(newPoly->closingDistance() > 1.2f || newPoly->size() < 3) {
                    delete newPoly;
                    continue;
                }
                //Clean up "dirty" poly from trace
                newPoly->repair();
                
                newPoly->decollinearize();
                newPoly->smooth(0.9, true);
                newPoly->repair();
                
                
                nhighp prevArea;
                bool newPolyContained = poly != nullptr && poly->contains(N2(x,y));
                if (newPolyContained || newPoly->size() < 3/* || 
                        (prevArea = newPoly->area()) < (img->width + img->height) / 4.f*/) {
                    delete newPoly;
                    continue;
                }

                //unsigned int prevSize = newPoly->size(), curSize;
                //newPoly->fixNormals();
                //nhighp eps = 0.57f;
                //nhighp relEps = 0.8f;
                if (origPoly == nullptr) origPoly = new Poly(*newPoly);
                else origPoly->chain(new Poly(*newPoly));

                //while (prevSize - (newPoly->fastSimplify(eps, relEps), curSize = newPoly->size()) > (2 + prevSize / 10)) {
                //    prevSize = curSize;
                //}
                //newPoly->fixNormals();
                //newPoly->repair();

                //std::cout << "\tpoly of size " << newPoly->size() << ", area=" << newPoly->area() << ", bounds="<<VP(newPoly->bounds.center) << " to " << VP(newPoly->bounds.center+newPoly->bounds.axes[0]) <<"\n";
                //std::cout << "\tLeafdepth = " << newPoly->leafDepth() << "\n";
                //for (unsigned int i = 0; i < newPoly->size(); i++) std::cout << "\t\t" << to_string((*newPoly)[i]) << "\n";

                bounds.add(newPoly->bounds);
                if (poly == nullptr) poly = newPoly;
                else poly->chain(newPoly);
                break;
            }
        }
    }

    if(dbg) {
        std::string procName = "dbg/" + this->getBaseName(path) + "_proc.png";
        this->renderDebug(proc);
        proc->Save(procName);
        std::cout << "saved " << procName << "\n";
    }

    delete proc;
}

Sprite* Sprite::Factory(const std::string p) {
    Sprite* ret = new Sprite();
    if (p.size() == 0) return ret;

    if (ret->load(p)) return ret;

    delete ret;
    return nullptr;
}

//#include "lib/inlDev.h"

void Sprite::renderDebug(ImageRGBA *proc) {
    const int pFac = 8;
    //polyDbgFac = pFac;
    const int pDO = pFac*1;
    ImageRGBA *polyDbg = new ImageRGBA(proc->width*pFac + 2*pDO, proc->height * pFac + 2*pDO);
    //PolygonSetDbg(reinterpret_cast<void*>(polyDbg), pFac, pDO);
    polyDbg->zero();
    for (int x = pDO; x < polyDbg->width-pDO; x++) {
        for (int y = pDO; y < polyDbg->height-pDO; y++) {
            ColorRGBA pix = img->getPixel((x-pDO) / pFac, (y-pDO) / pFac); // + (proc->getPixel(x/pFac, y/pFac).lerped(ColorRGBA(), 0.6f));
            polyDbg->setPixel(x, y, pix);
            //polyDbg->setPixel(x,y,ColorRGBA(255,255,255,255));
        }
    }
    
    Poly* polyPtr = poly;
    while(polyPtr != nullptr) {
        //std::cout << "printing poly sz=" << polyPtr->size() << "\n";
        for(auto& seg : polyPtr->segments()) {
            //std::cout << " \t got poly seg " << seg << "\n";
            polyDbg->drawLine(pDO + pFac * seg.start().x.to<int>(), pDO + pFac * seg.start().y.to<int>(),
                pDO + pFac * seg.end().x.to<int>(), pDO + pFac * seg.end().y.to<int>(),ColorRGBA(255, 0, 255));
            polyDbg->drawPoint(pDO + pFac * seg.start().x.to<int>(), pDO + pFac * seg.start().y.to<int>(), ColorRGBA(0,0,0,255), pFac/2);
        }
        polyPtr = polyPtr->next;
    }
    polyPtr = poly;
    
    if(poly != nullptr) {
        for (int i = 0; i <= (proc->width*proc->height); i++) {
            //int sx = i, sy = proc->height*3/4;//
            int sx =rand()%(proc->width+2*pDO) - pDO, sy = rand() % (proc->height+2*pDO) - pDO;
            //if(i == 0) sx = sy = 0;
            ColorRGBA col;
            //polyDbgImg = polyDbg;
            if (poly->contains(N2(sx, sy))) col = ColorRGBA(0, 255, 0, 90);
            else col = ColorRGBA(255, 0, 0, 90);
            polyDbg->drawPoint(pDO+sx*pFac, pDO+sy*pFac, col, 2);
        }
    }
    
    /*
    polyDbgImg = nullptr;
    Poly *polyPtr = poly;
    if(poly != nullptr) {
        //polyPtr->fixBounds();
        
        N2 dStart = N2(polyPtr->centroid().x/2.f, -5.f);
        
        N2 testAxisBase = N2(this->imgSize().x, this->imgSize().y);
        ;//N2 axisA = (this->imgSize() - dStart) * N2(0.f, -1.f), axisB = (this->imgSize() - dStart) * N2(-1.f, 0.f);
        N2 axisA = N2(-1.f, 0.f), axisB = N2(0.f, -1.f);
        
        
        N2 axisANorm = normalize(axisA), axisBNorm = normalize(axisB);
        N2 axisMidNorm = normalize(testAxisBase - dStart);//(axisA+axisB != N2(0.f))? normalize(-axisA * -axisB) : normalize(testAxisBase - dStart);
        N2 axisNormCross = N2(axisMidNorm.y*-1.f, axisMidNorm.x);
        
        
        nhighp axisAMax = 9999.f, axisBMax = 9999.f;
        nhighp axisADistSq = 0.f, axisBDistSq = 0.f;
        nhighp bestDotA = 2.f, bestDotB = 2.f;
        N2 axisABest, axisBBest;
        Poly::Point *edgeA = nullptr, *edgeB = nullptr;
        bool ortho = true;
        
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            Poly::Point *pt = polyPtr->get(i);
            N2 ls = polyPtr->get(i)->pt, le = polyPtr->get((i + 1) % polyPtr->size())->pt;
            
            //std::cout << "projecting " << VP(ls) << "\n";
            N2 lineStart, dLine;
            N2 *selAxis; ColorRGBA col;
            if(ortho) {
                lineStart = ls;// + dStart;
                if(sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(lineStart - dStart, axisNormCross))) {
                    selAxis = &axisANorm;
                    dLine = -axisBNorm;//N2(-selAxis->y, selAxis->x);//axisMidNorm;
                    col = ColorRGBA(255, 0, 0);
                } else {
                    selAxis = &axisBNorm;
                    dLine = -axisANorm;//N2(-selAxis->y, selAxis->x);//axisMidNorm;
                    col = ColorRGBA(0,0,255);
                }
            } else {
                lineStart = dStart;
                dLine = normalize(ls - lineStart);
                if(sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(dLine, axisNormCross))) {
                    selAxis = &axisANorm;
                    col = ColorRGBA(255, 0, 0);
                } else {
                    selAxis = &axisBNorm;
                    col = ColorRGBA(0,0,255);
                }
            }
*/
            
            /*
            N2 toPt = normalize(ls - testAxisBase);
            N2 dAxis = normalize(axisA + axisB);
            N2 dAxisCross = N2(-dAxis.x, dAxis.y);
            
            N2 *const selAxis = (dot(toPt, dAxisCross) - dot(axisANorm, dAxisCross) < 0.f )? &axisANorm : &axisBNorm;
            */
            //std::cout << "\tusing start="<<VP(lineStart)<<", dLine=" << VP(dLine) << "\n\t axis=" << VP(*selAxis) << ", base=" << VP(testAxisBase) << "\n";
            //std::cout << "\t toPt = " << VP(toPt) << ", dAxis=" << VP(dAxis) << ", dAxisCross=" << VP(dAxisCross) << "\n";
            /*std::cout <<"\tdotPt=" << dot(toPt, dAxisCross) << 
                    ", dotA="<< dot(axisANorm, dAxisCross)<<
                    ", dotB="<<dot(axisBNorm, dAxisCross) << 
                    ", refDot=" << dot(dAxis, N2(1,1))<<"\n";*/
            
            /*
            
            
            N2 edgePt; nhighp edgeTime;
            bool foundInter = polyPtr->intersectRays(lineStart, dLine, testAxisBase, *selAxis, edgePt, &edgeTime);
            
            if(foundInter) {
                const nhighp d2 = dot(*selAxis, (edgePt - testAxisBase));//distance2(lineStart, ls);
                //std::cout << "\tgot inter pt=" << VP(edgePt) << ", t=" << edgeTime << ", d2=" << d2 << "\n";
                if(fabs(imgSize().x - edgePt.x) > 0.5 && fabs(imgSize().y - edgePt.y) > 0.5f) std::cout << "PROJECTION ERR\n";
                
                polyDbg->drawLine(pDO+lineStart.x* pFac, pDO+lineStart.y* pFac, pDO+edgePt.x* pFac, pDO+edgePt.y* pFac, col/2);
                polyDbg->drawLine(pDO+lineStart.x* pFac, pDO+lineStart.y* pFac, pDO+ls.x* pFac, pDO+ls.y* pFac, col);
                
                polyDbg->drawPoint(pDO+edgePt.x*pFac, pDO+edgePt.y*pFac, col, 2);
                
                N2 ptNorm = normalize(edgePt - lineStart);
                nhighp ptDot = dot(ptNorm, axisMidNorm);
                
                if(selAxis == &axisANorm) {
                    //std::cout << "\t\tA: eT=" << axisAMax << ", dBest=" << axisADistSq << "\n";
                    if((!ortho && ptDot < bestDotA) || (ortho && (d2 > axisADistSq) ) ) {
                        axisAMax = edgeTime;
                        edgeA = pt;
                        axisADistSq = d2;
                        axisABest = edgePt;
                        bestDotA = ptDot;
                        //std::cout << "\t\tUPD A\n";
                    }
                } else {
                    //std::cout << "\t\tB: eT=" << axisBMax << ", dBest=" << axisBDistSq << "\n";
                    if((!ortho && ptDot < bestDotB) || (ortho && (d2 > axisBDistSq) )) {
                        axisBMax = edgeTime;
                        edgeB = pt;
                        axisBDistSq = d2;
                        axisBBest = edgePt;
                        bestDotB = ptDot;
                        //std::cout << "\t\tUPD B\n";
                    }
                }
                
            } else {
                //std::cout << "NO CENTER FOR PT\n";
            }
            
            //N2 ln = polyPtr->get(i)->normalConst()*nhighp(pFac);
            //polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x,pDO+ ls.y* pFac + ln.y, ColorRGBA(255, 127, 0, 127));
        }
        
        polyDbg->drawLine(pDO+dStart.x*pFac, pDO+dStart.y*pFac,
                pDO+dStart.x*pFac+axisNormCross.x*4* pFac, pDO+dStart.x*pFac+axisNormCross.y*4* pFac, ColorRGBA(0,0,0,255));
        polyDbg->drawLine(pDO+dStart.x*pFac, pDO+dStart.y*pFac,
                pDO+testAxisBase.x* pFac, pDO+testAxisBase.y* pFac, ColorRGBA(255,255,255,255));
        
        polyDbg->drawLine(pDO+testAxisBase.x * pFac, pDO+testAxisBase.y * pFac,
                pDO+testAxisBase.x * pFac+axisANorm.x*4* pFac,
                pDO+testAxisBase.y * pFac+axisANorm.y*4* pFac, ColorRGBA(255,0,0,255));
        polyDbg->drawLine(pDO+testAxisBase.x * pFac, pDO+testAxisBase.y * pFac,
                pDO+testAxisBase.x * pFac+axisBNorm.x*4* pFac,
                pDO+testAxisBase.y * pFac+axisBNorm.y*4* pFac, ColorRGBA(255,0,0,255));
        
        if(edgeA != nullptr && edgeB != nullptr) {
            polyDbg->drawPoint(pDO+edgeA->positionConst().x*pFac, pDO+edgeA->positionConst().y*pFac, ColorRGBA(255,255,255), 6);
            polyDbg->drawPoint(pDO+edgeB->positionConst().x*pFac, pDO+edgeB->positionConst().y*pFac, ColorRGBA(255,255,255), 6);
            //int eraseDirection = sgn(distance2(edgeA->left() .position(), testAxisBase) - 
            //                         distance2(edgeA->right().position(), testAxisBase));
            N2 leftNorm  = normalize(edgeA->left() .position() - edgeA->positionConst());
            N2 rightNorm = normalize(edgeA->right().position() - edgeA->positionConst());
            
            int eraseInc;
            if(ortho) eraseInc = 1;
            else eraseInc = (dot(leftNorm, axisMidNorm) > dot(rightNorm, axisMidNorm))? 1 : -1;
            int newIdx = polyPtr->removeRange(polyPtr->index(edgeB) + eraseInc, polyPtr->index(edgeA), eraseInc);
            
            polyPtr->insert(newIdx, axisABest, eraseInc);
            polyPtr->insert(newIdx, testAxisBase, eraseInc);
            polyPtr->insert(newIdx, axisBBest, eraseInc);
            
            polyPtr->repair();
        }
        
    }
    */
    polyPtr = poly;
    if(false && polyPtr != nullptr) {
    for (int i = 0; i <= (proc->width/2); i++) {
        int sx = rand()%(proc->width+1), sy = rand() % (proc->height+1);
        int ex = rand()%(proc->width+1), ey = rand() % (proc->height+1);
        if(sx == ex && sy == ey) continue;
        
        ColorRGBA col;
        //polyDbgImg = polyDbg;
        //N2 inter;
        auto inter = poly->intersect(Line<N2>(N2(sx, sy), N2(ex, ey)), true);
        if (bool(inter)) col = ColorRGBA(0, 255, 0, 255);
        else col = ColorRGBA(255, 0, 0, 255);
        
        polyDbg->drawPoint(pDO+sx*pFac, pDO+sy*pFac, col, 2);
        polyDbg->drawLine(pDO+sx* pFac, pDO+sy* pFac, pDO+ex* pFac, pDO+ey* pFac, col);
        if(bool(inter)) {
            polyDbg->drawPoint(pDO+inter.start().x.to<int>()*pFac, pDO+inter.start().y.to<int>()*pFac, ColorRGBA(255, 0, 255, 255), 3);
            polyDbg->drawPoint(pDO+inter.end().x.to<int>()*pFac, pDO+inter.end().y.to<int>()*pFac, ColorRGBA(255, 255, 255, 255), 2);
        }
    }
    }
    /*while (polyPtr != nullptr) {
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            N2 ls = polyPtr->get(i)->pt, le = polyPtr->get((i + 1) % polyPtr->size())->pt;
            polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+le.x* pFac, pDO+le.y* pFac, ColorRGBA(0, 255, 255, 255));
            
            N2 ln = polyPtr->get(i)->normalConst()*nhighp(pFac);
            //std::cout << "drawnorm " << (ls.x* pFac) << ", " <<( ls.y* pFac)<<", "<<(ls.x* pFac + ln.x) << ", "<<( ls.y* pFac + ln.y) << "\n";
            polyDbg->drawLine(ls.x* pFac, ls.y* pFac, ls.x* pFac + ln.x, ls.y* pFac + ln.y, ColorRGBA(0, 127, 255, 127));
        }
        polyPtr = polyPtr->next;
    }*/
   /* polyPtr = poly;
    while (polyPtr != nullptr) {
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            N2 ls = polyPtr->get(i).position(), le = polyPtr->get((i + 1) % polyPtr->size()).position();
            polyDbg->drawLine(int(pDO+ls.x* pFac), int(pDO+ls.y* pFac), int(pDO+le.x* pFac), int(pDO+le.y* pFac), ColorRGBA(255, 255, 0, 255));
            
            N2 ln = polyPtr->get(i).normal()*nhighp(pFac);
            polyDbg->drawLine(int(pDO+ls.x* pFac), int(pDO+ls.y* pFac), int(pDO+ls.x* pFac + ln.x),int(pDO+ ls.y* pFac + ln.y), ColorRGBA(255, 127, 0, 127));
        }
        N2 ls = polyPtr->bounds.center, ln = polyPtr->bounds.axes[0];
        ColorRGBA bCol = ColorRGBA(255, 255, 255, 255);
        polyDbg->drawLine(int(pDO+ls.x* pFac), int(pDO+ls.y* pFac), int(pDO+ls.x* pFac + ln.x*pFac),int(pDO+ ls.y* pFac)            , bCol);
        polyDbg->drawLine(int(pDO+ls.x* pFac), int(pDO+ls.y* pFac), int(pDO+ls.x* pFac)           ,int(pDO+ ls.y* pFac + ln.y*pFac), bCol);
        polyDbg->drawLine(int(pDO+ls.x* pFac + ln.x*pFac), int(pDO+ls.y* pFac), int(pDO+ls.x* pFac + ln.x*pFac), int(pDO+ ls.y* pFac + ln.y*pFac), bCol);
        polyDbg->drawLine(int(pDO+ls.x* pFac), int(pDO+ls.y* pFac + ln.y*pFac), int(pDO+ls.x* pFac + ln.x*pFac),int(pDO+ ls.y* pFac + ln.y*pFac), bCol);
        
        polyPtr = polyPtr->next;
    }
*/



    std::string polyName = "dbg/" + this->getBaseName(path) + "_poly.png";
    polyDbg->Save(polyName);
    delete polyDbg;
}

void Sprite::DrawDebugBounds(DebugGroup *dbg, Transformable *ins, const Bounds<N2> *bnd, ColorRGBA bCol) {
    N2 ls = bnd->center;
    N2 ln = bnd->axes[0] + ls;
    /*polyDbg->drawLine(pDO + ls.x* pFac, pDO + ls.y* pFac, pDO + ls.x * pFac + ln.x*pFac, pDO + ls.y* pFac, bCol);
    polyDbg->drawLine(pDO + ls.x* pFac, pDO + ls.y* pFac, pDO + ls.x* pFac, pDO + ls.y * pFac + ln.y*pFac, bCol);
    polyDbg->drawLine(pDO + ls.x * pFac + ln.x*pFac, pDO + ls.y* pFac, pDO + ls.x * pFac + ln.x*pFac, pDO + ls.y * pFac + ln.y*pFac, bCol);
    polyDbg->drawLine(pDO + ls.x* pFac, pDO + ls.y * pFac + ln.y*pFac, pDO + ls.x * pFac + ln.x*pFac, pDO + ls.y * pFac + ln.y*pFac, bCol);*/
    dbg->drawLine(ins->localToRender(N2(ls.x, ls.y)), ins->localToRender(N2(ln.x, ls.y)), bCol);
    dbg->drawLine(ins->localToRender(N2(ls.x, ls.y)), ins->localToRender(N2(ls.x, ln.y)), bCol);
    dbg->drawLine(ins->localToRender(N2(ln.x, ln.y)), ins->localToRender(N2(ln.x, ls.y)), bCol);
    dbg->drawLine(ins->localToRender(N2(ln.x, ln.y)), ins->localToRender(N2(ls.x, ln.y)), bCol);
}

void Sprite::drawDebug(DebugGroup *dbg, SpriteInstance *ins) {
    if(dbg == nullptr || ins == nullptr) return;
    
    Poly* polyPtr = poly;
    dbg->setLevel(2);
    if(bounds.area() > 0.01f) DrawDebugBounds(dbg, ins, &(this->bounds), ColorRGBA(0, 180, 180, 160));
    //std::cout << "Sprite Drawdbg, poly=" << polyPtr << ", area=" << (polyPtr == nullptr? 0.f : polyPtr->area()) << "\n";
    while (polyPtr != nullptr) {
        //std::cout << "DBG POLY:\n";
        for (unsigned int i = 0; i < polyPtr->size(); i++) {
            const PhysicsPt* pt = &polyPtr->get(i), *nextPt;
            nextPt = pt->right(polyPtr);
            if(pt == nullptr || nextPt == nullptr) continue;
            
            N2 ls = ins->localToRender(pt->position()), 
               le = ins->localToRender(nextPt->position());
            //polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+le.x* pFac, pDO+le.y* pFac, ColorRGBA(255, 255, 0, 255));
            dbg->setLevel(1);
            dbg->drawLine(ls, le, ColorRGBA(255, 255, 0, 255), 0.015f);
            dbg->drawPoint(ls, ColorRGBA(255, 0, round(255.f*(i+1)/polyPtr->size()), 255), 0.03f);
            //std::cout << "\t" << VP(pt->pt) << " -> " << VP(nextPt->pt) << "\n";
            
            //N2 ln = pt->normalConst();
            //polyDbg->drawLine(pDO+ls.x* pFac, pDO+ls.y* pFac, pDO+ls.x* pFac + ln.x,pDO+ ls.y* pFac + ln.y, ColorRGBA(255, 127, 0, 127));
        }
        
        dbg->setLevel(2);
        DrawDebugBounds(dbg, ins, &(polyPtr->bounds), ColorRGBA(0, 255, 0, 127));
        
        polyPtr = polyPtr->next;
    }
    
    dbg->setLevel(2);
    dbg->drawPoint(ins->localToRender(N2(0.0,0.0)*imgSize()), ColorRGBA(0, 0, 255));
    dbg->drawPoint(ins->localToRender(N2(1.0,0.0)*imgSize()), ColorRGBA(255, 0, 0));
    dbg->drawPoint(ins->localToRender(N2(0.0,1.0)*imgSize()), ColorRGBA(0, 255, 0));
    dbg->drawPoint(ins->localToRender(N2(1.0,1.0)*imgSize()), ColorRGBA(255, 255, 255));
}

void Sprite::repair() {
    bounds.zero();
    for(Poly* polyPtr = poly; polyPtr != nullptr; polyPtr = polyPtr->next) {
        polyPtr->repair();
        bounds.add(polyPtr->bounds);
    }
}
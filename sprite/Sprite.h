#ifndef SPRITE_H
#define SPRITE_H
#include "../TMSCInclude.h"
#include <ARKE/Loadable.h>
#include <ARKE/Image.h>
#include <NVX/NPolygon.h>
#include "../physics/PhysicsPt.h"

class TextureAtlas;
class Shader;
class DebugGroup;
class SpriteInstance;
class Transformable;

#define SPRITE_MULTI_ATLAS 2

class Sprite : public Loadable {
public:
    Sprite();
    Sprite(const std::string& p, bool rel = false);
    virtual ~Sprite();
    
    template<typename T>
    inline T* derive() {
        return dynamic_cast<T*>(this);
    }

    typedef NPolygon<N2, PhysicsPt> Poly;

    inline virtual std::string getName() const override {
        return this->getBaseName(subPath, "", "/.");
    }

    virtual bool couldLoad(const std::string& p) const override;
    bool load(const std::string& p, bool rel);
    inline virtual bool load(const std::string& p) override {
        return load(p, false);
    }
    virtual void unload() override;
    
    virtual void flip(bool doX, bool doY);
    
    virtual N2 imgSize() const;

    inline virtual void* rawInstancePtr() override {
        return reinterpret_cast<void*> (this);
    };

    virtual void generatePhysics();
    virtual void prepare();
    
    void repair();

    virtual void upload();

    static Sprite* Factory(const std::string p = "");

    inline Poly* polygon() {
        return this->poly;
    };

    inline const Poly* polygonConst() const {
        return this->poly;
    };

    N2T<Uint16, 1> getTexCoords(const TCoord& vert, const TCoord& skew = TCoord(0,0)) const;
    N2T<Uint16, 1> getOffsetCoords(const TCoord& vert, const TCoord& skew = TCoord(0,0)) const;
    unsigned short getAtlasPage() const;

    void renderDebug(ImageRGBA *proc);
    virtual void drawDebug(DebugGroup *dbg, SpriteInstance *ins);
    static void DrawDebugBounds(DebugGroup *dbg, Transformable *ins, const Bounds<N2> *bnd, ColorRGBA col);

    Poly *poly, *origPoly;
    Bounds<N2> bounds;

    ImageRGBA *img, *detail, *mask;
    unsigned int atlasID;

    void calculatePolygon(bool dbg = false);

    static void UploadAtlas();
    static void CacheAtlas();
    static void BindAtlas(Shader *shader);

protected:
    unsigned short atlasPage;
    N2T<Uint16, 1> texCoordBase, texCoordSize, texCoordOffsetBase, texCoordOffsetSize;

    static TextureAtlas *spriteAtlas[SPRITE_MULTI_ATLAS];
    static bool spriteAtlasInit, spriteAtlasDirty;
    static void AllocateAtlas();
};

#endif /* SPRITE_H */


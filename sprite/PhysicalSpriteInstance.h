#ifndef PHYSICALSPRITEINSTANCE_H
#define PHYSICALSPRITEINSTANCE_H
#include "../TMSCInclude.h"
#include "SpriteInstance.h"
#include "../physics/PhysicalObject.h"

class PhysicalSpriteInstance : public virtual SpriteInstance, public virtual PhysicalObject, public virtual Adapter {
public:
    PhysicalSpriteInstance() {};
    virtual ~PhysicalSpriteInstance() {};
private:

};

#endif /* PHYSICALSPRITEINSTANCE_H */


#ifndef SPRITEUSER_H
#define SPRITEUSER_H
#include "../TMSCInclude.h"
#include "../physics/PhysicsPt.h"
#include "../physics/Transformable.h"
#include <NVX/Bounds.h>

class Sprite;

class SpriteUser : public virtual Transformable{
public:
    SpriteUser();
    SpriteUser(const SpriteUser& orig);
    virtual ~SpriteUser();
    
    virtual Sprite* sprite() const;
    virtual PhysicsPoly* poly() const;
    virtual const Bounds<N2>* bounds() const;
    inline virtual bool isComplete() const { return sprite() != nullptr; };
    
    virtual Bounds<N2> getParentBoundsOf(const Bounds<N2>& bnd) const;
    virtual Bounds<N2> getParentBounds() const;
    
    virtual void computeLocals();
    virtual void computeAgnostics();
    
    virtual PhysicsPoly* agnosticPoly();
    virtual const Bounds<N2>* agnosticBounds() const;
    virtual void freeAgnostics();
protected:
    PhysicsPoly *phyCachedPoly;
    Bounds<N2> phyCachedBounds, phyLocalBounds;
};

#endif /* SPRITEUSER_H */


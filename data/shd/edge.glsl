
//Example getLineFX(normal, camdir, lightstr 0...1, minDot(0...1) = 0.4, maxDot(0...1) = 0.1)
float getLineFX(vec3 normal, vec3 camDir, float lightStrNorm,float a, float b) {
     float minDot = 0.8; float maxDot = 0.7;
    const float lineThreshold = mix(minDot, maxDot, smoothstep(0.0, 1.0, lightStrNorm));
    const float lineStr = clamp(lineThreshold - dot(camDir, normal), -0.5, 1.0);
    float ret = smoothstep(-0.5, lineThreshold*1.0, lineStr);
    //ret = pow(ret, 0.5);
    ret = smoothstep(0.0, 1.0, ret);//*0.6;
    ret = pow(ret, 6.0);//*3.0;
    return clamp(ret, 0.0, 1.0);
}

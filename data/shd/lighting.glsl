vec3 PerturbNormal(vec3 surf_pos,vec3 surf_norm , float height) {
        vec3 vSigmaS = dFdxCoarse( surf_pos );
        vec3 vSigmaT = dFdyCoarse( surf_pos );
        vec3 vN = surf_norm;
        // normalized
        vec3 vR1 = cross(vSigmaT ,vN);
        vec3 vR2 = cross(vN,vSigmaS);
        float fDet = dot(vSigmaS , vR1);
        float dBs = dFdxFine( height );
        float dBt = dFdyFine( height );
        vec3 vSurfGrad =
        sign(fDet) * ( dBs * vR1 + dBt * vR2 );
        return normalize(abs(fDet)*vN -vSurfGrad);
      }
      
float orenNayarDiffuse(
  vec3 lightDirection,
  vec3 viewDirection,
  vec3 surfaceNormal,
  float roughness,
  float albedo) {
  
  float LdotV = dot(lightDirection, viewDirection);
  float NdotL = dot(lightDirection, surfaceNormal);
  float NdotV = dot(surfaceNormal, viewDirection);

  float s = LdotV - NdotL * NdotV;
  float t = mix(1.0, max(NdotL, NdotV), step(0.0, s));

  float sigma2 = roughness * roughness;
  float A = 1.0 + sigma2 * (albedo / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));
  float B = 0.45 * sigma2 / (sigma2 + 0.09);

  return albedo * max(0.0, NdotL) * (A + B * s / t) / PI;
}

vec3 calcLighting(vec3 pos, vec3 useNormal, float height) {
    vec3 N = PerturbNormal(pos, normalize(useNormal), height);//normalize(cross(dFdy(normal), dFdx(normal)));
        //float lrp = abs(dot(N, useNormal)) * pow( abs(dot(useNormal, camViewDirection)), 1/4);      
        //N = normalize(mix(useNormal, N, pow(lrp, 1/2.0)));//  * ( 1.0 - pow(abs(dFdxFine(height))+abs(dFdxFine(height)) , 0.38)));
      
      ivec2 ssc = ivec2(gl_FragCoord.xy);
      vec4 lComp = texelFetch(lightComp, ssc, 0).rgba;
      
      //if(diffuse.a < 0.5) discard;
      //out_FragColor.a *= 0.2;
      
      const float phong = 1/3.6;
      vec3 lightDir = -normalize(vec3(0.5, -1.0, sin(gtd*PI/10.0) *2.0));
      float lightStrength = orenNayarDiffuse(lightDir, -camViewDirection, N, 0.5, 1.0);
      lightStrength = pow(clamp(lightStrength , 0.0, 1.0), phong*3.6);
      //lightStrength = pow( lComp.r*lComp.r* lightStrength, 0.4)*0.2 + pow(lightStrength, 0.7)*0.3 + pow(lightStrength, 1)*0.5;
      lightStrength =(lightStrength*(phong*2.2) + 0.25)*(0.3 + 0.7*pow(lComp.r, 1.3)) ;
      //out_FragColor.rgb = useNormal.rgb;//((normal/2)+vec3(0.5)) * pow(lComp.r, 1.5);//
      
      const float heightOcclusion = 1.2;
      vec3 lightCol = vec3(1, 250.0/255.0, 244.0/255.0);
      return 2.2 * lightStrength* lightCol * clamp((diffuse.a - 0.5)*(1.0/heightOcclusion) + 0.5*heightOcclusion, 0.1, 1.0);
}
#"Pretty" name without spaces, and system name (ie, the name of target files in Buildsys)
NAME = TMSC
SYS_NAME = TMSC

#Directory that contains .cpp files with main()
EXEC_DIRS = ./

#directories with classes/files to be compiled into objects
SRC_DIRS = entity map physics render sprite ui

#directories containing headers that need to be installed
HEADER_DIRS = 
HEADER_CP_DIRS = entity map physics render sprite ui
HEADER_FILES += TMSCFundamental.h TMSCInclude.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = TMSC/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
PCH =

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS +=  `sdl2-config --cflags` `freetype-config --cflags`
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?=
#Optimization flags, supporting PGO if needed
OPTI ?= -O0
#Include paths, ie -I/path/to/headers/
INCPATH += 
#Libraries, ie -lopenmp
LIBS += -lNVX -lARKE -lGL -lGLEW -lGLU `sdl2-config --libs` -lassimp -lfreetype -lnoise -lpthread

include /usr/include/LPBT.mk

install::

uninstall::

autorun::

disable::


